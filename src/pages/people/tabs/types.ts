export type TabsPeopleProps = {
  value?: string;
  indicatorColor?: "primary" | "secondary";
  textColor?: "inherit" | "primary" | "secondary";
  variant?: "fullWidth" | "scrollable" | "standard";
  scrollButtons?: "auto" | "desktop" | "off" | "on";
  arialabel?: string;
  children?: React.ReactNode;
};
