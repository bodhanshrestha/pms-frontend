import React from 'react';
import PeopleCard from './PeopleCard';
import Grid from '@material-ui/core/Grid';
const PeopleTab = ({ teamMembers }: any) => {
	return (
		<Grid container spacing={2}>
			{teamMembers &&
				teamMembers.map((el: any, i: number) => (
					<Grid item xs={12} sm={6} md={4} xl={3} key={i}>
						<PeopleCard data={el} />
					</Grid>
				))}
		</Grid>
	);
};

export default PeopleTab;
