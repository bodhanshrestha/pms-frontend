import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import PeopleTab from './everyone';

interface TabPanelProps {
	children?: React.ReactNode;
	index: any;
	value: any;
}

function TabPanel(props: TabPanelProps) {
	const { children, value, index, ...other } = props;

	return (
		<div
			role='tabpanel'
			hidden={value !== index}
			id={`scrollable-auto-tabpanel-${index}`}
			aria-labelledby={`scrollable-auto-tab-${index}`}
			{...other}
		>
			{value === index && (
				<Box p={3}>
					{children}
				</Box>
			)}
		</div>
	);
}

function a11yProps(index: any) {
	return {
		id: `scrollable-auto-tab-${index}`,
		'aria-controls': `scrollable-auto-tabpanel-${index}`,
	};
}

const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1,
		width: '100%',
		backgroundColor: theme.palette.background.paper,
	},
}));

export default function ScrollableTabsButtonAuto({ teamMembers }: any) {
	const classes = useStyles();
	const [value, setValue] = React.useState(0);

	const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
		setValue(newValue);
	};

	const filterData = (role: string) => {
		return teamMembers.filter((el: any) => el.role === role);
	};
	const tabPanelData = [
		{
			label: 'Everyone',
			teamMembers: teamMembers,
		},
		filterData('manager').length > 0 && {
			label: 'Manager',
			teamMembers: filterData('manager'),
		},
		filterData('developer').length > 0 && {
			label: 'Developer',
			teamMembers: filterData('developer'),
		},
		filterData('analyst').length > 0 && {
			label: 'Analyst',
			teamMembers: filterData('analyst'),
		},
		filterData('trainer').length > 0 && {
			label: 'Trainer',
			teamMembers: filterData('trainer'),
		},
		filterData('tester').length > 0 && {
			label: 'Tester',
			teamMembers: filterData('tester'),
		},
		filterData('member').length > 0 && {
			label: 'Member',
			teamMembers: filterData('member'),
		},
		filterData('management').length > 0 && {
			label: 'Management',
			teamMembers: filterData('management'),
		},
		filterData('deployment_team').length > 0 && {
			label: 'Deployment Team',
			teamMembers: filterData('deployment_team'),
		},
	];
	const filteredData = tabPanelData.filter((item: any) => item && item);
	return (
		<div className={classes.root}>
			<AppBar position='static' color='default'>
				<Tabs
					value={value}
					onChange={handleChange}
					indicatorColor='primary'
					textColor='primary'
					variant='scrollable'
					scrollButtons='auto'
					aria-label='scrollable auto tabs example'
				>
					{filteredData.map((data: any, i: number) => {
						if (data) return <Tab label={data.label} {...a11yProps(i)} key={i} />;
						return false;
					})}
				</Tabs>
			</AppBar>
			{filteredData.map((data: any, i: number) => {
				if (data)
					return (
						<TabPanel value={value} index={i} key={i}>
							<PeopleTab teamMembers={data.teamMembers} />
						</TabPanel>
					);
				return false;
			})}
		</div>
	);
}
