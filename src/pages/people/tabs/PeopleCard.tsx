import React from 'react';
import PaperComponent from '../../../components/paper';
import TypographyComponent from '../../../components/typography';
import ButtonComponent from '../../../components/button';
import AvatarComponent from '../../../components/avatar';
import image from '../../../images/pic1.jpg';
import '../index.scss';
import { Link } from 'react-router-dom';

const PeopleCard = ({ data }: any) => {
	const { role, member } = data;
	return (
		<div className='people-main'>
			<PaperComponent square className='paper-component'>
				<Link to={`/profile?username=${member.username}`}>
					<div className='avatar-style'>
						<AvatarComponent
							alt='User Avatar'
							src={member.avatar ? member.avatar : image}
							className='avatar-size '
							variant='square'
						/>
					</div>
					<TypographyComponent label={member.name} />
					<ButtonComponent label={role} variant='contained' color='primary' />
					<TypographyComponent label={member.username} />
					<TypographyComponent label={member.email} />
				</Link>
			</PaperComponent>
		</div>
	);
};

export default PeopleCard;
