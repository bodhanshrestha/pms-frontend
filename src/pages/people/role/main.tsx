import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import SelectInputField from '../../../components/form/select';
import TypographyComponent from '../../../components/typography';
import { ProjectRoleUpdateAsyncAction } from '../../../store/project/asyncAction';
import InputField from './../../../components/form/text';

const MainRole = ({ data }: any) => {
	const dispatch = useDispatch();
	const [role, setRole] = useState('');
	const handleChange = (e: any) => {
		setRole(e.target.value);
		dispatch(
			ProjectRoleUpdateAsyncAction(localStorage.getItem('project'), {
				_id: data._id,
				member: data.member._id,
				working_hours: data.working_hours,
				role: e.target.value,
			})
		);
	};
	const [estimate, setEstimate] = useState('');
	const [error, setError] = useState(false);

	const handleEstimateChange = (event: any) => {
		setEstimate(event.target.value);
	};
	const handleTimeSubmit = (e: any) => {
		setError(false);

		if (e.key === 'Enter') {
			if (new RegExp('^([0-9]|[0-1][0-8])(h)$').test(estimate)) {
				dispatch(
					ProjectRoleUpdateAsyncAction(localStorage.getItem('project'), {
						_id: data._id,
						member: data.member._id,
						role: data.role,
						working_hours: estimate,
					})
				);
			} else {
				return setError(true);
			}
		}
	};
	useEffect(() => {
		if (data) {
			setRole(data.role);
			setEstimate(data.working_hours)
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);
	return (
		<tr>
			<td>
				<TypographyComponent label={data.member.username} variant='h6' />
			</td>
			<td>

				<SelectInputField
					name='role'
					label=''
					placeholder='Select Role'
					value={role}
					disabled={data.role === 'manager'}
					callback={handleChange}
					data={[
						{
							value: 'management',
							label: 'Management',
						},
						{
							value: 'manager',
							label: 'Project Manager',
						},
						{
							value: 'analyst',
							label: 'Analyst',
						},
						{
							value: 'developer',
							label: 'Developer',
						},
						{
							value: 'tester',
							label: 'Tester',
						},
						{
							value: 'trainer',
							label: 'Trainer',
						},
						{
							value: 'member',
							label: 'Member',
						},
						{
							value: 'deployment_team',
							label: 'Deployment Team',
						},
					]}
				/>

			</td>
			<td>
				<InputField
					label=''
					name='estimate'
					type='text'
					value={estimate}
					callback={handleEstimateChange}
					eventCallBack={handleTimeSubmit}
					eventOnBlur={() => handleTimeSubmit({ key: 'Enter' })}
					error={error}
					marginBottom='0'
				/>
			</td>
		</tr>
	);
};

export default MainRole;
