/* eslint-disable */
import React from 'react';
import { Paper } from '@material-ui/core';
import { useSelector } from 'react-redux';
import TypographyComponent from '../../../components/typography';
import MainRole from './main';

const PeopleRole = ({ }) => {
	const teamMembers = useSelector((state: any) => state.project.team);

	return (
		<Paper className='p-20' >
			<TypographyComponent
				label='Manage Users Role'
				variant='h4'
				className='mb-10'
				style={{ textDecoration: 'underline' }}
			/>
			<table>
				<tr>
					<th style={{ minWidth: '160px', textAlign: 'left' }}>
						<TypographyComponent label='Username' variant='h6' />
					</th>
					<th style={{ minWidth: '200px', textAlign: 'left' }}>
						<TypographyComponent label='Role' variant='h6' />
					</th>
					<th style={{ minWidth: '200px', textAlign: 'left' }}>
						<TypographyComponent label='Working Hours / day' variant='h6' />
					</th>
				</tr>
				{teamMembers &&
					teamMembers.map((el: any, i: number) => (
						<MainRole data={el} key={i} />
					))}
			</table>
		</Paper>
	);
};
export default PeopleRole;
