import React, { useEffect } from 'react';
import ScrollableTabsButtonAuto from './tabs/index';
import Grid from '@material-ui/core/Grid';
import { useDispatch, useSelector } from 'react-redux';
import InviteTeam from './inviteTeam';
import './index.scss';
import { ProjectTeamMemberAsyncAction } from '../../store/project/asyncAction';
import Settings from '@material-ui/icons/Settings';
import TypographyComponent from '../../components/typography';
import { useHistory } from 'react-router';
const PeoplePage = () => {
	const dispatch = useDispatch();
	const history = useHistory();
	const teamMembers = useSelector((state: any) => state.project.team);

	useEffect(() => {
		dispatch(ProjectTeamMemberAsyncAction());
		return () => { };
	}, [dispatch]);
	const handleSettingIcon = () => {
		history.push('/people/edit-role');
	};
	return (
		<div className='people-main'>
			<div className='space-between '>
				<TypographyComponent label='Project Team Members' variant='h4' />

				{teamMembers &&
					teamMembers.filter(
						(el: any) =>
							el.member.username === localStorage.getItem('username') &&
							el.role === 'manager'
					).length > 0 && (
						<>
							<InviteTeam />
							<Settings className='pointer' onClick={handleSettingIcon} />
						</>
					)}
			</div>

			<br />
			<Grid container spacing={2}>
				{teamMembers && <ScrollableTabsButtonAuto teamMembers={teamMembers} />}
			</Grid>
		</div>
	);
};

export default PeoplePage;
