import React from 'react';
import FormDialog from '../../components/form dialog';
import { inviteTeammateWithEmailAsyncAction } from '../../store/auth/asyncAction';

export default function inviteTeamDialog() {
	const content =
		'	To invite teammate to this project, please enter your teammate email address here. We will send email to respective email Address.';
	return (
		<FormDialog
			dispatchFunction={inviteTeammateWithEmailAsyncAction}
			content={content}
			displayBtnLabel='Invite Teammate'
			dialogTitle='Invite Teammate'
			submitLabel='Invite'
			variant='outlined'
		/>
	);
}
