import React from 'react';
import TypographyComponent from '../../components/typography';

const DisplayProject = ({ project }: any) => {
	const data = [
		{
			leftLabel: 'Project Name',
			rightLabel: project.name,
		},
		{
			leftLabel: 'Project ShortName',
			rightLabel: project.shortName,
		},
		{
			leftLabel: 'Project Creator',
			rightLabel: project.leader,
		},
	];
	return (
		<div className='p-10 pt-20'>
			{data.map((proj: any, i: number) => (
				<div className='flex-start' key={i}>
					<TypographyComponent
						label={proj.leftLabel}
						style={{ paddingRight: '5px' }}
					/>
					:
					<TypographyComponent
						label={proj.rightLabel}
						style={{ paddingLeft: '5px' }}
					/>
				</div>
			))}
		</div>
	);
};

export default DisplayProject;
