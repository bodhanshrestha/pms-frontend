import React, { useEffect } from 'react';
import { useFormik } from 'formik';
import { useDispatch } from 'react-redux';
import ButtonComponent from '../../components/button';
import InputField from '../../components/form/text';
import { ProjectUpdateAsyncAction } from '../../store/project/asyncAction';

const EditProject = ({ setEdit, project }: any) => {
	const dispatch = useDispatch();
	const formik = useFormik({
		initialValues: {
			name: '',
			shortName: '',
		},
		onSubmit: (values) => {
			dispatch(ProjectUpdateAsyncAction(project._id, values));
			handleCancel();
		},
	});

	// const handleReset = () => {
	// 	formik.resetForm();
	// };
	const handleCancel = () => {
		setEdit(false);
	};
	useEffect(() => {
		formik.setValues(project);
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);
	return (
		<form
			noValidate
			autoComplete='off'
			onSubmit={formik.handleSubmit}
			className='pt-20'
		>
			<InputField
				label='Project Name'
				name='name'
				value={formik.values.name}
				callback={formik.handleChange}
				variant='outlined'
				className='mb-20'
			/>
			<InputField
				label='Project Short Name'
				name='shortName'
				value={formik.values.shortName}
				callback={formik.handleChange}
				variant='outlined'
				className='mb-20'
				disabled={true}
			/>
			<ButtonComponent
				label='Submit'
				type='submit'
				variant='contained'
				color='primary'
			/>
			<ButtonComponent label='cancel' callback={handleCancel} />
		</form>
	);
};

export default EditProject;
