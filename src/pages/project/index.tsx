import React, { useState, useEffect } from 'react';
import { Paper } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import Settings from '@material-ui/icons/Settings';
import Close from '@material-ui/icons/Close';
import DisplayProject from './display';
import EditProject from './edit';
import TypographyComponent from '../../components/typography';
import Divider from '@material-ui/core/Divider';
import { ProjectAsyncAction } from '../../store/project/asyncAction';
import AvatarComponent from './../../components/avatar/index';
import { managerAndManagementRole } from './../../utils/role';

const ProjectPage = () => {
	const dispatch = useDispatch();
	const [edit, setEdit] = useState(false);
	const project = useSelector((state: any) => state.project);
	const handleSettingIcon = () => {
		setEdit(!edit);
	};
	useEffect(() => {
		dispatch(ProjectAsyncAction());
		return () => { };
	}, [dispatch]);
	return (
		<>
			<Paper className='p-20 w-50' style={{ minWidth: '480px' }}>
				<div className='space-between'>
					<div className='space-between'>
						{!edit ? (
							<TypographyComponent
								label='Current Project'
								variant='h4'
								className='mb-20'
							/>
						) : (
							<TypographyComponent
								label='Edit Project'
								variant='h4'
								className='mb-20'
							/>
						)}
						<AvatarComponent
							src={project.image}
							className='mb-20 ml-10'
							variant='square'
						/>
					</div>

					{project && managerAndManagementRole(project.team) &&
						(!edit ? (
							<div>
								<Settings className='pointer' onClick={handleSettingIcon} />
							</div>
						) : (
							<div>
								<Close className='pointer' onClick={handleSettingIcon} />
							</div>
						))}
				</div>
				<Divider />
				{!edit ? (
					<DisplayProject project={project} />
				) : (
					<EditProject setEdit={setEdit} project={project} />
				)}
			</Paper>
		</>
	);
};

export default ProjectPage;
