import React from 'react';

import Skeleton from '@material-ui/lab/Skeleton';
import PaperComponent from '../../components/paper';
const SubSkeleton = () => {
  return (
    <PaperComponent square className="paper-component" style={{ padding: '10px' }}>
      <Skeleton variant="rect" width="100%" height={210} />
      <div style={{ margin: '20px 0px' }}>
        <Skeleton variant="rect" width={180} height={15} style={{ display: 'block', margin: "auto", marginTop: '15px' }} />
        <Skeleton variant="rect" width={100} height={40} style={{ display: 'block', margin: "10px auto auto auto " }} />
      </div>
    </PaperComponent >
  )
}
const SelectProjectSkeleton = () => {
  return (
    <div className='people-main'>
      <SubSkeleton />
    </div>
  )
};

export default SelectProjectSkeleton;
