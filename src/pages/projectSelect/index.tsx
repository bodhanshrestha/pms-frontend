import React from 'react'
import { useSelector } from 'react-redux'
import ProjectCard from './card';
import { useLocation } from 'react-router';
import SelectProjectSkeleton from './skeleton';

const SelectProject = () => {
  const { pathname } = useLocation()
  const projects = useSelector((state: any) => state.user.user.projects)
  return (
    <div style={{ textAlign: 'center', marginTop: pathname === '/projects' ? "20px" : '60px' }} >
      <h1>Select Project</h1>
      <div className='flex-center flex-wrap gap-20'>
        {
          projects && Object.keys(projects).length ? projects.map((el: any, i: number) => (
            <div key={i}>
              <ProjectCard data={el} />
            </div>
          )) : (
            <>
              {
                Array(2).fill(1).map((el: any, i: number) => (
                  <div key={i}>
                    <SelectProjectSkeleton />
                  </div>
                ))
              }
            </>
          )
        }
      </div>
    </div>
  )
}

export default SelectProject
