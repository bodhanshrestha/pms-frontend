import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import ButtonComponent from '../../components/button';

export default function ChooseOptionSelectWarning({ open, setOpen, id, loading, handleClick }: any) {


  const handleClickOpen = () => {
    handleClick(id, true);
    setOpen({ open, id, loading: true });
  };

  const handleClose = () => {
    if (setOpen) setOpen({ open: false, id: '', loading: false });
  };

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">Switch Project</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          Are you sure you want to switch the project.
        </DialogContentText>
      </DialogContent>
      <DialogActions>

        <Button onClick={handleClose} color="secondary">
          Disagree
        </Button>

        <ButtonComponent
          label='Agree'
          variant='text'
          color='primary'
          disabled={loading}
          callback={handleClickOpen}
        />
      </DialogActions>
    </Dialog>
  );
}
