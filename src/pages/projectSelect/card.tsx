import React, { useState } from 'react';

import image from '../../images/pic1.jpg';
import PaperComponent from './../../components/paper/index';
import AvatarComponent from './../../components/avatar/index';
import Divider from '@material-ui/core/Divider';
import ButtonComponent from './../../components/button/index';
import { useDispatch } from 'react-redux';
import { chooseProject } from '../../store/auth/asyncAction';
import { useHistory } from 'react-router-dom';
import ChooseOptionSelectWarning from './alertDialog';
import { CircularProgress } from '@material-ui/core';
import './index.scss'
const ProjectCard = ({ data }: any) => {
	const dispatch = useDispatch()
	const history = useHistory()
	const [loading, setLoading] = useState(false)
	const [openDialog, setOpenDialog] = React.useState({
		open: false,
		id: '',
		loading: false
	});
	const handleClick = (id: string, proceed: boolean = false) => {
		if (history.location.pathname === '/projects' && !proceed) {
			setOpenDialog({ open: true, id, loading: false })
		} else {
			setLoading(true)
			localStorage.setItem('project', id)
			dispatch(chooseProject(id, history))
		}
	}

	const project = localStorage.getItem('project')
	return (
		<div className='people-main'>
			<PaperComponent square className={`paper-component ${history.location.pathname !== '/projects' ? "pointer" : data._id !== project && "pointer"}`} style={{ padding: 0, boxShadow: data._id !== project ? 'none' : "3px 3px 3px #01579b, -3px -3px 3px #01579b" }}>
				<div onClick={() => {
					if (history.location.pathname !== '/projects') {
						handleClick(data._id)
					} else {
						data._id !== project && handleClick(data._id)
					}
				}} className='p-20'>
					{
						loading && (
							<div className='loading-circ-proj'>
								<CircularProgress />
							</div>
						)
					}

					{history.location.pathname !== '/projects' && data._id === project && <span style={{ color: '#fb8c00', fontWeight: 600, textDecoration: 'underline' }}>Recent Working Project</span>}
					<div className='avatar-style'>
						<AvatarComponent
							alt='Project Avatar'
							src={data.image ? data.image : image}
							className='avatar-size '
							variant='square'
						/>
					</div>
					<Divider />
					<h3 className='ellipsis'>{data.name}</h3>
					<ButtonComponent label={data.creator.username} variant='contained' color='primary' />
				</div>

			</PaperComponent >

			<ChooseOptionSelectWarning open={openDialog.open} loading={openDialog.loading} id={openDialog.id} setOpen={setOpenDialog} handleClick={handleClick} />

		</div >
	);
};

export default ProjectCard;
