import React, { Fragment, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { fetchNotificationAsyncAction } from './../../store/notification/asyncAction';
import Divider from '@material-ui/core/Divider';

import NotificationCard from './../../layout/header/notification/card';
import TypographyComponent from '../../components/typography';
import ButtonComponent from './../../components/button/index';
import { loadingNotification } from './../../store/notification/action';
import NotificationSkeleton from './../../layout/header/notification/skeleton';
const NotificationPage = () => {
  const dispatch = useDispatch()
  const { loading, notification, total } = useSelector((state: any) => state.notification)
  useEffect(() => {
    dispatch(loadingNotification(true))
    dispatch(fetchNotificationAsyncAction(0, 10))
  }, [dispatch])
  return (
    <div>
      <TypographyComponent label='Notifications' variant='h4' style={{ marginBottom: '5px' }} />
      <Divider style={{ marginBottom: '10px' }} />
      {
        loading ? (<NotificationSkeleton />) : (
          <>
            {
              notification && notification?.length && notification?.map((el: any, i: number) => (
                <Fragment key={i}>
                  <NotificationCard data={el} />
                </Fragment>
              ))
            }
            {
              total !== notification?.length && (
                <ButtonComponent
                  style={{ padding: '10px', width: '100%' }}
                  variant='contained'
                  label={`Load More ( ${notification?.length} / ${total} )`}
                  disabled={loading}
                  callback={() => dispatch(fetchNotificationAsyncAction(notification.length, 8, true))}
                />
              )
            }
          </>
        )
      }

    </div >
  )
}

export default NotificationPage
