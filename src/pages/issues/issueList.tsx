import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import AccordionComponent from '../../components/accordion';
import ButtonComponent from '../../components/button';
import SearchField from '../../components/search';
import { fetchGanttTasks } from '../../store/gantt-task/asyncAction';
import { fetchTaskLoading, modifyTasks } from '../../store/task/action';
import { fetchFilteringTasks, fetchTasks } from '../../store/task/asyncAction';
import CommonTaskTable from './../common/taskList/table';
const initialSelected = {
	all: false,
	my: false,
	reported: false,
	open: false,
	done: false,
};
const DEFAULT_PAGE = 1;
const DEFAULT_SIZE = 30;
const IssueList = ({ label, socket }: any) => {
	const dispatch = useDispatch();
	const history = useHistory();
	const { loading, tasks, totalCount } = useSelector((state: any) => state.task);
	const [keyword, setKeyword] = useState('');
	const [selected, setSelected] = useState(initialSelected);

	useEffect(() => {
		dispatch(fetchTasks({ page: pagination.page, pageSize: pagination.pageSize }));
		setSelected({ ...initialSelected, all: true });
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [dispatch]);

	const filterData = (keyword: any) => {
		return tasks && tasks.filter((task: any) => {
			return task.title.toLowerCase().includes(keyword) || task.key.toLowerCase().includes(keyword);
		});
	};
	const handleAllIssue = (type: string | undefined) => {
		history.push(`/issues${type ? `?check=${type}` : ''}`);
	};

	const path = new URLSearchParams(history.location.search).get('check');
	useEffect(() => {
		setPagination({
			page: DEFAULT_PAGE,
			pageSize: DEFAULT_SIZE,
		})
	}, [path])
	useEffect(() => {
		dispatch(fetchTaskLoading(true));
		if (path) {
			setSelected({ ...initialSelected, [path]: true });
			dispatch(fetchFilteringTasks(`${path}`, { page: DEFAULT_PAGE, pageSize: DEFAULT_SIZE }));
		} else {
			dispatch(fetchTasks({ page: DEFAULT_PAGE, pageSize: DEFAULT_SIZE }));
			setSelected({ ...initialSelected, all: true });
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [history, path]);


	const [pagination, setPagination] = useState({
		page: DEFAULT_PAGE,
		pageSize: DEFAULT_SIZE,
	})

	const getData = async (page: number, pageSize: number) => {
		const pagination = { page: page, pageSize: pageSize }
		if (path) {
			dispatch(fetchFilteringTasks(`${path}`, pagination));
		} else {
			dispatch(fetchTasks(pagination));
		}
	};

	const onPageChange = (page: number, pageSize: number) => {
		getData(page, pageSize);
		setPagination({ ...pagination, page, pageSize })
	};
	useEffect(() => {
		dispatch(fetchGanttTasks());
	}, [dispatch]);
	useEffect(() => {
		socket?.on("updateTasks", (data: any) => {
			dispatch(modifyTasks({ data, modifiedType: 'tasks' }));
		})
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [socket])

	return (
		<>
			<div className='flex-start'>
				<SearchField keyword={keyword} setKeyword={setKeyword} />

				<div className='flex-start gap-10 pl-10'>
					<ButtonComponent
						label='All Issues'
						variant={selected.all ? 'contained' : 'outlined'}
						color={selected.all ? 'primary' : 'default'}
						callback={() => handleAllIssue(undefined)}
					/>
					{/* <ButtonComponent
						label='My Open Issue'
						variant={selected.my ? 'contained' : 'outlined'}
						color={selected.my ? 'primary' : 'default'}
						callback={() => handleAllIssue('my')}
					/> */}
					<ButtonComponent
						label='Open Issue'
						variant={selected.open ? 'contained' : 'outlined'}
						color={selected.open ? 'primary' : 'default'}
						callback={() => handleAllIssue('open')}
					/>
					<ButtonComponent
						label='Reported Issue'
						variant={selected.reported ? 'contained' : 'outlined'}
						color={selected.reported ? 'primary' : 'default'}
						callback={() => handleAllIssue('reported')}
					/>
					<ButtonComponent
						label='Done Issue'
						variant={selected.done ? 'contained' : 'outlined'}
						color={selected.done ? 'primary' : 'default'}
						callback={() => handleAllIssue('done')}
					/>
				</div>
			</div>
			<AccordionComponent header={label}>
				<CommonTaskTable
					data={filterData(keyword)}
					selectCheckbox={false}
					loading={loading}
					pagination={{
						current: pagination.page,
						showSizeChanger: false,
						defaultPageSize: pagination.pageSize,
						total: totalCount,
						onChange: (page: any, pageSize: any) => onPageChange(page, pageSize),
						showTotal: (total: any, range: any) =>
							`${range[0]}-${range[1]} of ${total} items`,

					}}

				/>
			</AccordionComponent>
		</>
	);
};

export default IssueList;
