import React, { useContext } from 'react';
import IssueList from './issueList';
import './index.scss';
import TypographyComponent from '../../components/typography';
import { SocketContext } from '../../context/socket';
const IssuesPage = () => {
	const socket: any = useContext(SocketContext);

	return (
		<div className='issue'>
			<TypographyComponent label='Issues' variant='h4' style={{ fontSize: '600', marginBottom: '10px' }} />
			<IssueList socket={socket} />
		</div>
	);
};

export default IssuesPage;
