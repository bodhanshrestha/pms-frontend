import React, { useState } from 'react';

import SignUp from './form';
import './index.scss';
import imgSrc from '../../../images/welcome-new-team.svg';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import TypographyComponent from '../../../components/typography';
import { useFormik } from 'formik';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { updateUserAsyncAction } from './../../../store/user/asyncAction';

import * as yup from 'yup';
const validationSchema = yup.object({
	username: yup.string().required('Username is required'),
	password: yup
		.string()
		.required('Please Enter your password')
		.matches(
			/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})/,
			'Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and one special case Character'
		),
	confirmPassword: yup
		.string()
		.required('Confirm Password is required')
		.oneOf([yup.ref('password'), null], 'Passwords must match'),
});
const WelcomeNewUser = () => {
	const dispatch = useDispatch();
	const history = useHistory();
	const [showPassword, setShowPassword] = useState(false);
	const [showConfirmPassword, setShowConfirmPassword] = useState(false);
	const [loading, setLoading] = useState(false)
	const handleShowPassword = () => {
		setShowPassword(!showPassword);
	};
	const handleConfirmShowPassword = () => {
		setShowConfirmPassword(!showConfirmPassword);
	};
	const formik = useFormik({
		initialValues: {
			name: '',
			username: '',
			password: '',
			showPassword: false,
			gender: '',
			confirmPassword: false,
			active: false,
		},
		validationSchema: validationSchema,
		onSubmit: (values) => {
			try {
				setLoading(true)
				const localUsername = localStorage.getItem('username');
				const hash = localStorage.getItem('hash');
				if (localUsername && hash) {
					values.active = true;
					dispatch(
						updateUserAsyncAction(
							values,
							localUsername,
							false,
							'login',
							history
						)
					);
				}
			} catch (e) {
				localStorage.clear();
				history.replace('/login');
			}
		},
	});
	return (
		<div className='vh-100'>
			<Grid container>
				<Grid item xs={8}>
					<img src={imgSrc} alt='teamWork' width='100%' />
				</Grid>
				<Grid item xs={4}>
					<div className='p-20 box-new'>
						<TypographyComponent
							label='Fill your information'
							className='pb-10'
							variant='h5'
						/>
						<Divider className='mb-10' />
						<form noValidate autoComplete='off' onSubmit={formik.handleSubmit}>
							<SignUp
								formik={formik}
								showPassword={showPassword}
								handleShowPassword={handleShowPassword}
								showConfirmPassword={showConfirmPassword}
								handleConfirmShowPassword={handleConfirmShowPassword}
								loading={loading}
							/>
						</form>
					</div>
				</Grid>
			</Grid>
		</div>
	);
};

export default WelcomeNewUser;
