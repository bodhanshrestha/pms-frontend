import React from 'react';

import IconButton from '@material-ui/core/IconButton';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import PersonIcon from '@material-ui/icons/Person';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import InputField from '../../../components/form/text';

import ButtonComponent from '../../../components/button';
import SelectInputField from './../../../components/form/select';

const NewUserSignUp = ({
	formik,
	showPassword,
	handleShowPassword,
	showConfirmPassword,
	handleConfirmShowPassword,
	loading
}: any): any => {
	return (
		<>
			<InputField
				label='Full Name'
				name='name'
				type='text'
				value={formik.values.name}
				callback={formik.handleChange}
				endAdornment={
					<IconButton>
						<PersonIcon />
					</IconButton>
				}
			/>

			<InputField
				label='Username'
				name='username'
				type='text'
				placeholder='Enter Your Username'
				value={formik.values.username}
				callback={formik.handleChange}
				error={formik.touched.username && Boolean(formik.errors.username)}
				helperText={formik.touched.username && formik.errors.username}
				endAdornment={
					<IconButton>
						<AccountCircleIcon />
					</IconButton>
				}
			/>
			<InputField
				label='Password'
				name='password'
				type={showPassword ? 'text' : 'password'}
				placeholder='Enter Your Password'
				value={formik.values.password}
				callback={formik.handleChange}
				error={formik.touched.password && Boolean(formik.errors.password)}
				helperText={formik.touched.password && formik.errors.password}
				endAdornment={
					<IconButton
						aria-label='toggle password visibility'
						onClick={handleShowPassword}
					>
						{showPassword ? <Visibility /> : <VisibilityOff />}
					</IconButton>
				}
			/>
			<InputField
				label='Confirm Password'
				name='confirmPassword'
				type={showConfirmPassword ? 'text' : 'password'}
				placeholder='Confirm Your Password'
				value={formik.values.confirmPassword}
				callback={formik.handleChange}
				error={
					formik.touched.confirmPassword &&
					Boolean(formik.errors.confirmPassword)
				}
				helperText={formik.errors.confirmPassword}
				endAdornment={
					<IconButton
						aria-label='toggle confirm password visibility'
						onClick={handleConfirmShowPassword}
					>
						{showConfirmPassword ? <Visibility /> : <VisibilityOff />}
					</IconButton>
				}
			/>
			<div style={{ textAlign: 'left' }}>
				<SelectInputField
					label='Select Gender'
					name='gender'
					value={formik.values.gender}
					callback={formik.handleChange}
					placeholder='Task'
					className='w-100'
					data={[{
						id: 'male',
						name: 'Male'
					}, {
						id: 'female',
						name: 'Female'
					}]}

				/>
			</div>
			<br />
			<ButtonComponent
				label='Join Now'
				type='submit'
				variant='contained'
				color='primary'
				className='w-100'
				disabled={loading}
			/>
		</>
	);
};
export default NewUserSignUp;
