import React from 'react';
import infoGraphics from '../../../images/Infographic-screen.svg';
import './style.scss';
import TypographyComponent from '../../../components/typography';
import ButtonComponent from '../../../components/button';
import { useHistory } from 'react-router';
import { useSelector } from 'react-redux';

const StartUp = () => {
	const history = useHistory();
	const project = useSelector((state: any) => state.project)
	const handleCLick = () => {
		history.push('/dashboard');
	};

	return (
		<div className='startup'>
			<div className='project-body'>
				<img src={infoGraphics} alt='info graphics' />
				<TypographyComponent label={project?.name} variant='h5' color='secondary' style={{ fontWeight: '600', textDecoration: 'underline' }} />
				<TypographyComponent
					label={<span>
						<span style={{ color: '#ab003c', fontWeight: 600 }} >
							"{project?.leader}"{" "}
						</span>
						Has Invited You To Join The Team
					</span>}
					variant='h4'
				/>
				<ButtonComponent
					label='join now'
					variant='outlined'
					color='primary'
					size='medium'
					callback={handleCLick}
				/>
			</div>
		</div>
	);
};

export default StartUp;
