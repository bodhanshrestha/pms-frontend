import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import Welcome from '../../../images/Welcome.svg';
import './style.scss';
import Share from '../../../images/share.svg';
import Report from '../../../images/report.svg';
import Task from '../../../images/task.svg';
import Group from '../../../images/group.svg';
import CircleGroup from '../../../images/circle-group.svg';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import ButtonComponent from '../../../components/button';
import { useDispatch } from 'react-redux';
import { initialExternalInvitedLogin } from '../../../store/auth/asyncAction';

const WelcomePage = () => {
	const history = useHistory();
	const dispatch = useDispatch();
	let params = new URLSearchParams(history.location.search);
	const newUser = params.get('new');
	const hash = params.get('hash');
	if (hash) localStorage.setItem('hash', hash);
	const [loading, setLoading] = useState(false)
	const handleCLick = () => {
		setLoading(true)
		if (!hash) {
			return history.replace("/login")
		} else {
			dispatch(initialExternalInvitedLogin(hash, Boolean(newUser), history));
		}
	};
	return (
		<div className='welcome'>
			<div className='welcome-body'>
				<img src={Welcome} alt='welcome' className='welcome-image' />
				<ButtonComponent
					label="let's get started"
					variant='outlined'
					size='medium'
					className='button-st'
					color='primary'
					callback={handleCLick}
					disabled={loading}
					endIcon={<ArrowForwardIcon />}
				/>
				<div className='icons'>
					<img src={Share} alt='Share' />
					<img src={Report} alt='Report' />
					<img src={Group} alt='Group' />
					<img src={Task} alt='Task' />
					<img src={CircleGroup} alt='Groups' />
				</div>
			</div>
		</div>
	);
};

export default WelcomePage;
