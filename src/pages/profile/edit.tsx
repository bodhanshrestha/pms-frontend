import React, { useEffect } from 'react';
import InputField from '../../components/form/text';
import { IconButton } from '@material-ui/core';
import LocationOnIcon from '@material-ui/icons/LocationOn';

import PhoneIcon from '@material-ui/icons/Phone';

const EditProfile = ({ formik, user }: any) => {
	useEffect(() => {
		formik.setValues(user);
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);
	return (
		<div className='pt-20'>
			<InputField
				name='name'
				label='Full Name'
				variant='outlined'
				width='100%'
				value={formik.values.name}
				callback={formik.handleChange}
			/>
			<InputField
				name='address'
				label='Address'
				variant='outlined'
				width='100%'
				value={formik.values.address}
				callback={formik.handleChange}
				endAdornment={
					<IconButton>
						<LocationOnIcon />
					</IconButton>
				}
			/>

			<InputField
				name='phone_number'
				label='Phone'
				variant='outlined'
				width='100%'
				value={formik.values.phone_number}
				callback={formik.handleChange}
				endAdornment={
					<IconButton>
						<PhoneIcon />
					</IconButton>
				}
			/>

			<InputField
				name='color'
				label='Color'
				variant='outlined'
				width='100%'
				value={formik.values.color}
				callback={formik.handleChange}
			/>
		</div>
	);
};

export default EditProfile;
