import React, { Fragment, useEffect, useState } from 'react';

import { useFormik } from 'formik';

import { useDispatch } from 'react-redux';

import { useSelector } from 'react-redux';

import './style.scss';
import DisplaySetting from './displaySetting';
import Main from './main';
import {
	fetchUserAsyncAction,
	updateUserAsyncAction,
} from '../../store/user/asyncAction';
import { useLocation } from 'react-router';
import ProfileSkeleton from './skeleton';
import { loadingQueryUser } from '../../store/user/action';

const ProfilePage = (): any => {
	const dispatch = useDispatch();
	const location = useLocation();
	const [loading, setLoading] = useState(false);
	const [isQueryUser, setQueryUser] = useState(false);
	const { user, queryUser, loadQueryUser } = useSelector((state: any) => state.user);
	const formik = useFormik({
		initialValues: {
			address: '',
			color: '',
			phone_number: '',
		},
		onSubmit: (values) => {
			dispatch(updateUserAsyncAction(values, user.username, true));
			setEdit(false);
		},
	});
	const [edit, setEdit] = useState(false);
	const handleSettingIcon = () => {
		setEdit(!edit);
	};

	useEffect(() => {
		setLoading(true)
		let username: string | undefined | null;
		let params: any = new URLSearchParams(location.search);
		const queryUser = params.get('username');
		if (queryUser) {
			dispatch(loadingQueryUser(true))
			username = queryUser;
			setQueryUser(true);
			if (username) {
				dispatch(fetchUserAsyncAction(username, true));
			}
		} else {
			username = localStorage.getItem('username');
			if (username) dispatch(fetchUserAsyncAction(username));
			setQueryUser(false);
		}
		setLoading(false)

		return () => {
			setQueryUser(false);
		};
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [location.pathname, dispatch]);
	return (
		<div>
			{
				!loading ? (
					<>
						{(user && queryUser && !loadQueryUser) ? (
							<Fragment>
								<Main
									edit={edit}
									queryUser={isQueryUser}
									handleSettingIcon={handleSettingIcon}
									formik={formik}
									user={queryUser}
								/>
								{!isQueryUser && <DisplaySetting />}
							</Fragment>
						) : user && !loadQueryUser ? (
							<Fragment>
								<Main
									edit={edit}
									queryUser={isQueryUser}
									handleSettingIcon={handleSettingIcon}
									formik={formik}
									user={user}
								/>
								{!isQueryUser && <DisplaySetting />}
							</Fragment>
						) : (
							<ProfileSkeleton />
						)
						}
					</>
				) : (
					<ProfileSkeleton />
				)
			}
		</div>
	);
};

export default ProfilePage;
