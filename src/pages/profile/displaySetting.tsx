import { Paper } from '@material-ui/core';
import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import SelectInputField from '../../components/form/select';
import SwitchComponent from '../../components/switch';
import TypographyComponent from '../../components/typography';
import { changeMode } from '../../store/user/action';
import { updateUserAsyncAction } from '../../store/user/asyncAction';

const DisplaySetting = () => {
	const [language, setLanguage] = useState('');
	const { user } = useSelector((state: any) => state.user);
	const dispatch = useDispatch();

	const handleSwitch = (e: any) => {
		dispatch(changeMode(e.target.checked))
		dispatch(
			updateUserAsyncAction(
				{ dark_mode: e.target.checked },
				user.username,
				false,
				'change_mode'
			)
		);
	};
	const handleLanguageChange = (e: any) => {
		setLanguage(e.target.value);
		dispatch(
			updateUserAsyncAction({ language: e.target.value }, user.username, false)
		);
	};
	useEffect(() => {
		setLanguage(user.language);
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);
	return (
		<Paper className='p-20 w-50' style={{ minWidth: '450px' }}>
			<div className='display-settings'>
				<TypographyComponent label='Display Settings' variant='h5' />
				<hr />
			</div>
			<div className='select-field'>
				<SelectInputField
					className='languages'
					label='Language'
					name='language'
					width='24.5%'
					value={language}
					callback={handleLanguageChange}
					data={[
						// { value: 'nep', label: 'Nepali' },
						{ value: 'en', label: 'English' },
					]}
				/>
			</div>
			<div className='switch-theme'>
				<TypographyComponent label='Theme' className='theme-typo' />
				<SwitchComponent
					color='primary'
					size='medium'
					checked={user.dark_mode ? true : false}
					callback={handleSwitch}
				/>
			</div>
		</Paper>
	);
};

export default DisplaySetting;
