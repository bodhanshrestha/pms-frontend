import React from 'react';
import Skeleton from '@material-ui/lab/Skeleton';
import { Divider } from '@material-ui/core';
import { Paper } from '@material-ui/core';
const ProfileSkeleton = () => {
  return <Paper elevation={3} style={{ width: '450px', padding: '20px' }}>
    <div>
      <Skeleton variant="rect" width={210} height={20} />
    </div>
    <Divider style={{ margin: '15px 0px 10px 0px' }} />
    <div style={{ display: 'flex', justifyContent: 'space-between', gap: '20px' }}>
      <div>
        <Skeleton variant="rect" width={200} height={20} style={{ marginBottom: '5px' }} />
        <Skeleton variant="rect" width={200} height={20} style={{ marginBottom: '5px' }} />
        <Skeleton variant="rect" width={200} height={20} style={{ marginBottom: '5px' }} />
        <Skeleton variant="rect" width={200} height={20} style={{ marginBottom: '5px' }} />
        <Skeleton variant="rect" width={200} height={20} style={{ marginBottom: '5px' }} />
      </div>
      <div>
        <Skeleton variant="rect" width={150} height={120} />
      </div>
    </div>
  </Paper>
};

export default ProfileSkeleton;
