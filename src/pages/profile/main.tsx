import { Paper } from '@material-ui/core';
import React from 'react';
import TypographyComponent from '../../components/typography';
import EditProfile from './edit';
import DisplayProfile from './display';
import Settings from '@material-ui/icons/Settings';
import Close from '@material-ui/icons/Close';
import DoneOutlineIcon from '@material-ui/icons/DoneOutline';
import Grid from '@material-ui/core/Grid';
import AvatarComponent from '../../components/avatar';
import SpinningLoader from '../../components/circularProgress';
const Main = ({ edit, handleSettingIcon, formik, user, queryUser }: any) => {
	return (
		<Paper className='p-20 mb-10' style={{ width: '500px' }}>
			{
				!user ? (
					<SpinningLoader />
				) : (
					<>
						<div className='space-between'>
							<TypographyComponent
								label={!edit ? 'Profile' : 'Edit Profile'}
								variant='h4'
								className='typo'
							/>
							{
								!queryUser && (
									<>
										{!edit ? (
											<div>
												<Settings className='pointer' onClick={handleSettingIcon} />
											</div>
										) : (
											<div className='flex-end gap-20'>
												<DoneOutlineIcon
													color='primary'
													className='pointer'
													onClick={formik.handleSubmit}
												/>
												<Close className='pointer' onClick={handleSettingIcon} />
											</div>
										)}
									</>
								)
							}

						</div>
						<hr />
						<Grid container spacing={2}>
							<Grid item xs={8}>
								{!edit ? (
									<DisplayProfile user={user} />
								) : (
									<form noValidate autoComplete='off' onSubmit={formik.handleSubmit}>
										<EditProfile formik={formik} user={user} />
									</form>
								)}
							</Grid>
							<Grid item xs={4}>
								<AvatarComponent
									src={user && user.avatar}
									variant='square'
									className='avatar-image'
								/>
							</Grid>
						</Grid>
					</>
				)
			}

		</Paper >
	);
};

export default Main;
