import React from 'react';
import TypographyComponent from '../../components/typography';

const DisplayProfile = ({ user }: any) => {
	const data = [
		{
			leftLabel: 'Full Name',
			rightLabel: user?.name,
		},
		{
			leftLabel: 'Username',
			rightLabel: user?.username,
		},
		{
			leftLabel: 'Email',
			rightLabel: user?.email,
		},
		{
			leftLabel: 'Address',
			rightLabel: user?.address,
		},
		{
			leftLabel: 'Phone Number',
			rightLabel: user?.phone_number,
		},
	];
	return (
		<div className='p-10 pt-20'>
			{data.map((proj: any, i: number) => (
				<div className='flex-start' key={i}>
					<TypographyComponent
						label={proj.leftLabel}
						style={{ paddingRight: '5px' }}
					/>
					:
					<TypographyComponent
						label={proj.rightLabel}
						style={{ paddingLeft: '5px' }}
					/>
				</div>
			))}
		</div>
	);
};

export default DisplayProfile;
