import React, { useEffect, useState } from 'react';
import AvatarGroupComponent from '../../components/avatarGroup';

const FilterUser = ({ task }: any) => {
	const [users, setUsers] = useState<any>([]);
	useEffect(() => {
		let data: any = [];
		for (const i in task) {
			if (task[i].assigne) {
				const prevUser = data.filter((e: any) => e._id === task[i].assigne._id);
				if (prevUser.length <= 0) data.push(task[i].assigne);
			}
		}
		setUsers(data);
		return () => { };
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [task]);
	return <AvatarGroupComponent max={50} users={users} />;
};

export default FilterUser;
