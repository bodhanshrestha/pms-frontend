import React from 'react';

import InputField from '../../components/form/text';

const SprintForm = ({ formik }: any) => {
	return (
		<>
			<InputField
				label='Name'
				name='name'
				type='text'
				placeholder='Enter Sprint Name'
				value={formik.values.name}
				callback={formik.handleChange}
			/>
			<InputField
				label='Starting Date'
				name='starting_date'
				type='date'
				shrink={true}
				value={formik.values.starting_date}
				callback={formik.handleChange}
				error={formik.touched.starting_date && Boolean(formik.errors.starting_date)}
				helperText={formik.touched.starting_date && formik.errors.starting_date}
			/>
			<InputField
				label='Closing Date'
				name='closing_date'
				type='date'
				shrink={true}
				value={formik.values.closing_date}
				callback={formik.handleChange}
				error={formik.touched.closing_date && Boolean(formik.errors.closing_date)}
				helperText={formik.touched.closing_date && formik.errors.closing_date}
			/>
		</>
	);
};
export default SprintForm;
