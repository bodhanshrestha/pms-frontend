import React, { useContext, useEffect } from 'react';

import SprintTableList from './table';
import './index.scss';
import { useDispatch, useSelector } from 'react-redux';
import { setCurrentSprint } from '../../store/sprint/asyncAction';
import SprintAction from './action';
import CloseSprint from './close';

import FilterUser from './avatars';
import TypographyComponent from '../../components/typography';
import SprintSkeleton from './skeleton';
import { SocketContext } from '../../context/socket';
export const getRole = (team: any) => {
	const username = localStorage.getItem('username')
	const data = team?.filter((el: any) => el.member.username === username)[0]?.role
	return data
}
const SprintPage = () => {
	const sprint = useSelector((state: any) => state.sprint);
	const { team } = useSelector((state: any) => state.project);
	const dispatch = useDispatch();
	useEffect(() => {
		dispatch(setCurrentSprint());
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [dispatch]);

	const socket: any = useContext(SocketContext);

	useEffect(() => {
		socket?.on("updateSprint", (data: any) => {
			if (data.type === 'close') {
				return window.location.reload();
			}
			dispatch(setCurrentSprint());
		})
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [socket])
	return (
		<div className='sprint'>
			{
				sprint ? (
					<>
						<div className='flexed'>
							<TypographyComponent
								variant='h4'
								label={sprint.name ? sprint.name : 'Sprint Page'}
								className='mb-10'
							/>
							{sprint && !sprint._id && (team && getRole(team) === 'manager') && (
								<SprintAction type='start' socket={socket} />
							)}
							{sprint && sprint._id && (team && getRole(team) === 'manager') && (
								<div className='flex-center gap-10'>
									<div>
										<SprintAction type='edit' socket={socket} loading={sprint.loading} values={{ _id: sprint._id, name: sprint.name, starting_date: sprint.starting_date, closing_date: sprint.closing_date }} />
									</div>
									<div>
										<CloseSprint id={sprint._id} socket={socket} />
									</div>
								</div>
							)}
						</div>
						{sprint && sprint._id && (
							<>
								<FilterUser task={sprint.task} />
								<SprintTableList label='Current Sprint Tasks' data={sprint.task} socket={socket} />
							</>
						)}
						{sprint && !sprint._id && (team && getRole(team) !== 'manager') && (
							<TypographyComponent
								color='secondary'
								label='Sprint is not started yet. Inform your Team manager to access the sprint.'
							/>
						)}
					</>
				) : (
					<SprintSkeleton />
				)
			}

		</div>
	);
};

export default SprintPage;
