import React from 'react';
import { useDispatch } from 'react-redux';

// import { useDispatch } from 'react-redux';

import DialogComponent from '../../components/dialog';
import { closeSprintAsyncAction } from '../../store/sprint/asyncAction';

const CloseSprint = ({ id, socket }: { id: string, socket: any }) => {
	const dispatch = useDispatch();
	const [open, setOpen] = React.useState(false);
	const onSubmit = () => {
		dispatch(closeSprintAsyncAction(id, socket))
		setOpen(false)
	};

	return (
		<DialogComponent
			title='Close Sprint'
			action={{
				cancelBtn: 'Cancel',
				submitBtn: 'Close This Sprint',
			}}
			open={open}
			setOpen={setOpen}
			onSubmit={onSubmit}
			btn={{
				label: 'Close Sprint',
				variant: 'contained',
			}}
		>
			<h1>Are You Sure You Want To Close This Sprint</h1>
		</DialogComponent>
	);
};

export default CloseSprint;
