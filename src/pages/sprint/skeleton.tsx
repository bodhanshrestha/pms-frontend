import React from 'react';
import Skeleton from '@material-ui/lab/Skeleton';
const SprintSkeleton = () => {
  return (
    <div style={{ marginTop: '10px' }}>
      <Skeleton variant="rect" width={210} height={20} />
      <div style={{ display: 'flex', justifyContent: 'flex-start', gap: '20px', padding: '20px 0px' }}>
        <div>
          <Skeleton variant="rect" width={210} height={40} />
        </div>
        <div>
          <Skeleton variant="rect" width={210} height={40} />
        </div>

      </div>
      <Skeleton variant="rect" width={"100%"} height={400} style={{ marginTop: '10px' }} />
    </div>
  )
};

export default SprintSkeleton;
