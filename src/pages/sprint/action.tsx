import React, { useEffect } from 'react';

import { useDispatch } from 'react-redux';

import { useFormik } from 'formik';
import SprintForm from './form';
import { createSprint } from '../../store/sprint/asyncAction';
import DialogComponent from '../../components/dialog';
import { UpdateSprint } from './../../store/sprint/asyncAction';
import * as yup from 'yup';
import { setSnackbar } from './../../store/snackbar/action';
import { loadingSprint } from './../../store/sprint/action';
import { compareTwoArraysAsSameOrNot } from '../../utils/functions';
const getDays = (d1: string, d2: string) => {
	var date1 = new Date(d1);
	var date2 = new Date(d2);

	// To calculate the time difference of two dates
	var Difference_In_Time = date2.getTime() - date1.getTime();

	// To calculate the no. of days between two dates
	var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
	return Difference_In_Days
}
const validationSchema = yup.object({
	starting_date: yup
		.string()
		.required('Starting Date is required'),
	closing_date: yup.string().required('Closing Date is required'),

});

const SprintAction = ({ type, values = {}, loading, socket }: any) => {
	const dispatch = useDispatch();
	const [open, setOpen] = React.useState(false);

	const formik = useFormik({
		initialValues: {
			_id: '',
			name: '',
			starting_date: '',
			closing_date: '',
		},
		validationSchema: validationSchema,
		onSubmit: async (value) => {

			const days = getDays(value.starting_date, value.closing_date)
			if (days > 30) {
				formik.setFieldTouched('closing_date', false, true)
				formik.setFieldError('closing_date', 'Error')
				dispatch(setSnackbar({ open: true, type: 'error', message: 'Date should not exceed a month' }))
				return
			}
			dispatch(loadingSprint(true))
			if (type === 'edit') {
				if (!compareTwoArraysAsSameOrNot(values, value)) {
					dispatch(UpdateSprint(value._id, value, true, setOpen, socket))
				} else {
					setOpen(false)
					dispatch(loadingSprint(false))
				}
			}
			else {
				dispatch(createSprint(value, setOpen, socket))
				formik.resetForm()
			};
		},
	});


	useEffect(() => {
		if (type === 'edit') formik.setValues({ ...values });
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);
	return (
		<form noValidate autoComplete='off'>
			<DialogComponent
				title={`${type === 'edit' ? 'Edit Sprint' : 'Create Sprint'}`}
				action={{
					cancelBtn: 'Cancel',
					submitBtn: `${type === 'edit' ? 'Update' : 'Create'}`,
				}}
				open={open}
				setOpen={setOpen}
				onSubmit={formik.handleSubmit}
				formik={formik}
				btn={{
					label: `${type === 'edit' ? 'Edit Sprint' : 'Start Sprint'}`,
					variant: 'contained',
				}}
				loading={loading}
			>
				<SprintForm formik={formik} />
			</DialogComponent>
		</form>
	);
};

export default SprintAction;
