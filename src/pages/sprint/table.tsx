import React, { useEffect } from 'react';

import './antd.css';

import AccordionComponent from '../../components/accordion';
import CommonTaskTable from '../common/taskList/table';
import { useDispatch } from 'react-redux';
import { modifySprintTasks } from './../../store/sprint/action';
import { setCurrentSprint } from '../../store/sprint/asyncAction';

const SprintTableList = ({ label, data, socket }: any) => {
	const dispatch = useDispatch()
	useEffect(() => {
		socket?.on("updateSprintTasks", (data: any) => {
			dispatch(modifySprintTasks({ data, modifiedType: 'tasks' }));
		})
		socket?.on("updateTasks", (data: any) => {
			if (data.type !== 'create') dispatch(modifySprintTasks({ data, modifiedType: 'tasks' }));
		})
		socket?.on("updateSprint", (data: any) => {
			dispatch(setCurrentSprint());
		})
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [socket])
	return (
		<AccordionComponent header={label}>
			<CommonTaskTable data={data} />
		</AccordionComponent>
	);
};

export default SprintTableList;
