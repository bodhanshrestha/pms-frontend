import { useFormik } from 'formik';
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import DialogComponent from '../../components/dialog';
import { UpdateSprint } from '../../store/sprint/asyncAction';
import SprintForm from './form';
import { loadingSprint } from './../../store/sprint/action';

const EditSprint = () => {
	const dispatch = useDispatch();
	const sprint = useSelector((state: any) => state.sprint);
	const [newData, setNewData] = useState({
		name: '',
		starting_date: '',
		closing_date: '',
	});
	const handleSubmit = () => {
		dispatch(loadingSprint(true))
		// dispatch(UpdateSprint(sprint._id, newData, true));
	};
	const handleChange = (e: any) => {
		setNewData({ ...newData, [e.target.name]: e.target.value });
	};
	return (
		<DialogComponent
			title='Edit Sprint'
			action={{
				cancelBtn: 'Cancel',
				submitBtn: 'Submit',
			}}
			onSubmit={handleSubmit}
			btn={{
				label: 'Edit Sprint',
				variant: 'contained',
				loading: sprint.loading
			}}>
			<SprintForm
				handleSubmit={handleSubmit}
				sprint={sprint}
				handleChange={handleChange}
				newData={newData}
				setNewData={setNewData}
			/>
		</DialogComponent>
	);
};

export default EditSprint;
