import React, { useEffect, useState } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import TypographyComponent from '../../../components/typography';
import PaperComponent from '../../../components/paper';
import List from '@material-ui/core/List';
import Grid from '@material-ui/core/Grid';
import { Link } from 'react-router-dom'

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: '100%',
    backgroundColor: theme.palette.background.paper,
    padding: '10px'
  },
  nested: {
    paddingLeft: theme.spacing(4),
  },
  p_5: {
    padding: theme.spacing(1.5),
  },
  pb_5: {
    paddingBottom: theme.spacing(1.5),
  },
  mr_5: {
    marginRight: theme.spacing(1.5),
  },
  m_5: {
    margin: '10px 0px',
  },
}));

export const splitTime = (time: string) => {
  if (time) {

    const splited: any = time.split(' ');
    let hourToSecond: any;
    let minuteToSecond: any;
    if (splited[0]) {
      if (splited[0].includes('h')) {
        hourToSecond = Number(splited[0].match(/\d+/g)[0]) * 60;
      } else {
        hourToSecond = Number(splited[0].match(/\d+/g)[0]);
      }
    }
    if (splited[1]) {
      if (splited[1].includes('m')) {
        minuteToSecond = Number(splited[1].match(/\d+/g)[0]);
      }
    } else {
      minuteToSecond = 0;
    }
    return hourToSecond + minuteToSecond;
  }
  return 0
};
export const groupData = (data: any) => {
  var newData: any = {};
  for (const item in data) {
    if (newData[data[item].username]) {
      newData[data[item].username].push(data[item]);
    } else {
      newData[data[item].username] = [data[item]];
    }
  }

  return newData;
};
export function secondsToDhms(seconds: any) {
  seconds = Number(seconds);
  const d = Math.floor(seconds / (60 * 24));
  const h = Math.floor((seconds % (60 * 24)) / 60);
  var m = Math.floor((seconds % 60));
  const dDisplay = d > 0 ? d + (d === 1 ? ' day, ' : ' days, ') : '';
  const hDisplay = h > 0 ? h + (h === 1 ? ' hour ' : ' hours ') : '';
  var mDisplay = m > 0 ? m + (m === 1 ? ' minute ' : ' minutes ') : '';

  return dDisplay + hDisplay + mDisplay;
}
const SprintReportList = ({ data, sprintId }: any) => {
  const [open, setOpen] = React.useState(false);
  const [openTask, setOpenTask] = React.useState(false);
  const [openUsers, setOpenUsers] = React.useState(true);
  const classes = useStyles();
  const [report, setReport] = useState([])
  const [reportSprint, setReportSprint] = useState([])
  const generateUserReport = (data: any) => {
    let initialData: any = []
    data.forEach((el: any) => {
      if (el.time_tracking.length) {
        const filtered: any = el.time_tracking.filter((el: any) => el.sprint === sprintId)
        return filtered && filtered.length && filtered.forEach((el: any) => initialData.push({
          username: el?.user?.username,
          email: el?.user?.email,
          time: splitTime(el?.time)
        }))
      }
      return false
    })
    const result = groupData(initialData)
    let finalResult: any = []
    for (const [, value] of Object.entries(result)) {
      let valueData: any = value
      const result = valueData.reduce((prev: any, next: any) => {
        return {
          ...prev,
          time: prev.time + next.time
        }
      })
      finalResult.push({ ...result })
    }
    setReport(finalResult)
  }
  const generateTasksReport = (data: any) => {
    const result = data.map((el: any) => {
      if (el.time_tracking.length) {
        const filtered: any = el.time_tracking.filter((el: any) => el.sprint === sprintId)
        if (filtered.length === 1) {
          return {
            _id: el._id,
            title: el.title,
            time: secondsToDhms(splitTime(el.time_tracking[0].time))
          }
        } else if (filtered.length > 1) {
          return filtered.reduce((prev: any, next: any) => {
            const prevTime = splitTime(prev.time)
            const nextTime = splitTime(next.time)
            const totalTime = prevTime + nextTime
            return {
              _id: el._id,
              title: el.title,
              time: secondsToDhms(totalTime)
            }
          })
        } else {
          return {
            _id: el._id,
            title: el.title,
            time: 'N/A'
          }
        }
      }
      return {
        _id: el._id,
        title: el.title,
        time: 'N/A'
      }
    })
    setReportSprint(result)
  }
  const handleClick = () => {
    setOpen(!open);
  };
  const handleClickTask = () => {
    setOpenTask(!openTask);
  };
  const handleClickUsers = () => {
    setOpenUsers(!openUsers);
  };
  useEffect(() => {
    generateUserReport(data?.task)
    generateTasksReport(data?.task)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <PaperComponent className={`${classes.m_5}`}>
      <ListItem button onClick={handleClick}>
        <ListItemText primary={<TypographyComponent label={data.name} variant='h6' />} />
        {open ? <ExpandLess className={classes.mr_5} /> : <ExpandMore className={classes.mr_5} />}
      </ListItem>
      <Collapse in={open} timeout="auto" unmountOnExit>
        <Grid container className={classes.pb_5} >
          <Grid item xs={6} style={{ borderRight: '1px solid #eeeeee' }} >

            <List component="div" disablePadding>
              <ListItem className={classes.nested}>
                <TypographyComponent label='Starting Date' />
                <span> : </span>
                <TypographyComponent label={data.starting_date} />

              </ListItem>
              <ListItem className={classes.nested}>
                <TypographyComponent label='Closing Date' />
                <span> : </span>
                <TypographyComponent label={data.closing_date} />

              </ListItem>

            </List>
          </Grid>
          <Grid item xs={6} className="listing-sprint">

            <PaperComponent style={{ margin: '5px 32px 0 20px' }}>
              <ListItem button onClick={handleClickUsers}>
                <ListItemText primary={<TypographyComponent label="Active Users" variant='h6' />} />
                {openUsers ? <ExpandLess className={classes.mr_5} /> : <ExpandMore className={classes.mr_5} />}
              </ListItem>
              <Collapse in={openUsers} timeout="auto" unmountOnExit>
                {
                  report && report.length ? report.map((el: any, i: number) => (
                    <ListItem className={classes.nested} style={{ display: 'block' }}>
                      <div className='space-between' style={{ padding: '0px 10px 0px 0px' }}>
                        <div>
                          <TypographyComponent label={el.username} />
                        </div>
                        <div>

                          <TypographyComponent label={secondsToDhms(el.time)} />
                        </div>
                      </div>
                    </ListItem>
                  )) : <ListItem className={classes.nested}>
                    <TypographyComponent label='None' />
                  </ListItem>
                }
              </Collapse>
            </PaperComponent>
          </Grid>
          <Grid item xs={12} className="listing-sprint">
            <PaperComponent style={{ margin: '10px 32px 0 32px' }}>
              <ListItem button onClick={handleClickTask}>
                <ListItemText primary={<TypographyComponent label="Tasks" variant='h6' />} />
                {openTask ? <ExpandLess className={classes.mr_5} /> : <ExpandMore className={classes.mr_5} />}
              </ListItem>
              <Collapse in={openTask} timeout="auto" unmountOnExit  >
                {
                  reportSprint.length && reportSprint.map((el: any, i: number) => (
                    <Link to={`/task/${el._id}`} key={i} className='w-100' style={{ display: 'block' }}>
                      <ListItem button className={`${classes.nested} w-100`} key={i} style={{ display: 'block' }}>
                        <div className='space-between'>
                          <div>

                            <TypographyComponent label={el.title} className={classes.p_5} />
                          </div>
                          {/* <div>

                            <TypographyComponent label={el.time} className={classes.p_5} />
                          </div> */}
                        </div>
                      </ListItem>
                    </Link>
                  ))
                }

              </Collapse>
            </PaperComponent>
          </Grid>
        </Grid>
      </Collapse>
    </PaperComponent>
  )
}

export default SprintReportList
