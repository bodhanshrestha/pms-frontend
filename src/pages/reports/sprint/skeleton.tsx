import React from 'react';
import Skeleton from '@material-ui/lab/Skeleton';
import PaperComponent from '../../../components/paper';
import { Divider } from '@material-ui/core';

const SprintSkeleton = () => {
  return <PaperComponent style={{ marginTop: '10px', maxWidth: '700px', padding: '20px' }}>
    <Skeleton variant="rect" width={350} height={20} />
    <Divider style={{ margin: '20px 0px' }} />
    <div className='space-between'>
      <div>
        <Skeleton variant="rect" width={210} height={20} />
      </div>
      <div className='flex-end'>
        <Skeleton variant="rect" width={50} height={20} style={{ marginRight: '40px' }} />
      </div>
    </div>
    <Skeleton variant="rect" width={"100%"} height={200} style={{ marginTop: '20px' }} />
  </PaperComponent>
};

export default SprintSkeleton;
