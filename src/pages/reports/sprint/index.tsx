import React, { useEffect, Fragment } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import Grid from '@material-ui/core/Grid';
import TypographyComponent from '../../../components/typography';
import PaperComponent from '../../../components/paper';
import Divider from '@material-ui/core/Divider';
import SprintReportList from './list';
import './index.scss'
import { useDispatch, useSelector } from 'react-redux';
import { setDetailSprint } from './../../../store/sprint/asyncAction';
import SprintSkeleton from './skeleton';
const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: '100%',
    backgroundColor: theme.palette.background.paper,
    padding: '10px'
  },
  nested: {
    paddingLeft: theme.spacing(4),
  },
  padding_5: {
    padding: theme.spacing(1.5),
    margin: `10px 0px`
  },
  m_5: {
    margin: '10px 0px',
  },
}));

export default function SprintReport() {
  const classes = useStyles();
  const { details } = useSelector((state: any) => state.sprint);
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(setDetailSprint());
  }, [dispatch]);
  return (
    <>
      {
        details ? (
          <div style={{ maxWidth: '700px' }}>
            <PaperComponent className={classes.padding_5}>
              <List
                component="nav"
                aria-labelledby="nested-list-subheader"
                subheader={
                  <>
                    <TypographyComponent label='Sprint Report' variant='h4' style={{ paddingBottom: '10px' }} />
                    <Divider />
                  </>
                }
              >
                <Grid container
                // spacing={2}
                >
                  {
                    details && details.length ? details.map((el: any, i: number) => (
                      <Grid item key={i} xs={12} >
                        <SprintReportList data={el} sprintId={el._id} />
                      </Grid>
                    )) : (
                      <Grid item xs={12} style={{ marginTop: '20px' }} >
                        <TypographyComponent label='Sprint is not available' />
                      </Grid>
                    )
                  }
                </Grid>
              </List>
            </PaperComponent >
          </div>
        ) : (
          <SprintSkeleton />
        )
      }
    </>
  );
}
