import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation } from 'react-router';
import ChartComponent from './barChat';
import DoughnutComponent from './doughnut';
import {
	listSprint,
	sprintReport,
	setCurrentSprint,
} from '../../../store/sprint/asyncAction';
import SelectInputField from '../../../components/form/select';
import { useHistory } from 'react-router-dom';
import PaperComponent from '../../../components/paper/index';
import './index.scss'
import TypographyComponent from '../../../components/typography/index';
import SprintSkeleton from '../../sprint/skeleton';


const ReportPage = () => {
	const dispatch = useDispatch();
	const location = useLocation();
	const history = useHistory();
	let params: any = new URLSearchParams(location.search);
	const sprint = useSelector((state: any) => state.sprint);

	const [selected, setSelected] = useState<any>('');

	const [selectedReport, setSelectedReport] = useState('');
	const query = params.get('filter');
	const mappedData = (data: any) => {
		return data.map((el: any) => {
			if (el.name && el._id) {

				return {
					name: el.name,
					_id: el._id,
				};
			}
			return false
		});
	};



	useEffect(() => {
		dispatch(setCurrentSprint());
		dispatch(listSprint(false));
	}, [dispatch]);
	useEffect(() => {
		setSelectedReport(query);
	}, [query]);

	const ReportDisplay = ({ sprint, data, type, sprintId }: any) => (
		<PaperComponent className='report-paper-container'>
			<DoughnutComponent
				sprint={sprint}
				data={data}
				type={type}
				sprintId={sprintId}
			/>
		</PaperComponent>
	)
	return (
		<div>
			{
				sprint && sprint.list &&
					sprint.list.length ? (
					<div>
						<h1>REPORTS</h1>
						<div className='flex-start gap-20' style={{ width: '60%' }}>
							<SelectInputField
								name='sprint'
								label='Select Sprint'
								value={selected}
								data={sprint.list ? mappedData(sprint.list) : []}
								callback={(e: any) => {
									setSelected(e.target.value);

									dispatch(sprintReport(e.target.value));
								}}
							/>

							<SelectInputField
								name='report'
								label='Select Report Type'
								value={selectedReport}
								data={[
									{
										id: 'pie-chart',
										name: 'Pie-Chart',
									},
									{
										id: 'dougnut-chart',
										name: 'Dougnut-Chart',
									},
									{
										id: 'line-chart',
										name: 'Line-Chart',
									},
								]}
								callback={(e: any) => {
									setSelectedReport(e.target.value);

									history.push(`/report/chart?filter=${e.target.value}`);
								}}
							/>
						</div>

						{query === 'pie-chart' &&
							sprint &&
							sprint.report &&

							<ReportDisplay
								sprint={sprint.report.name}
								sprintId={sprint.report._id}
								data={sprint.report.task}
								type='pie'

							/>


						}

						{query === 'dougnut-chart' &&
							sprint &&
							sprint.report &&

							<ReportDisplay
								sprint={sprint.report.name}
								sprintId={sprint.report._id}
								data={sprint.report.task}
								type='doughnut'

							/>


						}

						{
							query === 'line-chart' &&
							sprint &&
							sprint.report && (

								<PaperComponent className='report-paper-container'>
									<ChartComponent
										sprint={sprint.report.name}
										sprintId={sprint.report._id}
										data={sprint.report.task}
									/>

								</PaperComponent>

							)
						}

					</div >

				) : Object.keys(sprint).length ? (
					<div>
						<h1>REPORTS</h1>
						<TypographyComponent label='Sprint is not available' />
					</div>

				) : (
					<SprintSkeleton />
				)
			}
		</div>
	);
};

export default ReportPage;
