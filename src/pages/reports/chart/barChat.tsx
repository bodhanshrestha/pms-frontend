import React, { useState, useEffect } from 'react';

import {
	Chart,
	Legend,
	ValueAxis,
	VisualRange,
	Export,
	Tooltip,
	Title,
	Pane,
	SeriesTemplate,
	CommonSeriesSettings,
} from 'devextreme-react/chart';
import { secondsToDhms } from '../sprint/list';
import { generateUserReport } from './doughnut';
const BarComponent = ({ sprint, data, sprintId }: any) => {
	const [report, setReport] = useState<any>([])
	const customizeLabel: any = () => {
		return {
			visible: true,
			customizeText(e: any) {
				return `${e.originalArgument} - ${secondsToDhms(e.originalValue)} `;
			},
		};
	};

	const customizeTooltip = (arg: any) => {
		return {
			text: `${arg.originalArgument} - ${secondsToDhms(arg.originalValue)}`,
		};
	};
	useEffect(() => {
		if (data) {
			const result = generateUserReport(data, sprintId)
			setReport(result)
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [])
	return (
		<div>
			<Chart
				id='chart'
				palette='Soft'
				dataSource={report}
				customizeLabel={customizeLabel}
			>
				<CommonSeriesSettings
					argumentField='name'
					valueField='val'
					type='bar'
					ignoreEmptyPoints={true}
				/>
				<SeriesTemplate nameField='name' />
				<Title text={`Line Chart`} subtitle={`of ${sprint}`} />
				<Export enabled={true} />
				<Pane name='topPane' />
				<ValueAxis maxValueMargin={0.1} pane='topPane'>
					<VisualRange startValue={0} />
					<Title text='Time in minute, m' />
				</ValueAxis>

				<Legend visible={true} />
				<Export enabled={true} />
				<Tooltip enabled={true} customizeTooltip={customizeTooltip} />
			</Chart>
		</div>
	);
};

export default BarComponent;
