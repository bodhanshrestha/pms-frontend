import React from 'react';
import Skeleton from '@material-ui/lab/Skeleton';

const ChartSkeleton = () => {
  return <div style={{ marginTop: '10px' }}>
    <div className='space-between'>
      <div>
        <Skeleton variant="rect" width={210} height={20} />
      </div>
      <div className='flex-end'>
        <Skeleton variant="rect" width={120} height={20} style={{ marginRight: '40px' }} />
        <Skeleton variant="rect" width={120} height={20} />
      </div>
    </div>
    <Skeleton variant="circle" width={40} height={40} style={{ marginTop: '20px' }} />
    <Skeleton variant="rect" width={"100%"} height={200} style={{ marginTop: '20px' }} />
  </div>
};

export default ChartSkeleton;
