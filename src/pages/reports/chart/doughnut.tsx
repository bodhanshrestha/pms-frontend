import React, { useState, useEffect } from 'react';

import PieChart, {
	Legend,
	Series,
	Tooltip,
	Format,
	Label,
	Connector,
	Export,
	Title
} from 'devextreme-react/pie-chart';
import { splitTime, groupData } from '../sprint/list';
import { secondsToDhms } from './../sprint/list';

export const generateUserReport = (data: any, sprintId: string) => {

	let initialData: any = []
	data.forEach((el: any) => {
		if (el.time_tracking.length) {
			const filtered: any = el.time_tracking.filter((el: any) => el.sprint === sprintId)
			return filtered && filtered.length && filtered.forEach((el: any) => initialData.push({
				username: el?.user?.username,
				email: el?.user?.email,
				time: splitTime(el?.time)
			}))
		}
		return false
	})
	const result = groupData(initialData)
	let finalResult: any = []
	for (const [, value] of Object.entries(result)) {
		let valueData: any = value
		const result = valueData.reduce((prev: any, next: any) => {
			return {
				...prev,
				time: prev.time + next.time
			}
		})
		finalResult.push({ name: result.username, val: result.time })
	}

	return finalResult
}


const DoughnutComponent = ({ data, type, sprint, sprintId }: any) => {

	const [report, setReport] = useState<any>([])

	const customizeTooltip = (arg: any) => {
		return {
			text: `${arg.originalArgument} - ${secondsToDhms(arg.originalValue)} - ${(
				arg.percent * 100
			).toFixed(2)}%`,
		};
	};

	const customizeLabel = (arg: any) => {
		return `${arg.originalArgument}\n${secondsToDhms(arg.originalValue)} `;
	};



	const pointClickHandler = (arg: any) => {
		arg.target.select();
	};
	useEffect(() => {
		if (data) {
			const result = generateUserReport(data, sprintId)
			setReport(result)
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [])
	return (
		<div>
			<PieChart
				id='pie'
				type={type}

				palette='Soft Pastel'
				dataSource={report}
				resolveLabelOverlapping='shift'
				onPointClick={pointClickHandler}
			>
				<Series argumentField='name'>
					<Label
						visible={true}
						format='fixedPoint'
						customizeText={customizeLabel}
					>
						<Connector visible={true} />
					</Label>
				</Series>
				<Title text={type === 'pie'
					? `Pie Chart`
					: `Doughnut Chart `} subtitle={`of ${sprint}`} />
				<Export enabled={true} />
				<Legend
					margin={0}
					horizontalAlignment='right'
					verticalAlignment='top'
				/>
				<Tooltip enabled={true} customizeTooltip={customizeTooltip}>
					<Format type='millions' />
				</Tooltip>
			</PieChart>
		</div>
	);
};

export default DoughnutComponent;
