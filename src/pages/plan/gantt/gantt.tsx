import React, { useEffect } from 'react';
import Gantt, {
	Tasks,
	Column,
	Editing,
	Toolbar,
	Item,
	Validation,
	Dependencies
} from 'devextreme-react/gantt';
import { useDispatch, useSelector } from 'react-redux';
import {
	createGranttTask,
	deleteGranttTask,
	fetchGanttTasks,
	fetchDependencyGanttTasks,
	updateGranttTask,
	createDependencyGranttTask,
	deleteDependencyGranttTask
} from '../../../store/gantt-task/asyncAction';
import { managerRole } from '../../../utils/role';
import './index.scss'
import GanttSkeleton from './skeleton';
import { Paper } from '@material-ui/core';



export const getWorkingDays = (startDate: any, endDate: any) => {
	let count = 0;
	const curDate = new Date(startDate);
	while (curDate <= new Date(endDate)) {
		const dayOfWeek = curDate.getDay();
		if (dayOfWeek !== 0 && dayOfWeek !== 6) count++;
		curDate.setDate(curDate.getDate() + 1);
	}
	return count;
}

function GanttChart() {



	const dispatch = useDispatch();

	const { tasks, dependency } = useSelector((state: any) => state.ganttTasks);
	const { team } = useSelector((state: any) => state.project);


	const onDependencyInserted = (e: any) => {
		dispatch(createDependencyGranttTask({ id: e.key, ...e.values }));
	};
	const onTaskInserted = (e: any) => {
		dispatch(createGranttTask({ key: e.key, id: e.key, ...e.values }));
	};
	const onTaskUpdated = (e: any) => {
		dispatch(updateGranttTask(e.key, e.values));
	};
	const onTaskDeleted = (e: any) => {
		dispatch(deleteGranttTask(e.key));
	};
	const onDependencyDeleted = (e: any) => {
		dispatch(deleteDependencyGranttTask(e.key));
	};

	useEffect(() => {
		require('devextreme/dist/css/dx.light.css');
	}, []);

	useEffect(() => {
		dispatch(fetchGanttTasks());
		dispatch(fetchDependencyGanttTasks());
	}, [dispatch]);


	return (
		<>
			{
				tasks ? (
					<Paper>
						<Gantt
							taskListWidth={500}
							scaleType='months'
							height={400}
							onTaskInserted={onTaskInserted}
							onTaskUpdated={onTaskUpdated}
							onTaskDeleted={onTaskDeleted}
							onDependencyInserted={onDependencyInserted}
							onDependencyDeleted={onDependencyDeleted}

						>
							<Tasks dataSource={tasks} />
							<Dependencies dataSource={dependency} />
							<Toolbar>
								<Item name='collapseAll' />
								<Item name='expandAll' />
								<Item name='separator' visible={team && managerRole(team)} />
								<Item name='addTask' visible={team && managerRole(team)} />
								<Item name='deleteTask' visible={team && managerRole(team)} />
								<Item name='separator' />
								<Item name='zoomIn' />
								<Item name='zoomOut' />
							</Toolbar>



							<Column dataField='title' caption='Subject' width={300} />
							<Column dataField='start' dataType='date' caption='Start Date' />
							<Column dataField='end' dataType='date' caption='End Date' />

							<Validation autoUpdateParentTasks />
							<Editing
								enabled={(team && managerRole(team)) ? true : false}
								allowDependencyAdding={true}
								allowDependencyDeleting={true}
								allowResourceAdding={false}
								allowResourceDeleting={false}
								allowTaskResourceUpdating={false}
								allowTaskAdding={(team && managerRole(team)) ? true : false}
								allowTaskDeleting={(team && managerRole(team)) ? true : false}
								allowTaskUpdating={(team && managerRole(team)) ? true : false}
							/>
						</Gantt>

					</Paper>
				) : (
					<GanttSkeleton />
				)
			}
		</>

	);
}

export default GanttChart;
