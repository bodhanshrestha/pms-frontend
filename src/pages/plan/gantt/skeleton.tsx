import React from 'react';
import Skeleton from '@material-ui/lab/Skeleton';
import { Divider } from '@material-ui/core';
import { Paper } from '@material-ui/core';

const GanttSkeleton = () => {
  return (
    <Paper elevation={3} className='dashboard-project-card'>
      <div style={{ display: 'flex', justifyContent: 'flex-start', gap: '10px', padding: '0px 0px' }}>
        {
          Array(7).fill(1).map((_, i: number) => (
            <Skeleton variant="rect" width={30} height={30} key={i} />
          ))
        }
      </div>
      <Divider style={{ margin: '15px 0px 10px 0px' }} />
      <div style={{ display: 'flex', justifyContent: 'flex-start', gap: '2px', padding: '5px 0px' }}>
        {
          Array(3).fill(1).map((_, i: number) => (
            <div key={i} style={{ width: i === 2 ? "60%" : "30%" }} >
              <Skeleton variant="rect" width={"100%"} height={300} style={{ marginBottom: '5px' }} />
            </div>
          ))
        }
      </div>
    </Paper>
  )
};

export default GanttSkeleton;
