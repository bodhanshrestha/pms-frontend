import React, { useEffect } from 'react';
import Diagram from 'devextreme-react/diagram';

const DiagramPage = () => {
	useEffect(() => {
		require('devextreme/dist/css/dx.light.css');
	}, []);
	return <Diagram id='diagram' />;
};

export default DiagramPage;
