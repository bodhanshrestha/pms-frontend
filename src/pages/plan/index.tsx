import React from 'react';

import 'devextreme/dist/css/dx.light.css';
import DiagramComponent from './diagram/diagram';
const PlanPage = () => {
	return (
		<div>
			<DiagramComponent />
		</div>
	);
};

export default PlanPage;
