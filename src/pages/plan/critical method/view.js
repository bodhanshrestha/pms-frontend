import { Typography, Paper } from '@material-ui/core'
import React, { useState } from 'react'
import { Activity, ActivityList } from './calculate'
import SearchableSelect from '../../common/taskList/rightSection/select/selectComponent'

const MappedEpicData = (data) => {
  return data.filter(el => !el.parentId).map((el) => {
    return {
      value: el.id,
      label: el.title
    }
  })
}
const findCriticalPathNames = (obj) => {
  console.log("OBJEE", obj)
  let data = [{ id: obj.id, duration: obj.duration }]
  const loopData = (obj) => {
    for (var k in obj) {
      if (k === 'predecessors' && typeof obj[k] == 'object') {
        if (obj[k].length <= 1) {
          if (obj[k][0]) {
            data.unshift({ id: obj[k][0].id, duration: obj[k][0].duration })
            if (obj[k][0]?.predecessors?.length) {
              loopData(obj[k][0]);
            }
          }
        } else {
          if (obj[k][1].predecessors.length > obj[k][0].predecessors.length) {
            if (obj[k][1]) {
              data.unshift({ id: obj[k][1].id, duration: obj[k][1].duration })
              if (obj[k][1]?.predecessors?.length) {
                loopData(obj[k][1]);
              }
            }
          }
        }
      }
    }
  }
  loopData(obj)
  return data
}
const CalculateCriticalPath = ({ activityData, mainData }) => {
  const [data, setData] = useState()
  const handleClick = ({ value }) => {
    var table = new ActivityList();
    activityData?.forEach((el) => {
      table.addActivity(new Activity(el));
    })
    var path = table?.getCriticalPath(value);
    const res = findCriticalPathNames(path)
    setData(res)
  }

  const getRequiredData = (mainData, id) => {
    return mainData.find(el => el.id === id)
  }
  let totalCriticalPath = 0
  // let remainingDays = 0
  // let firstIndexData


  return (
    <div style={{ marginTop: '10px', paddingLeft: '6px' }}>
      <SearchableSelect placeholder='Select epic' data={MappedEpicData(mainData)} callback={handleClick} />
      {
        data && (
          <Paper style={{ padding: '20px' }}>
            {
              data.map((el, i) => {
                const res = getRequiredData(mainData, el.id)
                totalCriticalPath = totalCriticalPath + el.duration
                // if (i == 0) {
                //   firstIndexData = res
                // }
                // if (i === data.length - 1) {
                //   if (!momentIsSameorBefore(res.start, moment().format()) && !momentIsSameorBefore(res.end, moment().format())) {
                //     if (momentIsSameorBefore(firstIndexData.start, moment().format())) {
                //       remainingDays = getWorkingDays(moment().format(), res.end)
                //     } else {
                //       if (momentDateOnlyIsSameOrAfter(firstIndexData.start, moment().format())) {
                //         remainingDays = getWorkingDays(moment().format(), res.end)
                //       } else {
                //         remainingDays = el.duration
                //       }
                //     }
                //   }
                // }
                return <Typography style={{ display: 'inline', fontWeight: 500 }} key={i} children={`${res.title} (${el.duration}) ${i < data.length - 1 ? " -> " : ''}`} />
              })
            }
            {
              totalCriticalPath && (
                <div>
                  <Typography style={{ fontWeight: 500 }} children={`Total Days => ${totalCriticalPath} Days`} />
                </div>
              )
            }
            {/* {
              remainingDays !== 0 && (
                <div>
                  <Typography style={{ fontWeight: 500 }} children={`Remaining Days => ${remainingDays} Days`} />
                </div>
              )
            } */}
            {/* {
              JSON.stringify(criticalPath)
            } */}
          </Paper>
        )
      }
    </div >
  )
}

export default CalculateCriticalPath