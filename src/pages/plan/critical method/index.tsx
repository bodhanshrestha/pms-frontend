import React, { useEffect, useState } from 'react';
import { Button, Paper } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import {
  fetchGanttTasks,
  fetchDependencyGanttTasks
} from '../../../store/gantt-task/asyncAction';

import { getWorkingDays } from '../gantt/gantt';

import CalculateCriticalPath from './view';

const CriticalPathCalculation = () => {
  const [calculate, setCalculate] = useState(false)
  const [activityData, setActivityData] = useState<any>(null)

  const dispatch = useDispatch();

  const { tasks, dependency } = useSelector((state: any) => state.ganttTasks);

  const getCriticalActivity = (main: any, dependency: any) => {
    return main?.map((el: any) => {
      let predecessors: any = []
      dependency?.forEach((x: any) => {
        if (x.successorId === el.id) {
          predecessors.push(x.predecessorId)
        }
      })
      return {
        ...el,
        ...(predecessors.length && { predecessors })
      }
    })
  }

  const criticalTasks = tasks && tasks.filter((el: any) => !el.parentId)
  const activityDuration = criticalTasks?.map((el: any) => {
    const duration = getWorkingDays(el.start, el.end)
    return {
      id: el.id,
      duration
    }
  })

  useEffect(() => {
    dispatch(fetchGanttTasks());
    dispatch(fetchDependencyGanttTasks());
  }, [dispatch]);


  useEffect(() => {
    if (calculate) {
      if (criticalTasks && activityDuration) setActivityData(getCriticalActivity(activityDuration, dependency))
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [calculate])
  return (
    <Paper style={{ padding: '40px', margin: '10px 0px' }}>

      <Button onClick={() => setCalculate(!calculate)}>
        {!calculate ? "Analyze Critical Path" : "Cancel Analyzing Critical Path"}
      </Button>
      {
        calculate && (
          <CalculateCriticalPath activityData={activityData && activityData} mainData={tasks} />
        )
      }
    </Paper>
  )
}

export default CriticalPathCalculation