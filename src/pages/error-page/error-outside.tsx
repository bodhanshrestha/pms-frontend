import React from 'react';
import Error from '../../images/error.svg';
import ButtonComponent from '../../components/button/index';
import './index.scss';
import { useHistory } from 'react-router-dom';
const ErrorPageOutSide = () => {
	const history = useHistory();
	return (
		<div className='error-page'>
			<img className='error-emoji' src={Error} alt='error' />

			<ButtonComponent
				label=' Go Back'
				variant='contained'
				color='primary'
				callback={() => history.push('/login')}
			/>
		</div>
	);
};

export default ErrorPageOutSide;
