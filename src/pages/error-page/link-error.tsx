import React from 'react';
import Error from '../../images/error.svg';
import ButtonComponent from '../../components/button/index';
import './index.scss';
import { useHistory } from 'react-router-dom';
const LinkErrorPage = () => {
	const history = useHistory();
	return (
		<div className='error-page'>
			<img className='error-emoji' src={Error} alt='error' />
			<h1 className='red'>Has Expired or Already been used</h1>
			<p>
				If you want to access into the system then enter your proper credential
				into login page.
			</p>
			<ButtonComponent
				label=' Go Back'
				variant='contained'
				color='primary'
				callback={() => history.replace('/login')}
			/>
		</div>
	);
};
export default LinkErrorPage;
