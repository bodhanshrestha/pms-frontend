import { Paper, Typography, Divider } from '@material-ui/core';
import React from 'react';

const PaperDetails = ({ question, answer }: { question: string, answer: string }) => {
  return (
    <Paper style={{ padding: '10px', margin: "5px" }}>
      <Typography children={question} variant='h6' />
      <Divider style={{ marginBottom: '5px' }} />
      <Typography children={answer} variant='body1' />
    </Paper>
  )
}


const Help = () => {
  return (
    <div>
      <h1>Help?</h1>
      <h3>
        TeamZone allows you to break your work down into manageable chunks,
        assign it to the right person, and progress it through a customizable
        workflow until it's done and make sure you have all the correct
        information to get your work done!
      </h3>
      <h3>Basic terminologies to help you get started</h3>


      <PaperDetails
        question='What is Backlog?'
        answer="A product backlog is a prioritized list of work for the development
        team that is derived from the roadmap and its requirements. The most
        important items are shown at the top of the product backlog so the
        team knows what to deliver first.It's a decision-making artifact that
        helps you estimate, refine, and prioritize everything you might
      sometime in the future want to complete."
      />
      <PaperDetails
        question='What is Sprint?'
        answer=' Sprint is a part of the Scrum framework. In Scrum, large projects are
        broken down into a series of iterations of smaller manageable bits that
        teams can handle. These iterations are called sprints. Sprints help
        teams apply agile principles such as “delivering working software
        frequently” and “responding to change over following a plan.” They
        typically take place in a time period of no more than two to four weeks.'
      />
      <PaperDetails
        question='What is Moment?'
        answer='Tasks from active sprints'
      />
      <PaperDetails
        question='What is an Issue?'
        answer='  An Issue log is a simple list or spreadsheet that helps managers track
        the issues that arise in a project and prioritize a response to them.
        An issue is something that has already come up in your project, and
        you need to identify and track that issue immediately.'
      />
      <PaperDetails
        question='What is critical path?'
        answer=' The critical path (or paths) is the longest path (in time) from Start to Finish; it indicates the minimum time necessary to complete the entire project.'
      />
      <PaperDetails
        question=''
        answer=''
      />

    </div>
  );
};

export default Help;
