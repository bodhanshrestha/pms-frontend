import React, { useState, useContext } from 'react';
import { useDispatch } from 'react-redux';
import InputField from '../../../components/form/text';
import { SocketContext } from '../../../context/socket';
import { createTaskTitle } from '../../../store/task/asyncAction';

const CreateList = () => {
	const dispatch = useDispatch();
	const [title, setTitle] = useState('');
	const socket: any = useContext(SocketContext);

	const keyPress = (e: any) => {
		if (e.keyCode === 13 || e.key === 'Enter') {
			e.preventDefault();
			dispatch(createTaskTitle({ title }, socket));
			setTitle('');
		}
	};
	const handleChange = (e: any) => {
		setTitle(e.target.value);
	};


	return (
		<div className='backlog-create-list'>
			<form autoComplete='off'>
				<InputField
					label='Create New Task'
					placeholder='Enter task title'
					name='name'
					type='text'
					value={title}
					callback={handleChange}
					eventCallBack={keyPress}
				/>
			</form>
		</div>
	);
};

export default CreateList;
