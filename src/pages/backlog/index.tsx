import React, { useContext, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { SocketContext } from '../../context/socket';
import { setCurrentSprint } from '../../store/sprint/asyncAction';

import CreateList from './list/createList';
import LogList from './log';

const BackLog = () => {
	const dispatch = useDispatch();
	const socket: any = useContext(SocketContext);
	useEffect(() => {
		dispatch(setCurrentSprint());
	}, [dispatch]);


	return (
		<div className='backlog'>
			<LogList label='New' socket={socket} />
			<CreateList />
		</div>
	);
};

export default BackLog;
