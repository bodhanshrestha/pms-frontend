import React, { useEffect, useState } from 'react';

import AccordionComponent from '../../../components/accordion';
import { useDispatch, useSelector } from 'react-redux';
import { fetchNewTasks } from '../../../store/task/asyncAction';
import CommonTaskTable from '../../common/taskList/table';
import { fetchTaskLoading, modifyTasks } from '../../../store/task/action';
import SearchField from '../../../components/search';

export const ModifiedTasksData = (originalData: any, updatedData: any) => {
	return originalData?.map((data: any) => {
		if (data._id === updatedData._id) {
			return updatedData
		} else {
			return data
		}
	})
}

const LogList = ({ label, socket }: any) => {
	const dispatch = useDispatch();
	const { loading, newTasks } = useSelector((state: any) => state.task);
	const [keyword, setKeyword] = useState('');
	useEffect(() => {
		dispatch(fetchTaskLoading(true));
		dispatch(fetchNewTasks());
	}, [dispatch]);

	const filterData = (keyword: any) => {
		return newTasks && newTasks?.length && newTasks.filter((task: any) => {
			return task.title.toLowerCase().includes(keyword) || task.key.toLowerCase().includes(keyword);
		});
	};
	useEffect(() => {
		socket?.on("updateTasks", (data: any) => {
			dispatch(modifyTasks({ data, modifiedType: 'newTasks' }));
		})
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [socket])

	return (
		<div>
			<SearchField keyword={keyword} setKeyword={setKeyword} />
			<AccordionComponent header={label}>
				<CommonTaskTable
					data={filterData(keyword)}
					selectCheckbox={true}
					loading={loading}
				/>
			</AccordionComponent>
		</div>
	);
};

export default LogList;
