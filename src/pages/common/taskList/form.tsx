import React from 'react';
import InputField from '../../../components/form/text';
import { useFormik } from 'formik';

const TaskForm = () => {
  const formik = useFormik({
    initialValues: {
      username: '',
      password: '',
      checked: false,
    },
    onSubmit: (values) => {},
  });
  return (
    <form noValidate autoComplete='off' onSubmit={formik.handleSubmit}>
      <InputField
        label='Username'
        name='username'
        type='text'
        placeholder='Enter Your Username'
        value={formik.values.username}
        callback={formik.handleChange}
        error={formik.touched.username && Boolean(formik.errors.username)}
        helperText={formik.touched.username && formik.errors.username}
      />
    </form>
  );
};

export default TaskForm;
