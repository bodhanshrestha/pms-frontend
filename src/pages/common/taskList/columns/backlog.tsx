import React from 'react';
import AvatarComponent from '../../../../components/avatar';
import { compareToSort, getFirstWord } from '../../../../utils/functions';

export const columns: any = [
	{
		title: 'Title',
		dataIndex: 'title',
		key: 'title',
		ellipsis: true,
		width: '45%',
		sorter: (a: any, b: any) => compareToSort(a.title, b.title),
		sortDirections: ['descend', 'ascend'],
	},
	{
		title: 'Epic',
		dataIndex: ['epic', 'title'],
		key: 'epic',
		ellipsis: true,
		sorter: (a: any, b: any) => compareToSort(a.epic?.title, b.epic?.title),
		sortDirections: ['descend', 'ascend'],
	},
	{
		// title: 'Assignee',
		dataIndex: 'assigne',
		key: 'assigne',
		width: 80,
		render: (assigne: any) => {
			let user: string = '';
			if (assigne) {
				if (assigne.name) {
					if (assigne.name.includes('@')) {
						user = assigne.name.split('@')[0].split('')[0].toUpperCase();
					} else {
						user = getFirstWord(assigne.name);
					}
				}
			} else {
				user = '';
			}
			return (
				<>
					{user && (
						<AvatarComponent
							children={`${user}`}
							background={`${assigne && assigne.color}`}
						/>
					)}
				</>
			);
		},
	},
	{
		title: 'Type',
		dataIndex: 'type',
		key: 'type',
		width: 100,
		sorter: (a: any, b: any) => compareToSort(a.type, b.type),
		sortDirections: ['descend', 'ascend'],
	},

	{
		title: 'Priority',
		dataIndex: 'priority',
		key: 'priority',
		width: 100,
		sorter: (a: any, b: any) => compareToSort(a.priority, b.priority),
		sortDirections: ['descend', 'ascend'],
	},
	{
		title: 'Status',
		key: 'status',
		width: 100,
		render: (_: any, record: any) => {
			if (record.to_do) {
				return <span>To Do</span>;
			}
			if (record.in_progress) {
				return <span>In Progress</span>;
			}
			if (record.qa) {
				return <span>QA</span>;
			}
			if (record.deferred) {
				return <span>Deferred</span>;
			}
			if (record.done) {
				return <span>Done</span>;
			}
		},
	},
];
