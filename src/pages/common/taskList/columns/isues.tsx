import React from 'react';
import AvatarComponent from './../../../../components/avatar/index';
import { compareToSort, getFirstWord, stringComparer } from './../../../../utils/functions';

export const columns: any = [
	{
		title: 'Title',
		dataIndex: 'title',
		key: 'title',
		width: '40%',
		ellipsis: true,
		sorter: (a: any, b: any) => compareToSort(a.title, b.title),
		sortDirections: ['descend', 'ascend'],

	},
	{
		title: 'Key',
		key: 'key',
		dataIndex: 'key',
		ellipsis: true,
		sorter: (a: any, b: any) => compareToSort(a.key, b.key),
		sortDirections: ['descend', 'ascend'],
	},
	{
		title: 'Epic',
		key: 'epic',
		dataIndex: 'epic',
		ellipsis: true,
		sorter: (a: any, b: any) => {
			const epicData: any = localStorage.getItem('epic')
			const epic = JSON.parse(epicData)
			let aData: any
			let bData: any
			if (epic) {
				aData = epic.filter((el: any) => el._id === a.epic)[0]?.title
				bData = epic.filter((el: any) => el._id === b.epic)[0]?.title
			}
			return compareToSort(aData, bData)
		},
		sortDirections: ['descend', 'ascend'],
		render: (data: any) => {
			const epicData: any = localStorage.getItem('epic')
			const epic = JSON.parse(epicData)
			if (epic) {
				const name = epic.filter((el: any) => el._id === data)[0]?.title
				return <span>{name}</span>
			}
		}
	},
	{
		// title: 'Assignee',
		dataIndex: 'assigne_info',
		key: 'assigne_info',
		width: 80,
		render: (assigne_info: any) => {
			let user: string = '';
			if (assigne_info) {
				if (assigne_info.name) {
					if (assigne_info.name.includes('@')) {
						user = assigne_info.name.split('@')[0].split('')[0].toUpperCase();
					} else {
						user = getFirstWord(assigne_info.name);
					}
				}
			} else {
				user = '';
			}
			return (
				<>
					{user && (
						<AvatarComponent
							children={`${user}`}
							background={`${assigne_info && assigne_info.color}`}
						/>
					)}
				</>
			);
		},
	},
	{
		title: 'Priority',
		dataIndex: 'priority',
		key: 'priority',
		sorter: (a: any, b: any) => stringComparer(a.priority, b.priority),
		sortDirections: ['descend', 'ascend'],
		width: 100,
	},
	{
		title: 'Type',
		dataIndex: 'type',
		key: 'type',
		sorter: (a: any, b: any) => stringComparer(a.type, b.type),
		sortDirections: ['descend', 'ascend'],
		width: 100,
	},

	{
		title: 'Status',
		key: 'status',
		width: 100,
		render: (_: any, record: any) => {
			if (record.to_do) {
				return <span>To Do</span>;
			}
			if (record.in_progress) {
				return <span>In Progress</span>;
			}
			if (record.done) {
				return <span>Done</span>;
			}
		},
	},
];
