import React from 'react';
import CardComp from './card';
export const columns: any = [
	{
		key: 'title',
		ellipsis: true,
		render: (_: any, record: any) => {
			return <CardComp data={record} />;
		},
	},
];
