import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
const useStyles = makeStyles({
	bullet: {
		display: 'inline-block',
		margin: '0 2px',
		transform: 'scale(0.8)',
	},
	title: {
		fontSize: 14,
	},
	pos: {
		marginBottom: 12,
	},
});

const CardComp = ({ data }: any) => {
	const classes = useStyles();

	return (
		<Card>
			<CardContent>
				<Typography
					className={classes.title}
					color='textSecondary'
					gutterBottom
				>
					{data.key}
				</Typography>
				<Typography variant='h5' component='h2' noWrap={true}>
					{data.title}
				</Typography>
			</CardContent>
		</Card>
	);
};

export default CardComp;
