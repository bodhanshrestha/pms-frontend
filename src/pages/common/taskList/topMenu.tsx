import React, { useState, useContext } from 'react';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { useDispatch } from 'react-redux';
import { DeleteTaskAsyncAction } from '../../../store/task/asyncAction';
import DeleteDialog from './deleteDialog';
import { SocketContext } from '../../../context/socket';

const TopMenu = ({ taskId, handleClose }: any) => {
	const dispatch = useDispatch();
	const [anchorEl, setAnchorEl] = useState(null);

	const open = Boolean(anchorEl);

	const handleClick = (event: any) => {
		setAnchorEl(event.currentTarget);
	};
	const socket: any = useContext(SocketContext);

	const handleDelete = () => {
		dispatch(DeleteTaskAsyncAction(taskId, false, socket));
		setAnchorEl(null);
		handleClose();
	};

	return (
		<div>
			<IconButton aria-controls='long-menu' onClick={handleClick}>
				<MoreVertIcon />
			</IconButton>
			<Menu
				id='long-menu'
				anchorEl={anchorEl}
				keepMounted
				open={open}
				onClose={() => setAnchorEl(null)}
				PaperProps={{
					style: {
						width: '20ch',
					},
				}}>
				<DeleteDialog setAnchorEl={setAnchorEl} handleDelete={handleDelete} />
			</Menu>
		</div>
	);
};

export default TopMenu;
