import React from 'react';
import Skeleton from '@material-ui/lab/Skeleton';
import { Divider } from '@material-ui/core';
import { Paper } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import AccordionComponent from './../../../components/accordion/index';

const InputSkeleton = () => (
  <div style={{ display: 'flex', justifyContent: 'flex-start', gap: '20px', padding: '5px 0px' }}>
    <Skeleton variant="rect" width={100} height={20} />
    <Skeleton variant="rect" width={200} height={20} />
  </div>
)
const TaskSkeleton = () => {
  return (
    <Grid container spacing={2} >
      <Grid item xs={12} sm={8} md={8}>
        <Paper elevation={1} className='dashboard-project-card'>
          <Skeleton variant="rect" width={"60%"} height={30} style={{ margin: '20px 0px 20px 0px' }} />
          <Divider style={{ margin: '0px 0px 20px 0px' }} />
          <div style={{ display: 'flex', justifyContent: 'flex-start', gap: '10px', padding: '5px 0px', margin: '0px 0px 20px 0px' }}>
            {Array(12).fill(1).map((_, i: number) => <Skeleton variant="rect" width={30} height={30} key={i} />)}
          </div>
          <Skeleton variant="rect" width={"100%"} height={180} style={{ margin: '0px 0px 20px 0px' }} />
        </Paper>
      </Grid>
      <Grid item xs={12} sm={4} md={4}>
        <AccordionComponent style={{ width: '200px' }} close={true} header={<Skeleton variant="rect" width={150} height={20} />}>
        </AccordionComponent>
        <Paper elevation={1} className='dashboard-project-card'>
          <AccordionComponent header={<Skeleton variant="rect" width={150} height={20} />}>
            <div>
              <InputSkeleton />
              <InputSkeleton />
              <InputSkeleton />
              <InputSkeleton />
              <InputSkeleton />
              <InputSkeleton />
              <AccordionComponent style={{ width: '100%' }} close={true} header={<Skeleton variant="rect" width={150} height={20} />}>
              </AccordionComponent>
            </div>
          </AccordionComponent>
        </Paper>
      </Grid>
    </Grid >
  )
};

export default TaskSkeleton;
