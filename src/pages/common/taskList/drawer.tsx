import React, { useState, useEffect, useContext } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
	UpdateTaskAsyncAction,
	UpdateMassTaskAsyncAction,
	DeleteTaskAsyncAction,
	DeleteGroupTaskAsyncAction,
} from '../../../store/task/asyncAction';
import './index.css';
import './drawer.scss';
import { Drawer } from 'antd';
import Divider from '@material-ui/core/Divider';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Button from '@material-ui/core/Button';

import {
	Add,
	PlaylistAdd,
	ExitToApp,
	DeleteSweep,
	Edit,
} from '@material-ui/icons';
import { setSnackbar } from '../../../store/snackbar/action';
import { UpdateSprintTask } from '../../../store/sprint/asyncAction';
import { getRole } from './../../sprint/index';
import { SocketContext } from '../../../context/socket';

const PopupDrawer = ({
	record,
	show,
	loading,
	selectedTasksId,
	setOpenDialogBox,
}: any) => {
	const dark_mode = useSelector((state: any) => state.user.user.dark_mode);
	const sprint = useSelector((state: any) => state.sprint);
	const { team } = useSelector((state: any) => state.project);
	const [visible, setVisible] = useState(false);
	const dispatch = useDispatch();
	const socket: any = useContext(SocketContext);

	const handleAddToSprint = () => {
		if (Object.keys(sprint).length === 0) {
			dispatch(
				setSnackbar({
					open: true,
					type: 'error',
					message: 'You Need To Start the Sprint',
				})
			);
		} else {
			dispatch(UpdateTaskAsyncAction(record._id, { current: true }, true, socket));
			dispatch(UpdateSprintTask(sprint._id, { task: [record._id] }, socket));
		}
		onClose();
	};
	const handleAddAllToSprint = () => {
		if (Object.keys(sprint).length === 0) {
			dispatch(
				setSnackbar({
					open: true,
					type: 'error',
					message: 'You Need To Start the Sprint',
				})
			);
		} else {
			if (selectedTasksId.length > 0) {
				dispatch(UpdateMassTaskAsyncAction(selectedTasksId));
				dispatch(UpdateSprintTask(sprint._id, { task: selectedTasksId }, socket));
				onClose();
				return;
			}
			dispatch(
				setSnackbar({
					open: true,
					type: 'error',
					message: 'Please Select the Available Tasks',
				})
			);
		}
		onClose();
	};


	const handleDelete = () => {
		if (selectedTasksId.length <= 0 || selectedTasksId.length <= 1) {
			dispatch(DeleteTaskAsyncAction(record._id, true, socket));
		} else {
			dispatch(DeleteGroupTaskAsyncAction(selectedTasksId, true, socket));
		}
		onClose();
	};
	const handleEdit = () => {
		setOpenDialogBox({
			visible: true,
			record: record,
		});
		onClose();
	};
	const onClose = () => {
		setVisible(false);
	};
	useEffect(() => {
		if (show) setVisible(true);
		return () => { };
	}, [show]);

	return (
		<Drawer
			title='Task Action'
			placement='right'
			className={`pt-60 ${dark_mode ? 'drawer_dark_mode' : ''}`}
			onClose={onClose}
			width='350'
			visible={visible}>

			<ButtonGroup
				className='btn-groups'
				orientation='vertical'
				aria-label='vertical contained button group'
				variant='text'
				style={{ width: '100%' }}
			>
				{
					team && getRole(team) === 'manager' && (
						<>
							<Button
								key='1'
								variant='outlined'
								onClick={handleAddToSprint}
								disabled={loading ? true : false}>
								<Add /> &nbsp; Add To Current Sprint
							</Button>
							<Divider />

							<Button
								key='2'
								variant='outlined'
								onClick={handleAddAllToSprint}
								disabled={loading ? true : false}>
								<PlaylistAdd />
								Add All Selected To Current Sprint
							</Button>
							<Divider />
						</>
					)
				}
				{(selectedTasksId.length <= 0 || selectedTasksId.length <= 1) && (
					<>
						<Button
							key='3'
							variant='outlined'
							onClick={handleEdit}
							disabled={loading ? true : false}>
							<Edit /> &nbsp; &nbsp; Edit
						</Button>
						<Divider />
					</>
				)}
				{
					team && getRole(team) === 'manager' && (
						<>
							<Button
								key='4'
								variant='outlined'
								onClick={handleDelete}
								disabled={loading ? true : false}>
								<DeleteSweep /> &nbsp; &nbsp; Delete
							</Button>
							<Divider />
						</>
					)}
				<Button key='5' variant='outlined' onClick={onClose}>
					<ExitToApp /> &nbsp; &nbsp; Exit
				</Button>
			</ButtonGroup>
		</Drawer>
	);
};

export default PopupDrawer;
