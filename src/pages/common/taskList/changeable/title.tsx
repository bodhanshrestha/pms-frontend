import React, { useState, ChangeEvent, KeyboardEvent, useContext } from 'react';
import { TextField, Typography } from '@material-ui/core';
import { useDispatch } from 'react-redux';
import { UpdateTaskAsyncAction } from '../../../../store/task/asyncAction';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import { useLocation } from 'react-router';
import { SocketContext } from '../../../../context/socket';
const useStyles = makeStyles(
	createStyles({
		name: { fontSize: '24px' },
		label: { borderBottom: '1px solid  gray' },
	})
);

const TaskTitle = ({ label, taskId }: any) => {
	const socket = useContext(SocketContext);

	const classes = useStyles();
	const dispatch = useDispatch();
	const [name, setName] = useState(label ? label : '');
	const [isNameFocused, setIsNamedFocused] = useState(false);
	const { pathname } = useLocation();
	const handleChangeRequest = () => {
		if (label !== name) {
			dispatch(UpdateTaskAsyncAction(taskId, { title: name }, false, pathname, socket));
		}
		setIsNamedFocused(false);
	};

	const onTitleChange = (event: ChangeEvent<HTMLInputElement>) => {
		setName(event.target.value);
	};
	const handleKeyPress = (event: KeyboardEvent<HTMLInputElement>) => {
		if (event.key === 'Enter') {
			handleChangeRequest();
		}
	};



	return (
		<>
			{!isNameFocused ? (
				<Typography
					children={name}
					variant='h5'
					noWrap={true}
					className={classes.label}
					style={{ fontWeight: 500 }}
					onClick={() => setIsNamedFocused(true)}
				/>
			) : (
				<TextField
					className='w-100'
					inputProps={{ className: classes.name }}
					autoFocus
					value={name}
					onChange={onTitleChange}
					onKeyPress={handleKeyPress}
					onBlur={() => handleChangeRequest()}
				/>
			)}
		</>
	);
};

export default TaskTitle;
