import React, { useState, useEffect, useContext } from 'react';

import { useDispatch } from 'react-redux';
import { UpdateTaskAsyncAction } from '../../../../store/task/asyncAction';
import { InputList } from './list';
import { timeValidation } from './../../../../utils/functions';
import { SocketContext } from '../../../../context/socket';

const OriginalEstimateInput = ({ data, taskId }: any) => {
	const socket: any = useContext(SocketContext)
	const dispatch = useDispatch();
	const [estimate, setEstimate] = useState('');
	const [error, setError] = useState(false);
	const handleEstimateChange = (event: any) => {
		setEstimate(event.target.value);
	};
	const handleTimeSubmit = (e: any) => {
		setError(false);
		if (e.key === 'Enter') {
			if (timeValidation(estimate)) {
				if (data !== estimate) {
					dispatch(
						UpdateTaskAsyncAction(taskId, { estimate: estimate.trim() }, false, socket)
					);
				}
			} else {
				return setError(true);
			}
		}
	};
	useEffect(() => {
		if (data) setEstimate(data);
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [data]);
	return (
		<InputList
			label='Estimated time'
			name='estimate'
			value={estimate}
			callback={handleEstimateChange}
			style={{ lineHeight: '1' }}
			eventCallBack={handleTimeSubmit}
			eventOnBlur={(e: any) => handleTimeSubmit({ ...e, key: 'Enter' })}
			error={error}
		/>
	);
};

export default OriginalEstimateInput;
