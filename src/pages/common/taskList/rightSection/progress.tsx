import React, { useContext, useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useLocation } from 'react-router';
import SelectInputField from '../../../../components/form/select';
import { SocketContext } from '../../../../context/socket';
import { UpdateTaskAsyncAction } from '../../../../store/task/asyncAction';

const TaskProgress = ({ taskId, progressData }: any) => {
	const dispatch = useDispatch();
	const { pathname } = useLocation();
	const [selected, setSelected] = useState('');
	const socket = useContext(SocketContext);

	useEffect(() => {

		if (progressData.deferred) setSelected('deferred');
		if (progressData.to_do) setSelected('to_do');
		if (progressData.in_progress) setSelected('in_progress');
		if (progressData.qa) setSelected('qa');
		if (progressData.done) setSelected('done');
	}, [progressData.to_do, progressData.in_progress, progressData.done, progressData.qa, progressData.deferred]);

	const handleUpdate = (progress: any) => {
		dispatch(UpdateTaskAsyncAction(taskId, progress, false, pathname, socket));
	};
	const handleProgressChange = (event: any) => {
		if (event.target.value === 'deferred') {
			setSelected('deferred');
			if (selected !== event.target.value) handleUpdate({ in_progress: false, deferred: true, done: false, qa: false, to_do: false });
		}
		if (event.target.value === 'to_do') {
			setSelected('to_do');
			if (selected !== event.target.value) handleUpdate({ in_progress: false, to_do: true, done: false, qa: false, deferred: false });
		}
		if (event.target.value === 'in_progress') {
			setSelected('in_progress');
			if (selected !== event.target.value) handleUpdate({ in_progress: true, to_do: false, done: false, qa: false, deferred: false });
		}
		if (event.target.value === 'qa') {
			setSelected('qa');
			if (selected !== event.target.value) handleUpdate({ in_progress: false, to_do: false, qa: true, deferred: false, done: false });
		}
		if (event.target.value === 'done') {
			setSelected('done');
			if (selected !== event.target.value) handleUpdate({ in_progress: false, to_do: false, done: true, qa: false, deferred: false });
		}
	};

	return (
		<SelectInputField
			name='type'
			label=''
			width='40%'
			value={selected}
			small={true}
			callback={handleProgressChange}
			placeholder='Task'
			className='progress_list'
			data={[
				{ value: 'deferred', label: 'Deferred' },
				{ value: 'to_do', label: 'To Do' },
				{ value: 'in_progress', label: 'In Progress' },
				{ value: 'qa', label: 'QA' },
				{ value: 'done', label: 'Done' },
			]}
		/>
	);
};

export default TaskProgress;
