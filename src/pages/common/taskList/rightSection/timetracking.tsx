import React, { useState, useContext } from 'react';

import { useDispatch, useSelector } from 'react-redux';
import { UpdateTaskAsyncAction } from '../../../../store/task/asyncAction';
import { timeValidation } from './../../../../utils/functions';
import { InputList } from './list';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import TypographyComponent from '../../../../components/typography';
import { setSnackbar } from './../../../../store/snackbar/action';
import { SocketContext } from '../../../../context/socket';
const TimeTrackingInput = ({ data, taskId }: any) => {
	const socket: any = useContext(SocketContext)
	const dispatch = useDispatch();
	const { user } = useSelector((state: any) => state.user)
	const { _id, task } = useSelector((state: any) => state.sprint)
	const [time, setTime] = useState('');
	const [error, setError] = useState(false);
	const [helperText] = useState('format: 1h 30m / 1h / 30m');

	const handleTimeChange = (event: any) => {
		setTime(event.target.value);
	};

	const handleTimeSubmit = (e: any) => {
		setError(false);
		if (e.key === 'Enter') {
			if (timeValidation(time)) {
				const currentTask = task?.find((el: any) => el._id === taskId)
				if (_id && currentTask) {
					dispatch(
						UpdateTaskAsyncAction(taskId, {
							time_tracking: {
								user: user?._id,
								time: time.trim(),
								sprint: _id
							}
						}, false, socket)
					);
					dispatch(
						setSnackbar({ open: true, type: 'success', message: `You have logged ${time} in ${currentTask.title}` })
					);
				} else {
					dispatch(
						setSnackbar({ open: true, type: 'error', message: `Task Must Be In Current Sprint` })
					);
					setTime('')
				}
			} else {
				if (time) {
					setError(true);
				} else {
					setError(false);
				}
			}
		}
	};

	return (
		<div className='time-tracking'>
			<InputList
				label='Time Tracking'
				name='time'
				value={time}
				callback={handleTimeChange}
				eventCallBack={handleTimeSubmit}
				eventOnBlur={() => handleTimeSubmit({ key: 'Enter' })}
				error={error}
				helperText={helperText}
			/>

			<Accordion>
				<AccordionSummary
					expandIcon={<ExpandMoreIcon />}
					aria-controls="panel1a-content"
					id="panel1a-header"
				>
					<TypographyComponent label='Time Assigned' />
				</AccordionSummary>
				<AccordionDetails>
					<ul style={{ marginLeft: '-20px' }} className='w-100'>
						{
							data?.map((el: any, i: number) => (
								<li key={i} className='space-between'>
									<span style={{ display: 'block' }}>
										{el?.user?.username}
									</span>
									<span style={{ display: 'block' }}>
										{el?.time}
									</span>
								</li>
							))
						}
					</ul>
				</AccordionDetails>
			</Accordion>

		</div>
	);
};

export default TimeTrackingInput;
