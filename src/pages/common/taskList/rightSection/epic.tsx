import React, { useState, useEffect, useContext } from 'react';

import { useDispatch } from 'react-redux';
import { useLocation } from 'react-router';
import { UpdateTaskAsyncAction } from '../../../../store/task/asyncAction';
import { SelectList } from './list';
import { useHistory } from 'react-router-dom';
import { SocketContext } from '../../../../context/socket';
import { findDataObject } from '../../../../utils/functions';
export const MappedEpicData = (data: any) => {
	if (data.length) {
		return data.map((el: any) => {
			return {
				value: el._id,
				label: el.title
			}
		})
	} else {
		return [{
			value: 'new',
			label: "Create New Epic"
		}]
	}
}

const EpicSelect = ({ data, epicTasks, taskId }: any) => {
	const socket = useContext(SocketContext);

	const dispatch = useDispatch();
	const history = useHistory();
	const [epic, setEpic] = useState<any>([]);
	const { pathname } = useLocation();
	const handleUpdate = (data: any) => {
		dispatch(UpdateTaskAsyncAction(taskId, data, false, pathname, socket));
	};
	const handleEpicChange = (event: any) => {
		if (event.value === 'new') {
			return history.push('/plan/gantt-chart')
		} else {
			setEpic(event);
			const prevEpic: any = findDataObject(MappedEpicData(data), event)
			if (epic[0]?.value !== prevEpic?.value) {
				handleUpdate({ epic: event.value });
			}
		}
	};
	useEffect(() => {
		if (epicTasks) setEpic(data.filter((e: any) => e._id === epicTasks).map((e: any) => { return { value: e._id, label: e.title } }));
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [epicTasks]);

	return (
		<SelectList
			name='epic'
			label='Epic'
			value={epic}
			callback={handleEpicChange}
			data={MappedEpicData(data)}
		/>
	);
};

export default EpicSelect;
