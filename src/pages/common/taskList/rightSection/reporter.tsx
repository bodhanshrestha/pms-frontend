import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { SelectList } from './list';

const ReporterSelect = ({ data }: any) => {
	const [reporter, setReporter] = useState<any>({});
	const { user } = useSelector((state: any) => state.user);
	const teamMember = useSelector((state: any) => state.project.team);
	const handleReporterChange = (event: any) => {
		setReporter(event);
	};
	const userRole = (username: string, email: string) => {
		const filter = teamMember.filter(
			(el: any) => el.member.username === username && el.member.email === email
		);
		return filter.length > 0 && filter[0].role;
	};
	useEffect(() => {
		if (data) setReporter({ value: data._id, label: data.name });
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [data]);
	return (
		<SelectList
			label='Reporter'
			name='reporter'
			value={reporter}
			callback={handleReporterChange}
			data={[{ value: data._id, label: data.name }]}
			disabled={
				user && teamMember && userRole(user.username, user.email) !== 'manager'
			}
		/>
	);
};

export default ReporterSelect;
