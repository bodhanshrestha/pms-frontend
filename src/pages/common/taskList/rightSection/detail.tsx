import React from 'react';
import AccordionComponent from '../../../../components/accordion';
import DetailList from './list';
import './index.scss';
import TaskProgress from './progress';
const LeftDetails = ({ data, epicTasks, epic }: any) => {
	const { in_progress, to_do, done, qa, deferred } = data;
	return (
		<div>
			<TaskProgress
				taskId={data._id}
				progressData={{ in_progress, to_do, done, qa, deferred }}
			/>
			<AccordionComponent header='Details'>
				<DetailList data={data} epicTasks={epicTasks} epic={epic} />
			</AccordionComponent>
		</div>
	);
};

export default LeftDetails;
