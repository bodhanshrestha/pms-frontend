import React from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import InputField from '../../../../components/form/text';
import { Grid } from '@material-ui/core';
import ReporterSelect from './reporter';
import AssigneeSelect from './assignee';
import OriginalEstimateInput from './originalestimate';
import PrioritySelect from './priority';
import TimeTrackingInput from './timetracking';
import TypographyComponent from '../../../../components/typography';
import EpicSelect from './epic';
import SearchableSelect from './select/selectComponent';

export const SelectList = ({
	label,
	name,
	callback,
	data,
	value,
	defaultValue,
	disabled = false,
	placeholder
}: any) => (
	<ListItem className='w-100'>
		<Grid container>
			<Grid item xs={4} sm={4} md={4}>
				<TypographyComponent label={label} className='pt-10' />
			</Grid>
			<Grid item xs={8} sm={8} md={8}>
				<SearchableSelect
					placeholder={placeholder}
					data={data}
					// defaultValue={defaultValue}
					value={value}
					disabled={disabled}
					callback={callback}
					name={name}
					className=''
				/>
			</Grid>
		</Grid>
	</ListItem>
);

export const InputList = ({
	label,
	name,
	callback,
	value,
	eventCallBack,
	error,
	helperText,
	style,
	eventOnBlur,
}: any) => (
	<ListItem>
		<Grid container>
			<Grid item xs={4} sm={4} md={4}>
				<TypographyComponent
					style={style}
					label={label}
					variant='body1'
					className='pt-10'
				/>
			</Grid>
			<Grid item xs={8} sm={8} md={8}>
				<InputField
					label=''
					name={name}
					type='text'
					small={true}
					value={value}
					callback={callback}
					marginBottom='0'
					eventCallBack={eventCallBack}
					eventOnBlur={eventOnBlur}
					error={error}
					helperText={helperText}
				/>
			</Grid>
		</Grid>
	</ListItem>
);

const DetailList = ({ data, epicTasks, epic }: any) => {
	const { priority, reporter, estimate, assigne, time_tracking, _id } = data;
	return (
		<List component='nav' aria-label='main mailbox folders' className='w-100'>
			<AssigneeSelect data={assigne} taskId={_id} />
			<ReporterSelect data={reporter} />
			<OriginalEstimateInput data={estimate} taskId={_id} />
			<PrioritySelect data={priority} taskId={_id} />
			<EpicSelect data={epic} epicTasks={epicTasks} taskId={_id} />
			<TimeTrackingInput data={time_tracking} taskId={_id} />
		</List>
	);
};

export default DetailList;
