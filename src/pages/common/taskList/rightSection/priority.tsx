import React, { useState, useEffect, useContext } from 'react';

import { useDispatch } from 'react-redux';
import { useLocation } from 'react-router';
import { SocketContext } from '../../../../context/socket';
import { UpdateTaskAsyncAction } from '../../../../store/task/asyncAction';
import { SelectList } from './list';

const PrioritySelect = ({ data, taskId }: any) => {
	const socket = useContext(SocketContext);
	const priorityData = [
		{ value: 'Highest', label: 'Highest' },
		{ value: 'High', label: 'High' },
		{ value: 'Medium', label: 'Medium' },
		{ value: 'Low', label: 'Low' },
		{ value: 'Lowest', label: 'Lowest' },
	]
	const dispatch = useDispatch();
	const [priority, setPriority] = useState({ value: 'Medium', label: 'Medium' });
	const { pathname } = useLocation();
	const handleUpdate = (data: any) => {
		dispatch(UpdateTaskAsyncAction(taskId, data, false, pathname, socket));
	};
	const handlePriorityChange = (event: any) => {
		setPriority(event);
		if (priority.value !== event.value) handleUpdate({ priority: event.value });
	};
	useEffect(() => {
		if (data) setPriority({ value: data, label: data });
	}, [data]);

	return (
		<SelectList
			name='priority'
			label='Priority'
			value={priority}
			callback={handlePriorityChange}
			data={priorityData}
		/>
	);
};

export default PrioritySelect;
