import React, { useState, useEffect, useContext } from 'react';
import { Socket } from "socket.io-client";
import { useDispatch, useSelector } from 'react-redux';
import { useLocation } from 'react-router';
import { UpdateTaskAsyncAction } from '../../../../store/task/asyncAction';
import { SelectList } from './list';
import { SocketContext } from '../../../../context/socket';

export const handleNotification = (socket: Socket | any, type: string, sender: string, reciever: string, senderId: string, recieverId: string, project: string, task: string) => {
	if (sender !== reciever && senderId !== recieverId) {
		socket.emit("sendNotification", { sender, reciever, senderId, recieverId, type, project, task })
	}
}

const AssigneeSelect = ({ data, taskId }: any) => {
	const socket = useContext(SocketContext);
	const dispatch = useDispatch();
	const { pathname } = useLocation();
	const [assignee, setAssignee] = useState<any>();
	const { user } = useSelector((state: any) => state.user);
	const teamMember = useSelector((state: any) => state.project.team);
	const members = teamMember.map((el: any) => { return el.member });

	const handleUpdate = (data: any) => {
		dispatch(UpdateTaskAsyncAction(taskId, data, false, pathname, socket));
	};
	const handleAssigneeChange = (event: any) => {
		setAssignee(event);
		const filteredUser = members.filter((el: any) => el._id === event.value)

		if (assignee === undefined || filteredUser[0]._id !== assignee.value) {
			handleUpdate({ assigne: event.value });
			socket && handleNotification(socket, 'assigned', user.username, filteredUser[0].username, user._id, event.value, user.project, taskId)
		}
	};
	// const userRole = (username: string, email: string) => {
	// 	const filter = teamMember.filter(
	// 		(el: any) => el.member.username === username && el.member.email === email
	// 	);
	// 	return filter.length > 0 && filter[0].role;
	// };
	useEffect(() => {
		if (data) { setAssignee({ value: data._id, label: data.name }) }
	}, [dispatch, data]);


	const mapDataMembers = (data: any) => {
		return data?.map((el: any) => {
			return {
				value: el._id,
				label: el.name
			}
		})
	}
	return (
		<SelectList
			label='Assignee'
			name='assignee'
			value={assignee}
			callback={handleAssigneeChange}
			data={members && mapDataMembers(members)}
		// disabled={
		// 	user && teamMember && userRole(user.username, user.email) !== 'manager'
		// }
		/>
	);
};

export default AssigneeSelect;
