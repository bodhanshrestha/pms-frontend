import React from 'react';
import { useSelector } from 'react-redux';

import Select, { StylesConfig } from 'react-select';

import './index.scss'

// const dot = (color = 'transparent') => ({
//   alignItems: 'center',
//   display: 'flex',

//   ':before': {
//     backgroundColor: color,
//     borderRadius: 10,
//     content: '" "',
//     display: 'block',
//     marginRight: 8,
//     height: 10,
//     width: 10,
//   },
// });

export default function SearchableSelect({
  data,
  callback,
  placeholder,
  value,
  loading,
  name,
  className,
  isDisabled,
  isClearable,
  isSearchable,
  onInputChange }: any) {

  const { dark_mode } = useSelector((state: any) => state.user.user)

  const colourStyles: StylesConfig<any> = {
    control: (styles) => ({ ...styles, backgroundColor: dark_mode ? '#424242' : 'white' }),
    option: (styles, { data, isDisabled, isFocused, isSelected }) => {
      return {
        ...styles,
        backgroundColor: isDisabled
          ? undefined
          : isSelected
            ? dark_mode ? "#bdbdbd" : '#e0e0e0'
            : isFocused
              ? dark_mode ? "#bdbdbd" : '#eeeeee'
              : undefined,
        color: isDisabled
          ? '#ccc'
          : isSelected
            ? 'black'
            : 'black',
        cursor: isDisabled ? 'not-allowed' : 'default',

        ':active': {
          ...styles[':active'],
          backgroundColor: !isDisabled
            ? isSelected
              ? data.color
              : dark_mode ? "#bdbdbd" : '#e0e0e0'
            : undefined,
        },
      };
    },
    input: (styles) => ({ ...styles }),
    placeholder: (styles) => ({ ...styles }),
    singleValue: (styles, { data }) => ({ ...styles }),
  };

  return (

    <Select
      classNamePrefix="select"
      onChange={callback}
      options={data}
      placeholder={placeholder}
      value={value}
      isLoading={loading}
      name={name}
      className={`${dark_mode ? "input-select-custom-dark" : "input-select-custom-light"} ${className}`}
      isDisabled={isDisabled}
      isClearable={isClearable}
      isSearchable={isSearchable}
      onInputChange={onInputChange}
      styles={colourStyles}
    />
  );
}