import { Grid } from '@material-ui/core';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation, useParams } from 'react-router';
import PaperComponent from '../../../components/paper';
import { fetchReviewTask } from '../../../store/task/action';
import { fetchTasksByID } from '../../../store/task/asyncAction';
import TaskTitle from './changeable/title';
import Summary from './summary';
import LeftDetails from './rightSection/detail';
import { ProjectTeamMemberAsyncAction } from '../../../store/project/asyncAction';
import { fetchGanttTasks } from '../../../store/gantt-task/asyncAction';
import TaskSkeleton from './skeleton';

const MainTaskView = ({ id }: { id?: string }) => {
	const dispatch = useDispatch();
	const location = useLocation();
	const params: any = useParams();
	const { review } = useSelector((state: any) => state.task);
	const ganttTasks = useSelector((state: any) => state.ganttTasks);

	useEffect(() => {
		if (id) dispatch(fetchTasksByID(id));
		dispatch(ProjectTeamMemberAsyncAction());
		dispatch(fetchGanttTasks());
		if (location.pathname.includes('/task/')) dispatch(fetchTasksByID(params.id));
		return () => {
			dispatch(fetchReviewTask({}));
		};
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [location]);
	const GridView = () => (
		<Grid container spacing={2}>
			<Grid item xs={12} sm={8} md={8}>
				<div className='w-100'>
					<TaskTitle label={review && review.title} taskId={review._id} />
				</div>
				<div className='w-100 mt-40'>
					<Summary data={review && review.summary} taskId={review._id} />
				</div>
			</Grid>

			<Grid item xs={12} sm={4} md={4}>
				<div className='w-100 task-container'>
					<LeftDetails data={review && review} epicTasks={review && review?.epic?._id} epic={ganttTasks.tasks} />
				</div>
			</Grid>
		</Grid>
	);
	return (
		<div>
			{review && ganttTasks && Object.keys(ganttTasks).length > 0 && Object.keys(review).length > 0 ? (
				<>
					{location.pathname.includes('/task/') ? (
						<PaperComponent className='p-20'>
							<GridView />
						</PaperComponent>
					) : (
						<GridView />
					)}
				</>
			) : (
				<div className='flex-center loader-main'>
					<TaskSkeleton />
				</div>
			)}
		</div>
	);
};

export default MainTaskView;
