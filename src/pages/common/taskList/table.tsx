import React, { useState, useEffect } from 'react';
import { Table } from 'antd';
import { columns } from './columns/backlog';
import { columns as momentColumn } from './columns/moment';
import { columns as issuesColumn } from './columns/isues';
import PopupDrawer from './drawer';
import FormDialogComponent from '.';
import '../../../darkMode.scss';
import { useSelector } from 'react-redux';
import { useLocation } from 'react-router';
import './index.scss';
const CommonTaskTable = ({ data, selectCheckbox, loading, header, pagination = false }: any) => {
	const location = useLocation();
	const dark_mode = useSelector((state: any) => state.user.user.dark_mode);
	const [selectedTasksId, setSelectedTasksId] = useState<React.Key[]>([]);
	const [selectedRecord, setSelectedRecord] = useState({});
	const rowSelection = {
		onChange: (selectedRowKeys: React.Key[], selectedRows: any[]) => {
			setSelectedTasksId(selectedRowKeys);
		},
	};
	const [popUp, setPopUp] = useState({
		record: {},
		visible: false,
	});
	const [openDialogBox, setOpenDialogBox] = React.useState({
		visible: false,
		record: {},
	});
	const [clickEvent, setClickEvent] = useState(true);
	useEffect(() => {
		if (location.pathname === '/sprint' || location.pathname === '/issues' || location.pathname === '/milestone') setClickEvent(false);
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [clickEvent]);
	return (
		<>
			<Table
				id='backlog_table'
				className={`w-100  ${dark_mode ? 'common_table_list_dark' : ''}`}
				pagination={pagination}
				title={header ? () => header : undefined}
				rowSelection={
					selectCheckbox
						? {
							type: 'checkbox',
							...rowSelection,
						}
						: undefined
				}
				rowKey={(record: any) => record._id}
				dataSource={data}
				columns={location.pathname === '/milestone' ? momentColumn : location.pathname === '/issues' ? issuesColumn : columns}
				rowClassName='pointer'
				loading={loading}
				onRow={(record, rowIndex) => {
					return {
						onClick: (event) => {
							event.preventDefault();
							setSelectedRecord(record);
							setOpenDialogBox({
								visible: true,
								record: record,
							});
						}, // click row

						onContextMenu: (event) => {
							event.preventDefault();
							if (clickEvent) {
								if (!popUp.visible) {
									document.addEventListener(`click`, function onClickOutside() {
										setPopUp({ ...popUp, visible: false });
										document.removeEventListener(`click`, onClickOutside);
									});
								}
								setPopUp({
									record: record,
									visible: true,
								});
							}
						}, // right button click row
					};
				}}
			/>
			<PopupDrawer
				record={popUp.record}
				loading={loading}
				show={popUp.visible}
				selectedTasksId={selectedTasksId}
				setOpenDialogBox={setOpenDialogBox}
			/>
			<FormDialogComponent
				data={selectedRecord}
				open={openDialogBox}
				setOpen={setOpenDialogBox}
			/>
		</>
	);
};

export default CommonTaskTable;
