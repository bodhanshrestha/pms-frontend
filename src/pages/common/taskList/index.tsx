import React from 'react';
import {
	Dialog,
	DialogTitle,
	DialogContent,
	IconButton,
	Paper,
} from '@material-ui/core';
import { Close } from '@material-ui/icons';
import Draggable from 'react-draggable';

import TopMenu from './topMenu';
import { useHistory, useLocation } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';
import { setCurrentSprint } from '../../../store/sprint/asyncAction';
import TypographyComponent from '../../../components/typography';
import MainTaskView from './main';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { fetchReviewTask } from '../../../store/task/action';
import { managerAndManagementRole } from '../../../utils/role';
function PaperComponent(props: any) {
	return (
		<Draggable
			handle='#draggable-dialog-title'
			cancel={'[class*="MuiDialogContent-root"]'}
		>
			<Paper {...props} />
		</Draggable>
	);
}

export default function FormDialogComponent({ data, open, setOpen }: any) {
	const location = useLocation();
	const history = useHistory();
	const dispatch = useDispatch();
	const { team } = useSelector((state: any) => state.project)
	const handleClose = () => {
		if (location.pathname === '/sprint') {
			dispatch(setCurrentSprint());
		}
		dispatch(fetchReviewTask({}));
		setOpen({
			...open,
			visible: false,
		});
	};
	const CustomDialogTitle = (props: any) => {
		const { children, onClose, ...other } = props;

		return (
			<DialogTitle {...other}>
				<div className='w-100 flex-start'>
					<div style={{ width: '400px' }}>{children}</div>
					<div className='w-100 flex-end'>
						<IconButton aria-label='close' onClick={onClose}>
							<ExitToAppIcon
								onClick={() => {
									history.push(`/task/${data._id}`);
								}}
							/>
						</IconButton>
						{
							team && managerAndManagementRole(team) && (
								<TopMenu taskId={data._id} handleClose={handleClose} />
							)
						}
						{onClose ? (
							<IconButton aria-label='close' onClick={onClose}>
								<Close />
							</IconButton>
						) : null}
					</div>
				</div>
			</DialogTitle>
		);
	};

	return (
		<form noValidate autoComplete='off' onSubmit={(e) => e.preventDefault()}>
			<Dialog
				open={open.visible}
				onClose={handleClose}
				fullWidth={true}
				PaperComponent={PaperComponent}
				aria-labelledby='draggable-dialog-title'
				maxWidth='lg'
			>
				<CustomDialogTitle
					style={{ cursor: 'move' }}
					onClose={handleClose}
					id='draggable-dialog-title'
				>
					<TypographyComponent label={data.key} color='textSecondary' />
				</CustomDialogTitle>
				<DialogContent>
					<MainTaskView id={data._id} />
				</DialogContent>
			</Dialog>
		</form>
	);
}
