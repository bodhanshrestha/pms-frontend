import React, { useState, useEffect } from 'react';

import HtmlEditor, {
	Toolbar,
	MediaResizing,
	Item,
} from 'devextreme-react/html-editor';

import AccordionComponent from '../../../../components/accordion';
import TypographyComponent from '../../../../components/typography';
import { useDispatch, useSelector } from 'react-redux';
import { UpdateTaskAsyncAction } from '../../../../store/task/asyncAction';
import { useLocation } from 'react-router';
import './dark.scss'
import './light.scss'
const sizeValues = ['8pt', '10pt', '12pt', '14pt', '18pt', '24pt', '36pt'];
const fontValues = [
	'Arial',
	'Courier New',
	'Georgia',
	'Impact',
	'Lucida Console',
	'Tahoma',
	'Times New Roman',
	'Verdana',
];
const headerValues = [false, 1, 2, 3, 4, 5];

export function Summary({ data, taskId }: any) {
	const dispatch = useDispatch();

	const { pathname } = useLocation();
	const [htmlData, setHtmlData] = useState('');
	const handleChange = (e: any) => {
		setHtmlData(e.value);
	};
	const handleSubmit = () => {

		dispatch(
			UpdateTaskAsyncAction(taskId, { summary: htmlData }, false, pathname,)
		);

	};
	const { dark_mode } = useSelector((state: any) => state.user.user)

	useEffect(() => {
		require('devextreme/dist/css/dx.light.css');
	}, []);
	return (
		<AccordionComponent
			header={<TypographyComponent label='Description' variant='h6' />}
		>
			<div className={`widget-container ${dark_mode ? 'darkMode' : 'lightMode'}`}>
				<HtmlEditor
					height='345px'
					defaultValue={data}
					valueType='html'
					onValueChanged={handleChange}
					onFocusOut={handleSubmit}

				>
					<MediaResizing enabled={true} />
					<Toolbar multiline={true}>
						<Item name='separator' />
						<Item name='size' acceptedValues={sizeValues} />
						<Item name='font' acceptedValues={fontValues} />
						<Item name='separator' />
						<Item name='bold' />
						<Item name='italic' />
						<Item name='strike' />
						<Item name='underline' />
						<Item name='separator' />
						<Item name='alignLeft' />
						<Item name='alignCenter' />
						<Item name='alignRight' />
						<Item name='alignJustify' />
						<Item name='separator' />
						<Item name='orderedList' />
						<Item name='bulletList' />
						<Item name='separator' />

						<Item name='header' acceptedValues={headerValues} />
						<Item name='separator' />
						<Item name='color' />
						<Item name='background' />
						<Item name='separator' />

						<Item name='clear' />
						<Item name='codeBlock' />
						<Item name='blockquote' />
					</Toolbar>
				</HtmlEditor>
			</div>
		</AccordionComponent>
	);
}

export default Summary;
