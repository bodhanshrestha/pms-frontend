import React from 'react';
import Skeleton from '@material-ui/lab/Skeleton';
import { Divider } from '@material-ui/core';
import { Paper } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
const SubSkeleton = () => {
  return (
    <Paper elevation={3} className='dashboard-project-card'>
      <Skeleton variant="rect" width={210} height={20} />
      <Divider style={{ margin: '15px 0px 10px 0px' }} />
      <div style={{ display: 'flex', justifyContent: 'flex-start', gap: '20px', padding: '5px 0px' }}>
        <div >
          <Skeleton variant="rect" width={50} height={50} style={{ marginBottom: '5px' }} />
          <Skeleton variant="rect" width={50} height={10} />
        </div>
        <div style={{ marginTop: '5px' }}>
          <Skeleton variant="rect" width={250} height={20} />
          <Skeleton variant="rect" width={100} height={10} style={{ marginTop: '15px' }} />
        </div>
      </div>
    </Paper>
  )
}
const SubSkeletonMultiple = () => {
  return (
    <Paper elevation={3} className='dashboard-project-card'>
      <Skeleton variant="rect" width={210} height={20} />
      <Divider style={{ margin: '15px 0px 10px 0px' }} />
      <div style={{ display: 'flex', justifyContent: 'flex-start', gap: '20px', padding: '5px 0px' }}>
        <div >
          <Skeleton variant="rect" width={50} height={50} style={{ marginBottom: '5px' }} />
          <Skeleton variant="rect" width={50} height={10} />
        </div>
        <div style={{ marginTop: '5px' }}>
          <Skeleton variant="rect" width={250} height={20} />
          <Skeleton variant="rect" width={100} height={10} style={{ marginTop: '15px' }} />
        </div>
      </div>
      <div style={{ display: 'flex', justifyContent: 'flex-start', gap: '20px', padding: '5px 0px' }}>
        <div >
          <Skeleton variant="rect" width={50} height={50} style={{ marginBottom: '5px' }} />
          <Skeleton variant="rect" width={50} height={10} />
        </div>
        <div style={{ marginTop: '5px' }}>
          <Skeleton variant="rect" width={250} height={20} />
          <Skeleton variant="rect" width={100} height={10} style={{ marginTop: '15px' }} />
        </div>
      </div>
    </Paper>
  )
}

const DashboardSkeleton = () => {
  return (
    <div className='dashboard-project-card'>
      <Grid container spacing={5} direction='row' justify='flex-start'>
        <Grid item xs={6} className='max-dashboard-card'>
          <SubSkeleton />
        </Grid>
        <Grid item xs={6} className='max-dashboard-card'>
          <SubSkeletonMultiple />
        </Grid>
      </Grid>
    </div>
  )
};

export default DashboardSkeleton;
