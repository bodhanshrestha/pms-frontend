import React from 'react';
import ListItemText from '@material-ui/core/ListItemText';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import TypographyComponent from '../../components/typography';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Collapse from '@material-ui/core/Collapse';
import { useState } from 'react';
import './card-dashboard.scss';
import { useSelector } from 'react-redux';
import AvatarComponent from '../../components/avatar';
import { Link } from 'react-router-dom';

const CardDashboard = () => {
	const { name, leader, team, image } = useSelector(
		(state: any) => state.project
	);

	const [open] = useState(true);

	const CardDashboardProp = ({
		head,
		listHead,
		subList,
		subListHead,
		link,
		list,
		avatar,
	}: any) => {
		return (
			<Paper elevation={3} className='dashboard-project-card'>
				<TypographyComponent
					variant='h5'
					label={head}
					className='typo-card-dashboard'
				/>
				<Divider />
				<Collapse in={open} timeout='auto' unmountOnExit>
					<List component='nav' aria-label='main mailbox folders'>
						{list ? (
							<>
								{list.map((el: any, i: number) => {
									if (i <= 4) {
										return (
											<Link to={`/profile?username=${el.username}`} key={i}>
												<ListItem button>
													<div className='dashboard-list-item gap-10'>
														<ListItemIcon>
															<AvatarComponent
																src={el.avatar}
																variant='square'
																alt={`image-of-${el.username}`}
															/>
														</ListItemIcon>
														<ListItemText primary={el.name} />
													</div>
												</ListItem>
											</Link>
										);
									}
									return false;
								})}
								{list.length > 4 && (
									<Link to='/people'>
										<ListItem button>
											<ListItemText primary='Show More' />
										</ListItem>
									</Link>
								)}
							</>
						) : (
							<Link to={`${link}`}>
								<ListItem button>
									<div className='dashboard-list-item gap-10'>
										<ListItemIcon>
											<AvatarComponent
												src={avatar}
												variant='square'
												alt={`image-of-project`}
											/>
										</ListItemIcon>
										<ListItemText primary={listHead} />
									</div>
									<div className='dashboard-list-item gap-20'>
										<TypographyComponent variant='caption' label={subList} />
										<TypographyComponent
											variant='caption'
											label={subListHead}
										/>
									</div>
								</ListItem>
							</Link>
						)}
					</List>
				</Collapse>
			</Paper>
		);
	};
	return (
		<div className='dashboard-project-card'>
			<Grid container spacing={5} direction='row' justify='flex-start' wrap='wrap'>
				<Grid item xs={12} className='max-dashboard-card'>
					<CardDashboardProp
						link='/project'
						head='Project Dashboard'
						listHead={name}
						subList='Lead'
						subListHead={leader}
						avatar={image}
					/>
				</Grid>
				<Grid item xs={12} className='max-dashboard-card'>
					<CardDashboardProp
						head='User Dashboard'
						listHead='Project Name'
						list={
							team &&
							team.map((el: any) => {
								return {
									role: el.role,
									name: el.member.name,
									username: el.member.username,
									avatar: el.member.avatar,
								};
							})
						}
					/>
				</Grid>
			</Grid>
		</div>
	);
};

export default CardDashboard;
