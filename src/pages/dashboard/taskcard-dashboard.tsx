import React from 'react';
import TypographyComponent from '../../components/typography';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
const TaskCardDashboard = ({ props }: any) => {
  const [progress, setProgress] = React.useState(10);

  React.useEffect(() => {
    const timer = setInterval(() => {
      setProgress((prevProgress) =>
        prevProgress >= 100 ? 0 : prevProgress + 10
      );
    }, 800);
    return () => {
      clearInterval(timer);
    };
  }, []);
  return (
    <div>
      <Grid container spacing={5} direction='row' justify='flex-start'>
        <Grid item xs={3}>
          <Paper elevation={3} className='dashboard-project-card'>
            <TypographyComponent
              variant='h6'
              label='Assigned Task'
              className='typo-card-dashboard'
            />
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
};

export default TaskCardDashboard;
