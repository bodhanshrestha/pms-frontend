import React, { useEffect } from 'react';
import CardDashboard from './card-dashoard';
import { useDispatch, useSelector } from 'react-redux';
import { ProjectByIdAsyncAction } from '../../store/project/asyncAction';
import { useHistory } from 'react-router';
import { fetchUserAsyncAction } from '../../store/user/asyncAction';
import DashboardSkeleton from './skeleton';
const Dashboard = () => {
	const dispatch = useDispatch();
	const history = useHistory();
	const auth = useSelector((state: any) => state.auth);
	const project = useSelector((state: any) => state.project);

	useEffect(() => {
		const project = localStorage.getItem('project')?.toString()
		if (project) {
			dispatch(ProjectByIdAsyncAction(project));
		}
		if (auth.username) dispatch(fetchUserAsyncAction(auth.username));
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [dispatch, history]);
	return (
		<div>
			{
				Object.keys(project).length && project.team ? (
					<CardDashboard />
				) : (
					<DashboardSkeleton />
				)
			}
		</div>
	);
};

export default Dashboard;
