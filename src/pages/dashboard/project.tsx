import React from 'react';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import TypographyComponent from '../../components/typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import './project.scss';
import { Divider } from '@material-ui/core';
import { useSelector } from 'react-redux';
import {
	DashboardProjectAccProps,
	DashboardProjectAccDataProps,
} from './types';

import ButtonComponent from '../../components/button';

const DashboardProject = () => {
	const [expanded, setExpanded] = React.useState<string | false>('panel1');
	const [expanded1, setExpanded1] = React.useState<string | false>('panel2');
	const projectTitle = useSelector((state: any) => state.project.name);

	const handleChange =
		(panel: string) => (event: React.ChangeEvent<{}>, isExpanded: boolean) => {
			if (panel === 'panel2') return setExpanded1(isExpanded ? panel : false);
			setExpanded(isExpanded ? panel : false);
		};

	const DashboardProjectAccordion = ({
		head,
		data,
		acc,
	}: DashboardProjectAccProps) => {
		return (
			<Accordion
				expanded={acc === 'panel2' ? expanded1 === acc : expanded === acc}
				onChange={handleChange(acc)}
			>
				<AccordionSummary
					expandIcon={<ExpandMoreIcon />}
					aria-controls='panel2bh-content'
					id='panel2bh-header'
				>
					<TypographyComponent label={head} variant='h6' />
				</AccordionSummary>
				<AccordionDetails>
					{data.map((element: DashboardProjectAccDataProps, i: number) => {
						return (
							<div>
								<Divider style={{ marginBottom: '15px' }} />
								<div className='acc-detail'>
									<TypographyComponent
										label={element.title}
										className='typo-1'
									/>
									<TypographyComponent
										className='typo-2'
										label={element.description}
										color='textSecondary'
									/>
								</div>
								<div>
									<ButtonComponent
										label={element.chip}
										className='type'
										variant='contained'
										disabled
									/>
								</div>
							</div>
						);
					})}
				</AccordionDetails>
			</Accordion>
		);
	};
	return (
		<div className='dashboard-accordion'>
			<DashboardProjectAccordion
				head='Recent Project'
				data={[
					{
						title: projectTitle,
						description: 'Project Motto or Objectives',
						chip: 'Project Type',
					},
				]}
				acc='panel1'
			/>
			<DashboardProjectAccordion
				head='Other Project'
				data={[
					{
						title: 'Other Project Name 1',
						description: 'Project Motto or Objectives',
						chip: 'Project Type',
					},
					{
						title: 'Other Project Name 2',
						description: 'Project Motto or Objectives',
						chip: 'Project Type',
					},
				]}
				acc='panel2'
			/>
		</div>
	);
};
export default DashboardProject;
