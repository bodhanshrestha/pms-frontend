import React, { useEffect, useState } from 'react'

import IconButton from '@material-ui/core/IconButton';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';

import { useFormik } from 'formik';
import * as yup from 'yup';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import ButtonComponent from './../../../components/button/index';
import InputField from './../../../components/form/text';
import TypographyComponent from '../../../components/typography';
import { forgetPassword } from '../../../store/auth/asyncAction';


const validationSchema = yup.object({
  password: yup
    .string()
    .required('Please Enter your password')
    .matches(
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})/,
      'Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and one special case Character'
    ),
  confirmPassword: yup
    .string()
    .required('Confirm Password is required')
    .oneOf([yup.ref('password'), null], 'Passwords must match'),
});
const ForgetPasswordPage = () => {
  const history = useHistory();
  let params = new URLSearchParams(history.location.search);
  const hash = params.get('hash');
  const email = params.get('email');
  useEffect(() => {
    if (!hash || !email) {
      history.replace('/login')
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [hash])
  const [showPassword, setShowPassword] = useState(false);
  const [loading, setLoading] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);

  const dispatch = useDispatch();
  const formik = useFormik({
    initialValues: {
      password: '',
      showPassword: false,
      confirmPassword: false,
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      setLoading(true)
      if (email) dispatch(forgetPassword(email, values.password, history, setLoading));
    },
  });

  const handleShowPassword = () => {
    setShowPassword(!showPassword);
  };
  const handleConfirmShowPassword = () => {
    setShowConfirmPassword(!showConfirmPassword);
  };
  return (
    <div className="flex-center" style={{ width: '100%', height: '80vh' }}>
      <div>
        <TypographyComponent label='Change Password' align='left' variant='h4' />

        <form
          noValidate
          autoComplete='off'
          onSubmit={formik.handleSubmit}
          style={{ width: '500px', marginTop: '20px' }}
        >

          <InputField
            label='Password'
            name='password'
            type={showPassword ? 'text' : 'password'}
            placeholder='Enter Your Password'
            value={formik.values.password}
            callback={formik.handleChange}
            error={formik.touched.password && Boolean(formik.errors.password)}
            helperText={formik.touched.password && formik.errors.password}
            endAdornment={
              <IconButton
                aria-label='toggle password visibility'
                onClick={handleShowPassword}
              >
                {showPassword ? <Visibility /> : <VisibilityOff />}
              </IconButton>
            }
          />
          <InputField
            label='Confirm Password'
            name='confirmPassword'
            type={showConfirmPassword ? 'text' : 'password'}
            placeholder='Confirm Your Password'
            value={formik.values.confirmPassword}
            callback={formik.handleChange}
            error={
              formik.touched.confirmPassword &&
              Boolean(formik.errors.confirmPassword)
            }
            helperText={formik.errors.confirmPassword}
            endAdornment={
              <IconButton
                aria-label='toggle confirm password visibility'
                onClick={handleConfirmShowPassword}
              >
                {showConfirmPassword ? <Visibility /> : <VisibilityOff />}
              </IconButton>
            }
          />


          <ButtonComponent
            label='Submit'
            type='submit'
            variant='contained'
            color='primary'
            className='w-100'
            disabled={loading}
          />
        </form>

      </div>
    </div>
  )


}

export default ForgetPasswordPage
