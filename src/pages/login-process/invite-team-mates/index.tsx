import React, { useEffect, useState } from 'react';
import './style.scss';
import Typography from '../../../components/typography';
import { useHistory } from 'react-router-dom';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { useDispatch } from 'react-redux';
import { inviteTeamAsyncAction } from '../../../store/user/asyncAction';
import InitialStepper from '../../../components/stepper';
import FormPart from './form';
import image from '../../../images/inviteteam.svg';
import Grid from '@material-ui/core/Grid';

const validationSchema = yup.object({
	first: yup.string().email('Enter a Valid Email'),

	second: yup.string().email('Enter a Valid Email'),

	third: yup.string().email('Enter a Valid Email'),
});
const InviteTeam = (): any => {
	const history = useHistory();
	const dispatch = useDispatch();
	const [submitting, setSubmitting] = useState(false)

	const formik = useFormik({
		initialValues: {
			first: '',
			second: '',
			third: '',
		},
		validationSchema: validationSchema,
		onSubmit: (values) => {
			setSubmitting(true)

			dispatch(inviteTeamAsyncAction(values, history));
		},
	});
	const handleSkipButton = () => {
		history.replace('/dashboard');
	};
	useEffect(() => {

		return () => {
			setSubmitting(false)
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);
	return (
		<div className='invite-team'>
			<Typography
				className='typo-2'
				variant='h4'
				align='center'
				label=' Invite your teammates'
			/>

			<div className='whole-part'>
				<Grid container spacing={3}>
					<Grid item xs={12} sm={6} md={8} xl={8}>
						<div className='left-part'>
							<img src={image} alt='task' className='invite-team-left-img' />
						</div>
					</Grid>
					<Grid item xs={12} sm={6} md={4} xl={4}>
						<div className='w-100 h-100 flex-center'>
							<form
								noValidate
								autoComplete='off'
								className='flex-center-column'
								onSubmit={formik.handleSubmit}>
								<FormPart formik={formik} disabled={submitting} handleSkipButton={handleSkipButton} />
							</form>
						</div>
					</Grid>
				</Grid>
			</div>

			<InitialStepper active={3} complete={{ 0: true, 1: true, 2: true }} />
		</div>
	);
};
export default InviteTeam;
