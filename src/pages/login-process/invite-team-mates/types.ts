export type InviteTeamFormProps = {
  formik?: any;
  disabled?: boolean;
  handleSkipButton?: () => void;
};
