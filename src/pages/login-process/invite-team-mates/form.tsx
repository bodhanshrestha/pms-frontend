import React from "react";
import ButtonComponent from "../../../components/button";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import InputField from "../../../components/form/text";
import { InviteTeamFormProps } from "./types";

const FormPart = ({ formik, handleSkipButton, disabled }: InviteTeamFormProps) => {
  return (
    <div>
      <InputField
        type='email'
        label='First Teammate'
        placeholder="Enter First Teammate's  Email Address"
        name='first'
        width='400px'
        variant='outlined'
        value={formik.values.first}
        callback={formik.handleChange}
        error={formik.touched.first && Boolean(formik.errors.first)}
        helperText={formik.touched.first && formik.errors.first}
      />

      <InputField
        type='email'
        label='Second Teammate'
        placeholder="Enter Second Teammate's Email Address"
        variant='outlined'
        name='second'
        width='400px'
        value={formik.values.second}
        callback={formik.handleChange}
        error={formik.touched.second && Boolean(formik.errors.second)}
        helperText={formik.touched.second && formik.errors.second}
      />

      <InputField
        type='email'
        label='Third Teammate'
        placeholder="Enter Third Teammate's Email Address"
        variant='outlined'
        name='third'
        width='400px'
        value={formik.values.third}
        callback={formik.handleChange}
        error={formik.touched.third && Boolean(formik.errors.third)}
        helperText={formik.touched.third && formik.errors.third}
      />

      <div className='group-button'>
        <ButtonComponent
          className='first-button'
          label='SKIP'
          variant='outlined'
          color='primary'
          endIcon={<ArrowForwardIcon />}
          callback={handleSkipButton}
        />

        <ButtonComponent
          type='submit'
          className='second-button'
          label='CONTINUE'
          variant='outlined'
          color='primary'
          disabled={disabled}
          endIcon={<ArrowForwardIcon />}
        />
      </div>
    </div>
  );
};

export default FormPart;
