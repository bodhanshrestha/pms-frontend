import React, { useState, useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import TypographyComponent from '../../../components/typography';
import './style.scss';
import { useDispatch } from 'react-redux';
import { resendLink } from './../../../store/auth/asyncAction';
import ButtonComponent from './../../../components/button/index';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import ResendWarning from './alertDialog';
const EmailVerification = (props: any) => {
	const location: any = useLocation();
	const [username, setUsername] = useState<string>('');
	const [email, setEmail] = useState<string>('');
	const [openDialog, setOpenDialog] = useState(false)
	const dispatch = useDispatch()
	const ResendEmail = () => {
		dispatch(resendLink(email, username));
	};
	useEffect(() => {
		if (location.state !== undefined) {
			setUsername(location.state.username);
			setEmail(location.state.email);
		}
		return () => { };
	}, [location.state]);

	return (
		<div className='main'>
			<div className='image-section' />
			<div className='text-section'>
				<TypographyComponent
					label={`Almost done, ${username}`}
					variant='h3'
					className='m-20'
				/>
				<TypographyComponent
					label={`Please Verify Your Email Address ( ${email} )`}
					variant='h6'
					className='m-20'
				/>

				<p>
					We’ve Sent You An Email To Users Email Address.
					<br />
					Please Check Your Inbox And Click To Link To Activate Your Account.
				</p>
				<ButtonComponent
					label="Go Back to Application"
					callback={() => props.history.push("/login")}
					startIcon={<ArrowBackIcon />}
					variant='contained' color='primary' />

				<div className='lines'>
					<div className='line'></div>
					<div className='or'>Or</div>
					<div className='line'></div>
				</div>
				<p>
					Didn't Receive An Email?{' '}
					<span onClick={() => setOpenDialog(true)} className='resend'>
						Resend Email.
					</span>
				</p>
				<ResendWarning open={openDialog} setOpen={setOpenDialog} handleClick={ResendEmail} email={email} />
			</div>
		</div >
	);
};

export default EmailVerification;
