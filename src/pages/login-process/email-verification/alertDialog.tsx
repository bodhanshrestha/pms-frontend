import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import ButtonComponent from './../../../components/button/index';

export default function ResendWarning({ open, setOpen, handleClick, email }: any) {


  const handleClickOpen = () => {
    handleClick();
    setOpen(false);
  };

  const handleClose = () => {
    if (setOpen) setOpen(false);
  };

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">Resend Email</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          Are you sure you want to resend email ({email}) for the verification of the registration process.
        </DialogContentText>
      </DialogContent>
      <DialogActions>

        <Button onClick={handleClose} color="secondary">
          Cancel
        </Button>

        <ButtonComponent
          label='Resend'
          variant='text'
          color='primary'
          callback={handleClickOpen}
        />
      </DialogActions>
    </Dialog>
  );
}
