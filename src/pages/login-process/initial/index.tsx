import React, { useEffect } from 'react';
import Typography from '../../../components/typography/index';
import ButtonComponent from '../../../components/button/index';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import { useHistory } from 'react-router-dom';
import './index.scss';
import { useDispatch, useSelector } from 'react-redux';
import { initialExternalLogin } from '../../../store/auth/asyncAction';
import { fetchUserAsyncAction } from '../../../store/user/asyncAction';

const FirstView = () => {
	let history = useHistory();
	const dispatch = useDispatch();
	let params = new URLSearchParams(history.location.search);
	const hash = params.get('hash');
	const username = params.get('username');
	const { user } = useSelector((state: any) => state.user);

	const handleButtonContinue = () => {
		if (hash) dispatch(initialExternalLogin(hash, history));
		else history.replace('/login');
	};

	useEffect(() => {
		if (username) dispatch(fetchUserAsyncAction(username));
		else {
			localStorage.clear();
			history.replace('/login');
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [dispatch]);

	return (
		<>
			<div className='initial-display'>
				<div className='initial-icon-head'>
					<img src={user && user.avatar} className='icon' alt='user' />
				</div>
				<Typography
					label={
						<span>
							WELCOME <span className='text-primary-color'>{username}</span>
						</span>
					}
					className='first-line '
				/>
				<Typography label='We Are Glad You Are Here' className='second-line' />
				<ButtonComponent
					label='Continue'
					endIcon={<ArrowForwardIcon />}
					color='primary'
					variant='outlined'
					className='button'
					callback={handleButtonContinue}
				/>
			</div>
		</>
	);
};

export default FirstView;
