import React from 'react';
import ButtonComponent from '../../../components/button';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import InputField from '../../../components/form/text';
import { ProjectTaskFormProps } from './type';

const FormProjectTask = ({
	formik,
	handleButtonSkip,
	disabled
}: ProjectTaskFormProps) => {
	return (
		<div>
			<InputField
				label='Task'
				placeholder='Enter Your Task'
				variant='outlined'
				width='400px'
				name='first'
				value={formik.values.first}
				callback={formik.handleChange}
			/>

			<InputField
				label='Task'
				placeholder='Enter Your Task'
				variant='outlined'
				name='second'
				width='400px'
				value={formik.values.second}
				callback={formik.handleChange}
			/>

			<InputField
				label='Task'
				placeholder='Enter Your Task'
				variant='outlined'
				name='third'
				width='400px'
				value={formik.values.third}
				callback={formik.handleChange}
			/>

			<div className='group-button'>
				<ButtonComponent
					className='first-button'
					label='SKIP'
					variant='outlined'
					color='primary'
					endIcon={<ArrowForwardIcon />}
					callback={handleButtonSkip}
				/>

				<ButtonComponent
					type='submit'
					className='second-button'
					label='CONTINUE'
					variant='outlined'
					color='primary'
					disabled={disabled}
					endIcon={<ArrowForwardIcon />}
				/>

			</div>
		</div>
	);
};

export default FormProjectTask;
