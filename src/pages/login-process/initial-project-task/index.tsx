import React, { useState, useEffect } from 'react';
import './style.scss';
import Typography from '../../../components/typography';
import { useHistory } from 'react-router-dom';
import { useFormik } from 'formik';
import { useSelector, useDispatch } from 'react-redux';
import { initialCreateTaskAsyncAction } from '../../../store/task/asyncAction';
import InitialStepper from '../../../components/stepper';
import FormProjectTask from './form';
import image from '../../../images/Creative.svg';
import Grid from '@material-ui/core/Grid';

const ProjectTask = (): any => {
	const dispatch = useDispatch();
	const [submitting, setSubmitting] = useState(false)
	let history = useHistory();
	const { name } = useSelector((state: any) => state.project);
	const formik = useFormik({
		initialValues: {
			first: '',
			second: '',
			third: '',
		},
		onSubmit: (values) => {
			setSubmitting(true)
			dispatch(initialCreateTaskAsyncAction(values, history));
		},
	});

	useEffect(() => {

		return () => {
			setSubmitting(false)
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);
	const handleButtonSkip = () => history.replace('/dashboard');
	return (
		<div className='setup-pt'>
			<Typography
				className='typo-2'
				variant='h4'
				align='center'
				label={
					<span>
						What are a few tasks that you have to do for
						<span className='text-primary-color'> {name}</span>
					</span>
				}
			/>

			<div className='whole-part'>
				<Grid container spacing={3}>
					<Grid item xs={12} sm={6} md={8} xl={8}>
						<div className='left-part'>
							<img src={image} alt='task' className='invite-team-left-img' />
						</div>
					</Grid>
					<Grid item xs={12} sm={6} md={4} xl={4}>
						<div className='w-100 h-100 flex-center'>
							<form
								noValidate
								autoComplete='off'
								className='flex-center-column'
								onSubmit={formik.handleSubmit}>
								<FormProjectTask
									formik={formik}
									disabled={submitting}
									handleButtonSkip={handleButtonSkip}
								/>
							</form>
						</div>
					</Grid>
				</Grid>
			</div>

			<InitialStepper active={1} complete={{ 0: true }} />
		</div>
	);
};

export default ProjectTask;
