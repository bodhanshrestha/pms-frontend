import React, { useEffect, useState } from 'react';
import Typography from '../../../components/typography';
import ButtonComponent from '../../../components/button/index';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import './index.scss';
import { useHistory } from 'react-router-dom';
import * as yup from 'yup';
import { useFormik } from 'formik';
import InputField from '../../../components/form/text';
import { useDispatch } from 'react-redux';
import { ProjectCreationAsyncAction } from '../../../store/project/asyncAction';
import InitialStepper from '../../../components/stepper';
import image from '../../../images/project-team.svg';

const validationSchema = yup.object({
	name: yup.string().required('Project name is required'),
});
const SetupProject = () => {
	let history: any = useHistory();
	const hash = localStorage.getItem('hash');
	const [submitting, setSubmitting] = useState(false)
	const dispatch = useDispatch();
	const formik = useFormik({
		initialValues: {
			name: '',
		},
		validationSchema: validationSchema,
		onSubmit: (values) => {
			setSubmitting(true)
			dispatch(ProjectCreationAsyncAction(values, hash, history, 'initial'));
			localStorage.removeItem('hash');
		},
	});
	useEffect(() => {
		if (!hash) {
			history.replace('/dashboard');
		}
		return () => {
			setSubmitting(false)
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);
	return (
		<div className='setup'>
			<Typography label='LET’S SET UP YOUR FIRST PROJECT' className='line1' />
			<img src={image} alt='project setup' className='initial-project-img' />
			<Typography
				label='Whats Something You And Your Team Are Currently Working On?'
				variant='h6'
				className='line2'
			/>
			<form noValidate autoComplete='off' onSubmit={formik.handleSubmit}>
				<InputField
					label='Project Name'
					name='name'
					type='text'
					width='400px'
					marginTop='40px'
					marginBottom='0px'
					value={formik.values.name}
					callback={formik.handleChange}
					error={formik.touched.name && Boolean(formik.errors.name)}
					helperText={formik.touched.name && formik.errors.name}
				/>
				<div className='for-button'>
					<ButtonComponent
						label='Continue'
						endIcon={<ArrowForwardIcon />}
						color='primary'
						variant='outlined'
						className='button'
						type='submit'
						disabled={submitting}
					/>
				</div>
			</form>
			<InitialStepper active={0} complete={{ 0: false }} />
		</div>
	);
};

export default SetupProject;
