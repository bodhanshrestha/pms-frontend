import React from 'react';
import ButtonComponent from '../../../../components/button';
import Typography from '../../../../components/typography';
import Login from '../../../../images/pic2.png';
import './index.scss';

const LeftPart = ({ isClicked, setValue }: any) => {
	const handleOnClick = (e: any) => {
		if (isClicked === 0) {
			setValue(1);
		} else {
			setValue(0);
		}
	};
	const MidContent = ({ className, label, pFirst, pSecond, btnLabel }: any) => (
		<div className={className}>
			<Typography variant='h2' label={label} className='typo' />
			<img src={Login} alt='login' height='50%' />
			<p>
				{pFirst}
				<br />
				{pSecond}
			</p>
			<ButtonComponent
				label={btnLabel}
				color='primary'
				className='btn-component'
				callback={handleOnClick}
			/>
		</div>
	);
	return (
		<div className='sign-left-part'>
			{isClicked === 0 ? (
				<MidContent
					className='left-sign-up'
					label='Welcome Back !'
					pFirst='To keep connected with us.Please login with your personal details'
					btnLabel='Sign Up'
				/>
			) : (
				<MidContent
					className='left-login'
					label='Hello Friend !'
					pFirst='Enter your personal details and start your journey with us'
					btnLabel='login'
				/>
			)}
		</div>
	);
};

export default LeftPart;
