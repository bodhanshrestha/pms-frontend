import React, { useState, useEffect } from 'react';

import Button from '@material-ui/core/Button';
import ButtonComponent from '../../../../components/button';
import IconButton from '@material-ui/core/IconButton';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import InputField from '../../../../components/form/text';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import { useFormik } from 'formik';
import * as yup from 'yup';
import './login.scss';
import { useDispatch, useSelector } from 'react-redux';
import { login, resendLink } from '../../../../store/auth/asyncAction';
import { useHistory } from 'react-router-dom';
import { loginAction } from '../../../../store/auth/action';
import AlertComponent from '../../../../components/alert';
import ChangePassword from './changePassword';

const validationSchema = yup.object({
	username: yup.string().required('Please enter your username'),
	password: yup.string().required('Please enter your password'),
});
const LoginComponent = () => {
	const dispatch = useDispatch();
	const [loading, setLoding] = useState(false)
	const { resendEmail, username, email } = useSelector(
		(state: any) => state.auth
	);
	const history = useHistory();
	const formik = useFormik({
		initialValues: {
			username: '',
			password: '',
			checked: false,
		},
		validationSchema: validationSchema,
		onSubmit: async (values) => {
			setLoding(true)
			dispatch(login(values, history, setLoding))

		},
	});
	const [showPassword, setShowPassword] = useState(false);

	const handleShowPassword = () => {
		setShowPassword(!showPassword);
	};
	const [alertOpen, setAlertOpen] = useState(false);
	useEffect(() => {
		dispatch(loginAction({}));
	}, [dispatch]);
	const checkResendEmail = () => {
		if (resendEmail) setAlertOpen(true);
	};
	useEffect(() => {
		localStorage.clear()
	}, [])

	useEffect(() => {
		checkResendEmail();
		return () => { };
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [resendEmail]);
	const handleResendEmail = () => {
		dispatch(resendLink(email, username));
		setAlertOpen(false);
	};
	return (
		<>
			<AlertComponent open={alertOpen} setOpen={setAlertOpen} type='warning' >
				<p>
					Your Account Is not Active.Registered {email && `in "${email}"`} with username "
					{username}". To active the Account You need to resend the email.
				</p>
				<Button onClick={handleResendEmail}>Resend Link to Email</Button>
			</AlertComponent>

			<br />
			<form noValidate autoComplete='off' onSubmit={formik.handleSubmit}>
				<InputField
					label='Username'
					name='username'
					type='text'
					placeholder='Enter Your Username'
					value={formik.values.username}
					callback={formik.handleChange}
					error={formik.touched.username && Boolean(formik.errors.username)}
					helperText={formik.touched.username && formik.errors.username}
					endAdornment={
						<IconButton>
							<AccountCircleIcon />
						</IconButton>
					}
				/>
				<InputField
					label='Password'
					name='password'
					type={showPassword ? 'text' : 'password'}
					placeholder='Enter Your Password'
					value={formik.values.password}
					callback={formik.handleChange}
					error={formik.touched.password && Boolean(formik.errors.password)}
					helperText={formik.touched.password && formik.errors.password}
					endAdornment={
						<IconButton
							aria-label='toggle password visibility'
							onClick={handleShowPassword}
						>
							{showPassword ? <Visibility /> : <VisibilityOff />}
						</IconButton>
					}
				/>

				<div className='bottom-container'>
					{/* <FormControlLabel
						className='sign-terms-check'
						control={
							<Checkbox
								color='primary'
								name='checked'
								checked={formik.values.checked}
								onChange={formik.handleChange}
								inputProps={{ 'aria-label': 'primary checkbox' }}
							/>
						}
						label='Remember Me'
					/> */}
					<div></div>
					<ChangePassword />
				</div>
				<div>
					<ButtonComponent
						label='Login'
						type='submit'
						variant='contained'
						className='w-100'
						color='primary'
						disabled={loading}
					/>
				</div>
			</form>
		</>
	);
};

export default LoginComponent;
