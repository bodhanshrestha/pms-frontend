import React from 'react';
import { makeStyles, Theme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import Box from '@material-ui/core/Box';
import LoginComponent from './login';
import SignComponent from './sign-up';
import LeftPart from './left-part/index';
import './index.scss';

interface TabPanelProps {
	children?: React.ReactNode;
	index: any;
	value: any;
}

function TabPanel(props: TabPanelProps) {
	const { children, value, index, ...other } = props;

	return (
		<div
			role='tabpanel'
			hidden={value !== index}
			id={`scrollable-force-tabpanel-${index}`}
			aria-labelledby={`scrollable-force-tab-${index}`}
			{...other}>
			{value === index && (
				<Box p={3}>
					<div>{children}</div>
				</Box>
			)}
		</div>
	);
}

function a11yProps(index: any) {
	return {
		id: `scrollable-force-tab-${index}`,
		'aria-controls': `scrollable-force-tabpanel-${index}`,
	};
}

const useStyles = makeStyles((theme: Theme) => ({
	root: {
		width: '100%',
		height: '100vh',
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'flex-start',

		backgroundColor: theme.palette.background.default,

	},
	left: {
		[theme.breakpoints.down('sm')]: {
			display: 'none'
		},
	}
}));

export default function SignProcessComp() {
	const classes = useStyles();
	const [value, setValue] = React.useState(0);

	const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
		setValue(newValue);
	};

	return (
		<div className={classes.root}>
			<div className={`${classes.left} sign-left`}>
				<LeftPart isClicked={value} setValue={setValue} />
			</div>
			<div className='sign-right'>
				<AppBar position='static' color='default'>
					<Tabs
						value={value}
						onChange={handleChange}
						indicatorColor='primary'
						textColor='primary'
						aria-label='scrollable force tabs example'>
						<Tab label='Login' icon={<AccountCircleIcon />} {...a11yProps(0)} />
						<Tab
							label='Sign up '
							icon={<AccountCircleIcon />}
							{...a11yProps(1)}
						/>
					</Tabs>
				</AppBar>
				<div className='tab-panel'>
					<TabPanel value={value} index={0}>
						<LoginComponent />
					</TabPanel>
					<TabPanel value={value} index={1}>
						<SignComponent />
					</TabPanel>
				</div>
			</div>
		</div>
	);
}
