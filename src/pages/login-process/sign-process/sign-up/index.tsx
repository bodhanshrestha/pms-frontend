import React, { useState } from 'react';
import Checkbox from '@material-ui/core/Checkbox';
import './style.scss';
import IconButton from '@material-ui/core/IconButton';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import PersonIcon from '@material-ui/icons/Person';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import EmailIcon from '@material-ui/icons/Email';
import InputField from '../../../../components/form/text';
import { useFormik } from 'formik';
import * as yup from 'yup';
import ButtonComponent from '../../../../components/button';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { register } from '../../../../store/auth/asyncAction';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import SelectInputField from './../../../../components/form/select';

const validationSchema = yup.object({
	email: yup
		.string()
		.email('Enter a valid email')
		.required('Email is required'),
	username: yup.string().required('Username is required'),
	password: yup
		.string()
		.required('Please Enter your password')
		.matches(
			/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})/,
			'Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and one special case Character'
		),
	confirmPassword: yup
		.string()
		.required('Confirm Password is required')
		.oneOf([yup.ref('password'), null], 'Passwords must match'),
});

const SignUp = (): any => {
	const [showPassword, setShowPassword] = useState(false);
	const [showConfirmPassword, setShowConfirmPassword] = useState(false);

	const dispatch = useDispatch();
	const history = useHistory();
	const formik = useFormik({
		initialValues: {
			name: '',
			email: '',
			username: '',
			password: '',
			gender: '',
			showPassword: false,
			confirmPassword: false,
			terms: true,
		},
		validationSchema: validationSchema,
		onSubmit: (values) => {
			dispatch(register(values, history));
		},
	});

	const handleShowPassword = () => {
		setShowPassword(!showPassword);
	};
	const handleConfirmShowPassword = () => {
		setShowConfirmPassword(!showConfirmPassword);
	};

	return (
		<form
			noValidate
			autoComplete='off'
			onSubmit={formik.handleSubmit}
			className='sign-up-form'
		>
			<InputField
				label='Full Name'
				name='name'
				type='text'
				value={formik.values.name}
				callback={formik.handleChange}
				endAdornment={
					<IconButton>
						<PersonIcon />
					</IconButton>
				}
			/>

			<InputField
				label='Email'
				name='email'
				type='email'
				value={formik.values.email}
				callback={formik.handleChange}
				error={formik.touched.email && Boolean(formik.errors.email)}
				helperText={formik.touched.email && formik.errors.email}
				endAdornment={
					<IconButton>
						<EmailIcon />
					</IconButton>
				}
			/>

			<InputField
				label='Username'
				name='username'
				type='text'
				placeholder='Enter Your Username'
				value={formik.values.username}
				callback={formik.handleChange}
				error={formik.touched.username && Boolean(formik.errors.username)}
				helperText={formik.touched.username && formik.errors.username}
				endAdornment={
					<IconButton>
						<AccountCircleIcon />
					</IconButton>
				}
			/>
			<InputField
				label='Password'
				name='password'
				type={showPassword ? 'text' : 'password'}
				placeholder='Enter Your Password'
				value={formik.values.password}
				callback={formik.handleChange}
				error={formik.touched.password && Boolean(formik.errors.password)}
				helperText={formik.touched.password && formik.errors.password}
				endAdornment={
					<IconButton
						aria-label='toggle password visibility'
						onClick={handleShowPassword}
					>
						{showPassword ? <Visibility /> : <VisibilityOff />}
					</IconButton>
				}
			/>
			<InputField
				label='Confirm Password'
				name='confirmPassword'
				type={showConfirmPassword ? 'text' : 'password'}
				placeholder='Confirm Your Password'
				value={formik.values.confirmPassword}
				callback={formik.handleChange}
				error={
					formik.touched.confirmPassword &&
					Boolean(formik.errors.confirmPassword)
				}
				helperText={formik.errors.confirmPassword}
				endAdornment={
					<IconButton
						aria-label='toggle confirm password visibility'
						onClick={handleConfirmShowPassword}
					>
						{showConfirmPassword ? <Visibility /> : <VisibilityOff />}
					</IconButton>
				}
			/>
			<div style={{ textAlign: 'left' }}>
				<SelectInputField
					label='Select Gender'
					name='gender'
					value={formik.values.gender}
					callback={formik.handleChange}
					placeholder='Task'
					className='w-100'
					data={[{
						id: 'male',
						name: 'Male'
					}, {
						id: 'female',
						name: 'Female'
					}]}

				/>
			</div>
			<FormControlLabel
				className='sign-terms-check'
				control={
					<Checkbox
						color='primary'
						name='terms'
						checked={formik.values.terms}
						onChange={formik.handleChange}
						inputProps={{ 'aria-label': 'secondary checkbox' }}
					/>
				}
				label='I agree to the terms and Privacy Policy'
			/>

			<ButtonComponent
				label='Sign Up'
				type='submit'
				variant='contained'
				color='primary'
				className='w-100'
			/>
		</form>
	);
};
export default SignUp;
