import React from 'react';
import AdbIcon from '@material-ui/icons/Adb';
import './index.scss';

const HeaderBar = () => {
	return (
		<div className='header-bar'>
			<AdbIcon fontSize='large' />
		</div>
	);
};

export default HeaderBar;
