import React from 'react';
import Typography from '../../../components/typography';
import Button from '../../../components/button/index';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import './index.scss';
import { useHistory } from 'react-router-dom';
import { useFormik } from 'formik';

import InputField from '../../../components/form/text';
import InitialStepper from '../../../components/stepper';

const TaskStages = (): any => {
	let history = useHistory();

	const formik = useFormik({
		initialValues: {
			deferred: 'DEFERRED/NOT INCLUDED',
			todo: 'TO-DO',
			inprogress: 'IN PROGRESS',
			qa: 'QA',
			done: 'DONE',
		},
		onSubmit: () => {
			history.replace(`/initial-invite-team`);
		},
	});
	const handleButtonSkip = () => history.replace('/dashboard');
	return (
		<div className='group-task-stages'>
			<Typography
				variant='h4'
				label='Default Section Or Stages Of Your Tasks'
				className='line-1'
			/>
			<form
				noValidate
				autoComplete='off'
				className='form-task-stages'
				onSubmit={formik.handleSubmit}>
				<InputField
					label='Project Stages'
					placeholder='Enter Your Project Stage'
					variant='outlined'
					name='deferred'
					width='400px'
					value={formik.values.deferred}
					disabled={true}
				/>

				<InputField
					label='Project Stages'
					placeholder='Enter Your Project Stage'
					variant='outlined'
					name='todo'
					width='400px'
					value={formik.values.todo}
					disabled={true}
				/>

				<InputField
					label='Project Stages'
					placeholder='Enter Your Project Stage'
					variant='outlined'
					width='400px'
					value={formik.values.inprogress}
					name='inprogress'
					disabled={true}
				/>

				<InputField
					label='Project Stages'
					placeholder='Enter Your Project Stage'
					variant='outlined'
					name='qa'
					width='400px'
					value={formik.values.qa}
					disabled={true}
				/>

				<InputField
					label='Project Stages'
					placeholder='Enter Your Project Stage'
					variant='outlined'
					name='done'
					width='400px'
					value={formik.values.done}
					disabled={true}
				/>
				<div className='group-button'>
					<Button
						label='Skip'
						endIcon={<ArrowForwardIcon />}
						color='primary'
						variant='outlined'
						className='button1'
						callback={handleButtonSkip}
					/>
					<Button
						type='submit'
						label='Continue'
						endIcon={<ArrowForwardIcon />}
						color='primary'
						variant='outlined'
						className='button2'
					/>
				</div>
			</form>
			<InitialStepper active={2} complete={{ 0: true, 1: true }} />
		</div>
	);
};

export default TaskStages;
