import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { useDispatch, useSelector } from 'react-redux';
import { ProjectCreationAsyncAction } from '../../store/project/asyncAction';
import { useHistory } from 'react-router-dom';
import ButtonComponent from '../../components/button';
import { projectLoading } from '../../store/project/action';

export default function NewProjectCreation() {
  const [open, setOpen] = React.useState(false);
  const dispatch = useDispatch()
  const history = useHistory()
  const [project, setProject] = useState('')
  const handleClickOpen = () => {
    setOpen(true);
  };
  const { loading } = useSelector((state: any) => state.project)
  const handleClose = () => {
    setOpen(false);
  };
  const handleCreate = () => {
    dispatch(projectLoading(true))
    dispatch(ProjectCreationAsyncAction({ name: project }, 'existing_user_new_project', history, 'new'));
  };

  return (
    <div style={{ textAlign: 'right' }}>
      <Button variant="contained" color="primary" onClick={handleClickOpen}>
        Create New Project
      </Button>
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Create New Project</DialogTitle>
        <DialogContent>
          <DialogContentText>
            To generate new project, please enter your project name here. you will be redirected to the new project initial phases.
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Project Name"
            type="email"
            fullWidth
            onChange={(e: any) => setProject(e.target.value)}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="secondary">
            Cancel
          </Button>
          <ButtonComponent
            label='Create'
            variant='text'
            color='primary'
            disabled={loading}
            callback={handleCreate}
          />
        </DialogActions>
      </Dialog>
    </div>
  );
}
