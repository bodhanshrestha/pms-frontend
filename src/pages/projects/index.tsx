import React from 'react'
import SelectProject from '../projectSelect';
import NewProjectCreation from './dialog';

const ProjectsPage = () => {
	return (
		<div>
			<NewProjectCreation />
			<SelectProject />
		</div>
	)
}

export default ProjectsPage
