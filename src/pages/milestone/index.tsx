import React, { useContext, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import AccordionComponent from '../../components/accordion';
import TypographyComponent from '../../components/typography';
import {
	fetchSprintTasks,
	setCurrentSprint,
} from '../../store/sprint/asyncAction';
import CommonTaskTable from '../common/taskList/table';
import './index.scss';
// import MilestoneSkeleton from './skeleton';
import { makeStyles } from '@material-ui/core/styles';
import { Divider } from '@material-ui/core';
import { loadingSprint, modifyCurrentSprintTasks } from './../../store/sprint/action';
import { SocketContext } from '../../context/socket';

const useStyles = makeStyles((theme) => ({
	root: {
		display: 'flex',
		flexWrap: 'nowrap',
		justifyContent: 'flex-start',
		[theme.breakpoints.down('md')]: {
			flexWrap: 'wrap',
		},
	},
	card: {
		minWidth: '280px', width: "500px", maxWidth: '600px', display: 'inline-block', margin: '10px',
		[theme.breakpoints.down('md')]: {
			width: "300px", maxWidth: '300px',
		},
	}
}));
const MomentPage = () => {
	const socket: any = useContext(SocketContext);

	const dispatch = useDispatch();
	const { current, loading } = useSelector((state: any) => state.sprint);
	useEffect(() => {
		dispatch(loadingSprint(true));
		dispatch(setCurrentSprint());
		dispatch(fetchSprintTasks());
	}, [dispatch]);

	const classes = useStyles();
	const mainContent = [
		{
			label: 'To Do',
			key: 'to_do',
		},
		{
			label: 'In Progress',
			key: 'in_progress',
		},
		{
			label: 'QA',
			key: 'qa',
		},
		{
			label: 'Done',
			key: 'done',
		},
		// {
		// 	label: 'Deferred',
		// 	key: 'deferred',
		// },
	];


	useEffect(() => {
		socket?.on("updateTasks", (data: any) => {
			dispatch(modifyCurrentSprintTasks({ data, modifiedType: 'tasks' }));
		})
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [socket])

	return (
		<div>
			<TypographyComponent label="Organize your Tasks" variant='h4' style={{ fontWeight: 600 }} />
			<Divider style={{ margin: '20px 0px' }} />
			<div className={classes.root}	>
				{mainContent.map((data: any, i: number) => (
					<div key={i} className={classes.card} >
						<AccordionComponent
							key={i}
							header={<TypographyComponent label={data.label} variant='h4' />}
							className='moment_acc'
						>
							<CommonTaskTable
								data={
									current && current.filter((el: any) => el[data.key] === true)
								}
								selectCheckbox={false}
								loading={loading}
							/>
						</AccordionComponent>
					</div>
				))}
			</div>
		</div>
	);
};

export default MomentPage;


// <div className='flex-start' style={{ width: '100%', gap: '20px' }}>
// {
// 	Array(4).fill(1).map(() => (<MilestoneSkeleton />))
// }
// </div>