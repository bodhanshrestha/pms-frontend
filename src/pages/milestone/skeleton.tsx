import React from 'react';
import Skeleton from '@material-ui/lab/Skeleton';
import { Divider } from '@material-ui/core';
import { Paper } from '@material-ui/core';
import PaperComponent from '../../components/paper';
const CardPaper = () => (
  <Paper elevation={3} style={{ padding: '8px', marginBottom: '10px' }}>
    <Skeleton variant="rect" width={100} height={10} style={{ marginBottom: '10px' }} />
    <Skeleton variant="rect" width={200} height={20} />
  </Paper>

)
const MilestoneSkeleton = () => {
  return (
    <div style={{
      minWidth: '280px', maxWidth: '400px', display: 'inline-block',
    }}>
      <PaperComponent elevation={3} style={{ padding: '15px' }} >
        <Skeleton variant="rect" width={210} height={30} />
        <Divider style={{ margin: '10px 0px 10px 0px' }} />
        <CardPaper />
        <CardPaper />
        <CardPaper />
      </PaperComponent >
    </div >
  )
};

export default MilestoneSkeleton;
