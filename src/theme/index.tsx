import React, { useEffect } from 'react';

import { ThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';
import CssBaseline from '@material-ui/core/CssBaseline';

const darkTheme = createMuiTheme({
	palette: {
		type: 'dark',
		primary: { 500: '#ffffff' },
		secondary: {
			main: '#E33E7F',
		},
	},
	typography: {
		allVariants: {
			color: '#ffffff',
		},
	},
});
const lightTheme = createMuiTheme({
	palette: {
		type: 'light',
		// primary: { 500: '#009688' },
	},
	typography: {
		allVariants: {
			color: '#000000',
		},
	},
});
export const Theme = (props: any) => {
	const dark_mode = useSelector((state: any) => state.user.user.dark_mode);
	const { children } = props;
	useEffect(() => { }, [dark_mode]);
	return (
		<ThemeProvider theme={dark_mode ? darkTheme : lightTheme}>
			<CssBaseline />
			{children}
		</ThemeProvider>
	);
};
