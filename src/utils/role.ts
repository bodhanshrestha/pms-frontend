export const getRole = (team: any) => {
  const username = localStorage.getItem('username')
  const data = team?.filter((el: any) => el.member.username === username)[0]?.role
  return data
}
export const managerAndManagementRole = (team: any) => {
  return (getRole(team) === 'manager' || getRole(team) === 'manager')
}
export const managerRole = (team: any) => {
  return getRole(team) === 'manager'
}