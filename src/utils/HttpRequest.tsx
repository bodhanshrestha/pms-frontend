import axios from 'axios';
const URL = process.env.REACT_APP_SERVER_NAME
axios.interceptors.request.use(
	(request) => {
		const accessToken = localStorage.getItem('token');
		const headers = {
			'Content-Type': 'application/json',
			Authorization: `Bearer ${accessToken}`,
		};
		request.headers = headers;
		return request;
	},
	(error) => {
		return Promise.reject(error);
	}
);

// axios.interceptors.response.use(undefined, function axiosRetryInterceptor(err) {
// 	if (err.response.status === 403) {
// 		store.dispatch(deauthenticate());
// 	}
// 	return Promise.reject(err);
// });

const GET = async (url: string) => {
	return await axios.get(`${URL}${url}`);
};
const POST = async (url: string, data: any) => {
	return await axios.post(`${URL}${url}`, data);
};

const PUT = async (url: string, data: any) => {
	return await axios.put(`${URL}${url}`, data);
};

const DELETE = async (url: string) => {
	return await axios.delete(`${URL}${url}`);
};

const HttpRequest = {
	GET,
	POST,
	PUT,
	DELETE,
};

export default HttpRequest;
