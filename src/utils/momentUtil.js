
import * as moment from 'moment';



export const momentViewFormat = (date, isNull = false) => {
  if (date && !moment(date).isValid())
    return date;
  const formattedDate = date ? moment(date).format() : isNull ? null : moment().format();
  return formattedDate;
}


export const momentIsValid = (date) => {
  return moment(date).isValid() && momentIsSameorAfterMinDate(date);
}

export const momentIsSameorBefore = (date1 = new Date(), date2 = new Date()) => {
  return moment(date1).isSameOrBefore(moment(date2));
}

export const momentIsSameorAfterMinDate = (date) => {
  return moment(date).isSameOrAfter('1753-01-01');
}

export const momentIsBefore = (date1 = new Date(), date2 = new Date()) => {
  return moment(date1).isBefore(moment(date2));
}

export const momentDateOnlyIsSameOrAfter = (date1 = new Date(), date2 = new Date()) => {
  return moment(date1).isSameOrAfter(momentViewIgnoreTimezone(moment(date2)));
}

export const momentDateOnlyIsBefore = (date1 = new Date(), date2 = new Date()) => {
  return moment(date1).isBefore(momentViewIgnoreTimezone(moment(date2)));
}

export const momentDateOnlyIsAfter = (date1 = new Date(), date2 = new Date()) => {
  return moment(date1).isAfter(momentViewIgnoreTimezone(moment(date2)));
}

export const momentUnix = (date) => {
  return moment.unix(date);
}

export const momentViewIgnoreTimezone = (date, isNull = true) => {
  return date ? moment.parseZone(date).format() : isNull ? null : moment.parseZone().format();
}

// sends date in format 1994-01-22
export const getCurrentUTCDate = () => {
  const utcTime = moment().utc();
  return utcTime.format('YYYY-MM-DD');
}

// sends date in format 5:00 AM
export const getCurrentUTCTime = () => {
  const utcTime = moment().utc();
  return utcTime.format('LT');
}

export const getCurrentUTCDateTime = () => {
  const utcTime = moment().utc();
  return utcTime.format('YYYY-MM-DD HH:mm:ss');
}

export const dobIsValid = (date) => {
  if (moment(date).isValid()) {
    const today = new Date();
    const birthDate = new Date(date);
    const age = today.getFullYear() - birthDate.getFullYear();
    if (age < 150 && age > 0) return true;
  }
  return false;
}

export const momentDifference = (a = `${new Date()}`, b = `${new Date()}`, unit = 'days') => {
  return moment(a).diff(moment(b), unit); // if a>b, value will be +ve,else -ve
}

export const localToUtc = (date) => {
  if (!date)
    return date
  return moment.utc(date).format();
}

export const utcTime = (date) => {
  if (!date)
    return date
  return moment.utc(date).format('LT');
}

export const momentIsToday = (date) => {
  return moment(date).isSame(moment(), 'day');
}

export const momentIsThisYear = (date) => {
  return moment(date).isSame(moment(), 'year');
}

export const momentAdd = (a = `${new Date()}`, amount = 0, unit = 'days') => {
  return moment(a).add(amount, unit);
}

export const momentSubtract = (a = `${new Date()}`, amount = 0, unit = 'days') => {
  return moment(a).subtract(amount, unit);
}