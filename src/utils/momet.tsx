import moment from 'moment'
const allFormat = 'YYYY-MM-DD hh:mm a'
const defaultFormat = 'YYYY-MM-DD'

export const dateFormat = (date: string) => {
  return moment(moment.utc(date)).local().format(allFormat)
}
export const dateFormatDefault = () => {
  return moment().local().format(defaultFormat)
}