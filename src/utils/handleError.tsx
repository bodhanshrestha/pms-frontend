export const ErrorHandler = (e: any) => {
	if (e) {
		if (e.response) {
			if (e.response.data) {
				if (e.response.data.message) {
					return e.response.data.message;
				}
			}
			return e.response?.statusText;
		}
		return e.message;
	} else {
		console.log(e);
	}
};
