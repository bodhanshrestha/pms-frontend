export const getFirstWord = (str: string) => {
	let acronym = str
		.split(/\s/)
		.reduce((response, word) => (response += word.slice(0, 1)), '')
		.slice(0, 2)
		.toUpperCase();
	return acronym;
};

export const getDataFromLocalStorage = () => {
	return {
		username: localStorage.getItem('username'),
		token: localStorage.getItem('token'),
		project: localStorage.getItem('project'),
	};
};

export const timeValidation = (time: string) => {
	const validVariableReg = new RegExp(
		'^([0-9]|[0-9][0-9])(h) ([0-9]|[0-5][0-9])(m)$'
	);
	const validVariableRegHrs = new RegExp('^([0-9]|[0-9][0-9])(h)$');
	const validVariableRegMin = new RegExp('^([0-9]|[0-5][0-9])(m)$');
	return (
		validVariableReg.test(time.trim()) ||
		validVariableRegHrs.test(time.trim()) ||
		validVariableRegMin.test(time.trim())
	);
};


export const Capitalize = (str: string) => {
	const arr = str.split(" ");
	for (var i = 0; i < arr.length; i++) {
		arr[i] = arr[i].charAt(0).toUpperCase() + arr[i].slice(1);
	}
	return arr.join(" ");
}


export const stringComparer = (a: string, b: string) => {
	if (a !== undefined && b !== undefined)
		return a.localeCompare(b);
	else
		if (a)
			return -1;
		else if (b)
			return 1;
		else
			return 0;
}
export const compareToSort = (a: string, b: string) => {
	console.log("AA", a);
	console.log("BB", b);
	if (a === b) return 0;
	else if (a === null || a === undefined) return 1;
	else if (b === null || b === undefined) return -1;
	else return a.localeCompare(b);
}
export const compareTwoArraysAsSameOrNot = (x: any, y: any): any => {
	const ok = Object.keys, tx = typeof x, ty = typeof y;
	return x && y && tx === 'object' && tx === ty ? (
		ok(x).length === ok(y).length &&
		ok(x).every(key => compareTwoArraysAsSameOrNot(x[key], y[key]))
	) : (x === y);
}


export const findDataObject = (mainArrayData: any, objectToFind: any) => mainArrayData.find((el: any) => el.value === objectToFind.value)