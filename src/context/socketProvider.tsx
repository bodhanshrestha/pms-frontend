import React, { ReactNode } from 'react';
import { SocketContext, socket } from './socket';
const SocketProvider = ({ children }: { children: ReactNode }) => {
  const socketValue: any = socket
  return <SocketContext.Provider value={socketValue}>
    {children}
  </SocketContext.Provider>
};

export default SocketProvider;
