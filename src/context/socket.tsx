import React from 'react';

import { io } from "socket.io-client";

const server = process.env.REACT_APP_SERVER_NAME || "http://localhost:9090/"

export const socket = io(server);
export const SocketContext = React.createContext(null);