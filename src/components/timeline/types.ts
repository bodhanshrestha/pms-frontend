import { ElementType } from "react"

export type TimeLineProps={
   align?: 'alternate'|'left'| 'right'
      variant?:'h1'|'h2'|'h3'|'h4'|'h5'|'h6'|'subtitle1'|'subtitle2'|'body1'|'body2'|'caption'|'button'|'overline'|'srOnly'|'inherit'
     color?:'initial'|'inherit'|'primary'|'secondary'|'textPrimary'|'textSecondary'|'error'
     
     timelineData:TimeLineDataProp[]         
     component?:ElementType
   
}
export type TimeLineDataProp={
    time:string,
    title:string,
    description:string,

}