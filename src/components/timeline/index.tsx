import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Timeline from '@material-ui/lab/Timeline';
import TimelineItem from '@material-ui/lab/TimelineItem';
import TimelineSeparator from '@material-ui/lab/TimelineSeparator';
import TimelineConnector from '@material-ui/lab/TimelineConnector';
import TimelineContent from '@material-ui/lab/TimelineContent';
import TimelineOppositeContent from '@material-ui/lab/TimelineOppositeContent';
import TimelineDot from '@material-ui/lab/TimelineDot';
import FastfoodIcon from '@material-ui/icons/Fastfood';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { TimeLineProps } from './types';

const useStyles = makeStyles((theme) => ({
	paper: {
		padding: '6px 16px',
	},
	secondaryTail: {
		backgroundColor: theme.palette.secondary.main,
	},
}));

const TimeLineComponent = ({
	align,
	timelineData,
	variant,
	color,
	component,
}: TimeLineProps) => {
	const classes = useStyles();

	return (
		<Timeline align={align}>
			{timelineData.map((item: any, i: number) => (
				<TimelineItem>
					<TimelineOppositeContent>
						<Typography
							variant={variant ? variant : 'body2'}
							color={color ? color : 'primary'}>
							{item.time}
						</Typography>
					</TimelineOppositeContent>
					<TimelineSeparator>
						<TimelineDot>
							<FastfoodIcon />
						</TimelineDot>
						<TimelineConnector />
					</TimelineSeparator>
					<TimelineContent>
						<Paper elevation={3} className={classes.paper}>
							<Typography
								variant={variant ? variant : 'body2'}
								component={component ? component : 'h1'}>
								{item.title}
							</Typography>
							<Typography>{item.description}</Typography>
						</Paper>
					</TimelineContent>
				</TimelineItem>
			))}
		</Timeline>
	);
};
export default TimeLineComponent;
