import React from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';

import HomeIcon from '@material-ui/icons/Home';

import GrainIcon from '@material-ui/icons/Grain';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme: Theme) =>
	createStyles({
		link: {
			display: 'flex',
			color: 'gray',
		},
		icon: {
			marginRight: theme.spacing(0.5),
			width: 20,
			height: 20,
		},
	})
);

const BreadCrumbsComponent = ({ pathname }: any) => {
	const classes = useStyles();

	const mappedData: any = {
		'/project': 'Project',
	};
	return (
		<Breadcrumbs
			aria-label='breadcrumb'
			separator={<NavigateNextIcon fontSize='small' />}>
			<Link to='/project' style={{ color: 'gray' }}>
				<HomeIcon className={classes.icon} />
				Project
			</Link>

			<Typography color='textPrimary' className={classes.link}>
				<GrainIcon className={classes.icon} />
				{mappedData[pathname]}
			</Typography>
		</Breadcrumbs>
	);
};
export default BreadCrumbsComponent;
