import React from 'react';
import Typography from '@material-ui/core/Typography';
import { TypographyProps } from './types';

const TypographyComponent = ({
	variant,
	display,
	color,
	align,
	label,
	className,
	onClick,
	style,
	noWrap,
}: TypographyProps) => {
	return (
		<div>
			<Typography
				variant={variant}
				display={display}
				color={color}
				align={align}
				onClick={onClick}
				style={style}
				noWrap={noWrap}

				className={className}
			>
				{label}
			</Typography>
		</div>
	);
};

export default TypographyComponent;
