export type TypographyProps = {
  label: string | React.ReactNode;

  className?: string;
  variant?:
  | "h1"
  | "h2"
  | "h3"
  | "h4"
  | "h5"
  | "h6"
  | "subtitle1"
  | "subtitle2"
  | "body1"
  | "body2"
  | "caption"
  | "button"
  | "overline"
  | "srOnly"
  | "inherit";
  display?: "initial" | "block" | "inline";
  color?:
  | "initial"
  | "inherit"
  | "primary"
  | "secondary"
  | "textPrimary"
  | "textSecondary"
  | "error";
  align?: "inherit" | "left" | "center" | "right" | "justify";
  children?: React.ReactNode;
  style?: any;
  onClick?: () => any
  noWrap?: boolean
};
