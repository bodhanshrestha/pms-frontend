export type SnackbarProps={
    open:boolean,
    type:string,
    message:string
}