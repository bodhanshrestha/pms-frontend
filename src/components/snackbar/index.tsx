import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';
import { makeStyles } from '@material-ui/core/styles';
import { setSnackbar } from '../../store/snackbar/action';

const useStyles = makeStyles((theme) => ({
	root: {
		width: '100%',
		'& > * + *': {
			marginTop: theme.spacing(2),
		},
	},
}));

const SnackbarComponent = () => {
	const classes = useStyles();
	const dispatch = useDispatch();
	const { open, type, message } = useSelector((state: any) => state.snack);

	const handleClose = (event?: React.SyntheticEvent, reason?: string) => {
		if (reason === 'clickaway') {
			return;
		}

		dispatch(setSnackbar({ open: false, type, message }));
	};
	return (
		<div className={classes.root}>
			<Snackbar open={open} autoHideDuration={3000} onClose={handleClose}>
				<Alert
					elevation={6}
					variant='filled'
					onClose={handleClose}
					color={type}>
					{message}
				</Alert>
			</Snackbar>
		</div>
	);
};

export default SnackbarComponent;
