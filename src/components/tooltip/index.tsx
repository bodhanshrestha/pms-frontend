import React from 'react';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';
import Fade from '@material-ui/core/Fade';
import Zoom from '@material-ui/core/Zoom';
import {ToolTipProps} from './types';

const ToolTipComponent=({label,tipTitle}:ToolTipProps)=> {
  return (
    <Tooltip title={tipTitle} arrow TransitionComponent={Fade} TransitionProps={{ timeout: 300 }}>
      <Button>{label}</Button>
    </Tooltip>
  );
}
export default ToolTipComponent;