export type ModalProps={
    button: {
        type?:"button" | "reset" | "submit" | undefined
        label:string
    }
    modal:{
        title:string
        description:string
    }
}