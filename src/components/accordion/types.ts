import { ReactNode } from "react";

export type AccordionProps = {
  header: ReactNode | string
  children: ReactNode
  close?: boolean
  id?: string
  setIsExpended?: any
  className?: string
  style?: any
}