import React from 'react';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { AccordionProps } from './types';

const AccordionComponent = ({
	header,
	children,
	close,
	id,
	className,
	style
}: AccordionProps) => {
	const [expanded, setExpanded] = React.useState<string | false>(
		close ? false : 'panel'
	);
	const handleChange =
		(panel: string) => (event: React.ChangeEvent<{}>, isExpanded: boolean) => {
			setExpanded(isExpanded ? panel : false);
		};
	return (
		<Accordion
			style={style}
			className={className}
			expanded={expanded === 'panel'}
			onChange={handleChange('panel')}
			id={id}>
			<AccordionSummary
				expandIcon={<ExpandMoreIcon />}
				aria-controls='panel1a-content'
				id='panel1a-header'>
				{header}
			</AccordionSummary>
			<AccordionDetails className='w-100'>{children}</AccordionDetails>
		</Accordion>
	);
};

export default AccordionComponent;
