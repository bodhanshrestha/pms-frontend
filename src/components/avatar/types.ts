export type AvatarProps = {
	alt?: string;
	src?: string;
	className?: string;
	variant?: 'circle' | 'circular' | 'rounded' | 'square';
	children?: string;
	background?: string;
	style?: any;
};
