import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import { AvatarProps } from './types';

const AvatarComponent = ({
	alt,
	src,
	className,
	variant,
	children,
	background,
	style = {}
}: AvatarProps) => {
	return (
		<Avatar
			alt={alt}
			src={src}
			className={className}
			variant={variant}
			style={{ ...style, background: background }}
		>
			{children}
		</Avatar>
	);
};

export default AvatarComponent;
