import React from 'react';
import Backdrop from '@material-ui/core/Backdrop';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import { BackdropProps } from './types';



const useStyles = makeStyles((theme: Theme) =>
createStyles({
  backdrop: {
	zIndex: theme.zIndex.drawer + 1,
	color: '#fff',
  },
}),
);



const BackdropComponent = ({label}: BackdropProps) => {

  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const handleClose = () => {
    setOpen(false);
  };
  const handleToggle = () => {
    setOpen(!open);
  };


	return (
		<div>
		<Button variant="outlined" color="primary" onClick={handleToggle}>
        {label}
      </Button>
      <Backdrop className={classes.backdrop} open={open} onClick={handleClose}>
        <CircularProgress color="inherit" />
      </Backdrop>
	  </div>
	
	)


};

export default BackdropComponent;
