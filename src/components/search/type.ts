export type searchProps = {
  keyword: string,
  setKeyword: (e: any) => void
}