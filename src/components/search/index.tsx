import React from 'react';
import InputField from '../form/text';
import { searchProps } from './type';

const SearchField = ({ keyword, setKeyword }: searchProps) => {
	return (
		<InputField
			label='Search'
			name='search'
			type='text'
			small={true}
			placeholder='Search Task'
			value={keyword}
			width='400px'

			callback={(e) => setKeyword(e.target.value)}
		/>
	);
};

export default SearchField;
