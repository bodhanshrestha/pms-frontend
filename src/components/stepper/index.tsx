import React, { useEffect } from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepButton from '@material-ui/core/StepButton';

const useStyles = makeStyles((theme: Theme) =>
	createStyles({
		root: {
			width: '70%',
			margin: '50px auto',
			[theme.breakpoints.down('sm')]: {
				width: '95%',
			},
			[theme.breakpoints.down('xs')]: {
				width: '100%',
			},
		},
		button: {
			marginRight: theme.spacing(1),
		},
		completed: {
			display: 'inline-block',
		},
		instructions: {
			marginTop: theme.spacing(1),
			marginBottom: theme.spacing(1),
		},
	})
);

function getSteps() {
	return [
		'Project Setup',
		'Initialize Tasks',
		'Project Default Stages',
		'Invite Teammates',
	];
}

export default function InitialStepper({ active, complete }: any) {
	const classes = useStyles();
	const [activeStep, setActiveStep] = React.useState(0);
	const [completed, setCompleted] = React.useState<any>({});
	const steps = getSteps();

	useEffect(() => {
		setActiveStep(active);
		return () => { };
	}, [active]);
	useEffect(() => {
		setCompleted({ ...complete });
		return () => { };
	}, [complete]);
	return (
		<div className={classes.root}>
			<Stepper nonLinear activeStep={activeStep}>
				{steps.map((label, index) => (
					<Step key={label} disabled>
						<StepButton completed={completed[index]}>{label}</StepButton>
					</Step>
				))}
			</Stepper>
		</div>
	);
}
