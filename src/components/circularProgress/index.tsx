import React from 'react'
import CircularProgress from '@material-ui/core/CircularProgress';

const SpinningLoader = () => {
  return (
    <div className='flex-center loader-main'>
      <CircularProgress style={{ width: '150px', height: '150px' }} />
    </div>
  )
}

export default SpinningLoader
