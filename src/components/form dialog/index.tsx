import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { useDispatch } from 'react-redux';

export default function FormDialog({
	dispatchFunction,
	submitLabel,
	content,
	displayBtnLabel,
	dialogTitle,
	variant,
}: any) {
	const [open, setOpen] = useState(false);
	const [email, setEmail] = useState('');
	const [isEmailValid, setIsEmailValid] = useState(true);
	const dispatch = useDispatch();
	const emailValidation = (email: string) => {
		const regex =
			//eslint-disable-next-line
			/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
		return !(!email || regex.test(email) === false);
	};
	const handleClickOpen = () => {
		setOpen(true);
	};

	const handleClose = () => {
		setOpen(false);
		setIsEmailValid(true);
	};
	const handleInvite = () => {
		if (isEmailValid) {
			dispatch(dispatchFunction(email));
			return handleClose();
		}
	};

	return (
		<div>
			<Button variant={variant} color='primary' onClick={handleClickOpen}>
				{displayBtnLabel}
			</Button>
			<Dialog
				open={open}
				onClose={handleClose}
				aria-labelledby='form-dialog-title'
			>
				<DialogTitle id='form-dialog-title'>{dialogTitle}</DialogTitle>
				<DialogContent>
					<DialogContentText>{content}</DialogContentText>
					<TextField
						autoFocus
						margin='dense'
						id='name'
						label='Email Address'
						type='email'
						fullWidth
						onChange={(e: any) => {
							setEmail(e.target.value);
							setIsEmailValid(emailValidation(e.target.value));
						}}
						error={!isEmailValid ? true : false}
						helperText={!isEmailValid ? 'Enter a valid Email Address' : ''}
					/>
				</DialogContent>
				<DialogActions>
					<Button onClick={handleClose} color='primary'>
						Cancel
					</Button>
					<Button onClick={handleInvite} color='primary'>
						{submitLabel}
					</Button>
				</DialogActions>
			</Dialog>
		</div>
	);
}
