import React from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Alert from '@material-ui/lab/Alert';
import IconButton from '@material-ui/core/IconButton';
import Collapse from '@material-ui/core/Collapse';
import CloseIcon from '@material-ui/icons/Close';
import { AlertProps } from './types';

const useStyles = makeStyles((theme: Theme) =>
	createStyles({
		root: {
			width: '100%',
			'& > * + *': {
				marginTop: theme.spacing(2),
			},
		},
	})
);

export default function AlertComponent({
	open,
	setOpen,
	children,
	type,
}: AlertProps) {
	const classes = useStyles();
	const handleClose = () => setOpen(false);
	return (
		<div className={classes.root}>
			<Collapse in={open}>
				<Alert
					severity={type}
					action={
						<IconButton
							aria-label='close'
							color='inherit'
							size='small'
							onClick={handleClose}>
							<CloseIcon fontSize='inherit' onClick={handleClose} />
						</IconButton>
					}>
					{children}
				</Alert>
			</Collapse>
		</div>
	);
}
