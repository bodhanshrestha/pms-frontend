import { ReactNode } from 'react';
export type AlertProps = {
  open: boolean,
  setOpen: (e: boolean) => void,
  children: ReactNode,
  type: 'error' | 'info' | 'success' | 'warning'
}