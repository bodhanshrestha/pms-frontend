
export type BadgeProps = {
    color?: 'default' | 'error' | 'primary' | 'secondary',
    badgeContent?: number,
    max?: number,
    overlap?: 'circle' | 'rectangle',
    variant?: 'dot' | 'standard',
    icon: React.ReactNode
    className?: string
}