import React from 'react';
import Badge from '@material-ui/core/Badge';
import { BadgeProps } from './types';

const BadgeComponent = ({
  color,
  badgeContent,
  max,
  overlap,
  variant,
  icon,
  className,
}: BadgeProps) => {
  return (
    <Badge
      color={color ? color : 'error'}
      badgeContent={badgeContent}
      max={max ? max : 10}
      overlap={overlap}
      className={className}
      variant={variant}
    >
      {icon}
    </Badge>
  );
};
export default BadgeComponent;
