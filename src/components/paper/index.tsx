import Paper from '@material-ui/core/Paper';

const PaperComponent = ({
	variant,
	children,
	square,
	elevation,
	className,
	style
}: any) => {
	return (
		<Paper
			style={style}
			variant={variant ? variant : 'outlined'}
			square={square ? square : false}
			elevation={elevation ? elevation : 3}
			className={className}>
			{children}
		</Paper>
	);
};

export default PaperComponent;
