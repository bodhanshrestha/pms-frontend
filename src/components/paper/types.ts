export type PaperProps = {
  elevation: number;
  children?: React.ReactNode;
  variant?: "elevation" | "outlined";
  square?: Boolean;
  className?: string;
};
