import React from 'react';
import Switch from '@material-ui/core/Switch';
import { SwitchProps } from './types';

const SwitchComponent = ({
  checked,
  color,
  name,
  size,
  callback,
}: SwitchProps) => {
  return (
    <Switch
      checked={checked}
      color={color}
      name={name}
      size={size}
      onChange={callback}
    />
  );
};

export default SwitchComponent;
