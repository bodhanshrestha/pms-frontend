export type SwitchProps = {
    checked?: boolean
    color?: 'primary' | 'secondary' | 'default'
    name?: string
    size?: 'medium' | 'small'
    edge?: 'end' | 'start' | 'false'
    callback?: (e: any) => any
}