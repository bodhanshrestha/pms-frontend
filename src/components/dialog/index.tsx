import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Paper, { PaperProps } from '@material-ui/core/Paper';
import Draggable from 'react-draggable';
import Divider from '@material-ui/core/Divider';
import ButtonComponent from './../button/index';

function PaperComponent(props: PaperProps) {
	return (
		<Draggable
			handle='#draggable-dialog-title'
			cancel={'[class*="MuiDialogContent-root"]'}
		>
			<Paper {...props} />
		</Draggable>
	);
}
const DialogComponent = ({
	btn,
	title,
	children,
	action,
	dialogStyle,
	resetCallback,
	maxWidth,
	onSubmit,
	onSubmitClose,
	open,
	formik,
	setOpen,
	loading
}: any) => {

	const handleClickOpen = () => {
		setOpen(true);
	};

	const handleClose = () => {
		if (resetCallback !== undefined) {

			handleReset();
		}
		formik && formik.resetForm()
		setOpen(false);
	};

	const handleReset = () => {
		if (resetCallback !== undefined) {
			resetCallback();
		}
	};
	const handleSubmit = () => {
		onSubmit();
	};

	return (
		<>
			<Button
				variant={btn.variant ? btn.variant : 'outlined'}
				color='primary'
				type={btn.type ? btn.type : 'button'}
				onClick={handleClickOpen}
				className={btn.className}
				style={{ width: '100%', textAlign: 'left' }}
			>
				{btn.label}
			</Button>

			<Dialog
				open={open}
				fullWidth={true}
				maxWidth={maxWidth}
				onClose={handleClose}
				PaperComponent={PaperComponent}
				aria-labelledby='draggable-dialog-title'
			>
				<DialogTitle style={{ cursor: 'move' }} id='draggable-dialog-title'>
					{title}
				</DialogTitle>
				<Divider />
				<DialogContent>
					{React.cloneElement(children, { handleClose: handleClose })}
				</DialogContent>
				<Divider />
				<DialogActions>
					{action.cancelBtn && (
						<Button autoFocus onClick={handleClose} color='primary'>
							{action.cancelBtn}
						</Button>
					)}
					{action.resetBtn && (
						<Button
							type='reset'
							autoFocus
							onClick={handleReset}
							color='primary'
						>
							{action.resetBtn}
						</Button>
					)}
					{action.submitBtn && (
						<ButtonComponent
							label={action.submitBtn}
							type='submit'
							variant='text'
							color='primary'
							disabled={loading}
							callback={handleSubmit}
						/>

					)}
				</DialogActions>
			</Dialog>
		</>
	);
};

export default DialogComponent;
