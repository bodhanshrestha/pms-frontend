import React from 'react';
import Button from '@material-ui/core/Button';
import { ButtonProps } from './types';
import CircularProgress from '@material-ui/core/CircularProgress';

const ButtonComponent = ({
	variant,
	color,
	size,
	className,
	startIcon,
	endIcon,
	label,
	callback,
	type,
	disabled, style
}: ButtonProps) => {
	return (

		<Button
			type={type ? type : 'button'}
			onClick={callback}
			variant={variant}
			color={color}
			size={size}
			disabled={disabled ? disabled : false}
			className={className}
			startIcon={startIcon}
			style={style}
			endIcon={endIcon}>
			{label}  {disabled && <CircularProgress size={14} style={{ marginLeft: '10px' }} />}
		</Button>

	);
};

export default ButtonComponent;
