import { Color } from "@material-ui/core";
import { ReactNode } from "react";

export type ButtonProps = {
  variant?: "contained" | "outlined" | "text";
  color?: "default" | "inherit" | "primary" | "secondary";
  size?: "small" | "medium" | "large"; // ? = undefined or null
  className?: string;
  startIcon?: ReactNode;
  endIcon?: ReactNode;
  label: string;
  callback?: (e: any) => any;
  type?: "button" | "submit" | "reset";
  disabled?: boolean
  style?: any
};
