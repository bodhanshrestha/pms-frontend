import React from 'react';
import Avatar from '../avatar';
import AvatarGroup from '@material-ui/lab/AvatarGroup';
import { AvatarGroupProps, AvatarUserProps } from './types';
import { getFirstWord } from '../../utils/functions';

const AvatarGroupComponent = ({ max, users }: AvatarGroupProps) => {
	return (
		<AvatarGroup max={max ? max : 4}>
			{users &&
				users.map((user: AvatarUserProps, i: number) => (
					<Avatar alt={user.alt} background={user.color} key={i}>
						{user.name && getFirstWord(user.name)}
					</Avatar>
				))}
		</AvatarGroup>
	);
};

export default AvatarGroupComponent;
