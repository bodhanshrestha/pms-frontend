import { ReactNode } from 'react';
export type AvatarGroupProps = {
    max?: number
    users?: AvatarUserProps[]

}

export type AvatarUserProps = {
    alt?: string
    src?: string
    name?: string
    color?: string
}