import React from 'react';

import InputAdornment from '@material-ui/core/InputAdornment';

import { TextInputFieldProps } from './types';
import TextField from '@material-ui/core/TextField';

const InputField = ({
	value,
	className,
	callback,
	helperText,
	startAdornment,
	endAdornment,
	name,
	placeholder,
	variant,
	label,
	rows,
	error,
	multiline,
	disabled,
	readOnly,
	type,
	width,
	marginTop,
	marginLeft,
	marginRight,
	marginBottom,
	eventCallBack,
	small,
	autoFocus,
	eventOnBlur,
	shrink,
}: TextInputFieldProps) => {
	return (
		<TextField
			autoFocus={autoFocus ? autoFocus : false}
			id={name}
			label={label}
			name={name}
			size={small ? 'small' : 'medium'}
			type={type ? type : 'text'}
			value={value ? value : ''}
			placeholder={placeholder}
			variant={variant ? variant : 'outlined'}
			helperText={helperText}
			error={error ? error : false}
			multiline={multiline ? multiline : false}
			rows={rows}
			disabled={disabled ? disabled : false}
			onChange={callback}
			className={className}
			onKeyDown={eventCallBack}
			onBlur={eventOnBlur}
			style={{
				width: width ? width : '100%',
				margin: `${marginTop ? marginTop : '0px'} ${
					marginRight ? marginRight : '0px'
				} ${marginBottom ? marginBottom : '20px'} ${
					marginLeft ? marginLeft : '0px'
				}
				`,
			}}
			InputLabelProps={{
				shrink: shrink,
			}}
			InputProps={{
				readOnly: readOnly ? readOnly : false,
				startAdornment: startAdornment ? (
					<InputAdornment position='start'>{startAdornment}</InputAdornment>
				) : (
					false
				),
				endAdornment: endAdornment ? (
					<InputAdornment position='end'>{endAdornment}</InputAdornment>
				) : (
					false
				),
			}}
		/>
	);
};

export default InputField;
