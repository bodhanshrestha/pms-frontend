import React from 'react';
import FormControl from '@material-ui/core/FormControl';

import InputAdornment from '@material-ui/core/InputAdornment';
import Input from '@material-ui/core/Input';
import FormHelperText from '@material-ui/core/FormHelperText';
import InputLabel from '@material-ui/core/InputLabel';

import { TextInputFieldProps } from './types';

const TextInputField = ({
	value,
	className,
	callback,
	helperText,
	startAdornment,
	endAdornment,
	name,
	placeholder,
	variant,
	label,
	defaultValue,
	error,
	multiline,
	disabled,
	readOnly,
	type,
	fullWidth,
	width,
}: TextInputFieldProps) => {
	return (
		<FormControl
			className={className}
			style={{ width: `${width ? width : '100%'}` }}
		>
			<InputLabel htmlFor={`standard-adornment-${name}`}>{label}</InputLabel>
			<Input
				id={`standard-adornment-${name}`}
				fullWidth={fullWidth ? fullWidth : true}
				name={name}
				defaultValue={defaultValue}
				type={type ? type : 'text'}
				value={value ? value : ''}
				placeholder={placeholder}
				error={error ? error : false}
				multiline={multiline ? multiline : false}
				disabled={disabled ? disabled : false}
				onChange={callback}
				readOnly={readOnly ? readOnly : false}
				startAdornment={
					startAdornment ? (
						<InputAdornment position='start'>{startAdornment}</InputAdornment>
					) : (
						false
					)
				}
				endAdornment={
					endAdornment ? (
						<InputAdornment position='end'>{endAdornment}</InputAdornment>
					) : (
						false
					)
				}
			/>
			{helperText ? (
				<FormHelperText id='standard-weight-helper-text'>Weight</FormHelperText>
			) : (
				''
			)}
		</FormControl>
	);
};

export default TextInputField;
