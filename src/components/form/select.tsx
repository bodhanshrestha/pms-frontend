import React from 'react';

import MenuItem from '@material-ui/core/MenuItem';

import FormControl from '@material-ui/core/FormControl';
import { TextInputFieldProps } from './types';
import TextField from '@material-ui/core/TextField';
import './index.scss';
const SelectInputField = ({
	value,
	className,
	callback,
	helperText,

	name,
	placeholder,
	variant,
	label,
	defaultValue,
	error,
	small,
	disabled,
	width,
	fullWidth,
	type,
	data,
}: TextInputFieldProps) => {
	return (
		<FormControl
			className={className}
			style={{
				width: `${width ? width : '100%'}`,
			}}
		>
			<TextField
				placeholder={placeholder}
				label={label}
				variant={variant ? variant : 'outlined'}
				select
				value={value ? value : ''}
				defaultValue={defaultValue ? defaultValue : ''}
				type={type}
				name={name}
				size={small ? 'small' : 'medium'}
				onChange={callback}
				helperText={helperText}
				error={error ? error : false}
				disabled={disabled ? disabled : false}
			>
				{data &&
					data.map((option: any, i: number) => {
						return (
							<MenuItem
								key={i}
								value={
									option._id ? option._id : option.id ? option.id : option.value
								}
							>
								{option.name ? option.name : option.label}
							</MenuItem>
						);
					})}
			</TextField>
		</FormControl>
	);
};

export default SelectInputField;
