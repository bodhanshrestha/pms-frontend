import React from 'react';
import { DropzoneArea } from 'material-ui-dropzone';
import './index.scss';
const DropzoneAreaExample = ({ label, callback }: any) => {
	return (
		<DropzoneArea
			acceptedFiles={['image/*', 'application/msword', 'application/pdf']}
			onChange={callback}
			dropzoneText={label}
			dropzoneClass='dropZone-root-element'
		/>
	);
};

export default DropzoneAreaExample;
