
import { ReactElement, ReactNode } from "react";
export type ChipProps = {
    label:  ReactNode
    variant?:'default' | 'outlined'
    size?:'medium' | 'small'
    color?:'default' | 'primary' | 'secondary'
    deleteIcon?: ReactElement
    disabled?:boolean
    clickable?: boolean
    
}