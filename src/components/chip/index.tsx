import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Chip from '@material-ui/core/Chip';


import { ChipProps } from './types'; 

const ChipComponent = ({label, variant, size, color, deleteIcon, disabled,clickable}:ChipProps) => {
	const handleDelete = () => {
		console.info('You clicked the delete icon.');
 	  };

	  
   const handleClick = () => {
     console.info('You clicked the Chip.');
  };

	return (
		<Chip
        label={label}
		variant={variant}
		size={size}
        color= {color}
         onClick={handleClick}
		onDelete={handleDelete}
	    deleteIcon={deleteIcon}
		disabled={disabled}
		clickable={clickable}
      />
	);
};

export default ChipComponent;
