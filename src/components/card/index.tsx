import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { CardProps } from './types';

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
  },
});
const TypographyData: any = ({ label, variant, component, color }: any) => (
  <Typography
    gutterBottom
    variant={variant ? variant : 'body2'}
    component={component ? component : 'p'}
    color={color ? color : 'textPrimary'}
  >
    {label}
  </Typography>
);
const CardComponent = ({
  title,
  description,
  actions,
  image,
  extra,
  typography,
}: CardProps) => {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardActionArea>
        {image ? (
          <CardMedia
            component='img'
            alt={image.alt}
            height={image.height}
            image={image.src}
            title={image.title}
          />
        ) : (
          ''
        )}

        <CardContent>

          <TypographyData
            variant={typography && typography.title && typography.title.variant}
            component={
              typography && typography.title && typography.title.component
            }
            label={title}
          />

          <TypographyData
            variant={
              typography &&
              typography.description &&
              typography.description.variant
            }
            color={
              typography &&
              typography.description &&
              typography.description.color
            }
            component={
              typography &&
              typography.description &&
              typography.description.component
            }
            label={description}
          />

          {extra ? (
            <TypographyData
              variant={
                typography && typography.extra && typography.extra.variant
              }
              color={typography && typography.extra && typography.extra.color}
              component={
                typography && typography.extra && typography.extra.component
              }
              label={extra}
            />
          ) : (
            ''
          )}
        </CardContent>
      </CardActionArea>

      {actions && actions.length > 0 ? (
        <CardActions>
          {actions.map((action: string, i: number) => (
            <Button size='small' color='secondary'>
              {action}
            </Button>
          ))}
        </CardActions>
      ) : (
        ''
      )}
    </Card>
  );
};
export default CardComponent;
