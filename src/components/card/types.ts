import { ElementType } from "react"

export type CardProps = {
    title: string;
    description: string
    actions?: string[],
    extra?: string
    image?: {
        alt: string,
        height?: string,
        src: string,
        title?: string
    }

    typography?: {
        title?: typographyProps,
        description?: typographyProps,
        extra?: typographyProps
    }


}

type typographyProps = {
    variant?: 'h1'
    | 'h2'
    | 'h3'
    | 'h4'
    | 'h5'
    | 'h6'
    | 'subtitle1'
    | 'subtitle2'
    | 'body1'
    | 'body2'
    | 'caption'
    | 'button'
    | 'overline'
    | 'srOnly'
    | 'inherit'
    color?: 'initial'
    | 'inherit'
    | 'primary'
    | 'secondary'
    | 'textPrimary'
    | 'textSecondary'
    | 'error'
    component?: ElementType
}