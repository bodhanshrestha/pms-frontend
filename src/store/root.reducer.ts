import { userReducer } from './user/reducer';
import { combineReducers } from 'redux';
import dashboardReducer from './dashboard/reducer';
import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist';

import authReducer from './auth/reducer';
import snackReducer from './snackbar/reducer';
import projectReducer from './project/reducer';
import { taskReducer } from './task/reducer';
import sprintReducer from './sprint/reducer';
import { ganttTaskReducer } from './gantt-task/reducer';
import socketReducer from './socket/reducer';
import { notificationReducer } from './notification/reducer';

const persistConfig: any = {
	key: 'root',
	storage,
	whitelist: ['auth', 'project', 'user'],
};

const rootReducer = combineReducers({
	socket: socketReducer,
	auth: authReducer,
	project: projectReducer,
	task: taskReducer,
	dashboard: dashboardReducer,
	snack: snackReducer,
	user: userReducer,
	sprint: sprintReducer,
	ganttTasks: ganttTaskReducer,
	notification: notificationReducer
});

export default persistReducer(persistConfig, rootReducer);
