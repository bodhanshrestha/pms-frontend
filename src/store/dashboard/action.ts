import { DASHBOARD, DASHBOARD_ERROR, INITIAL_DASHBOARD_LOADING, DASHBOARD_LOADING, DASHBOARD_DATA_FETCH } from './actionTypes'

export const dashboardAction = () => {
  return {
    type: DASHBOARD
  }
}
export const setInitialDashboardLoading = (data: boolean) => {
  return {
    type: INITIAL_DASHBOARD_LOADING,
    payload: data
  }
}
export const setDashboardLoading = (data: boolean) => {
  return {
    type: DASHBOARD_LOADING,
    payload: data
  }
}
export const setDashboardError = (data: boolean) => {
  return {
    type: DASHBOARD_ERROR,
    payload: data
  }
}
export const fetchDashboardData = (data: any) => {
  return {
    type: DASHBOARD_DATA_FETCH,
    payload: data
  }
}
