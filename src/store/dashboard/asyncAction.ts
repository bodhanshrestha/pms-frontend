import { fetchDashboardData, setDashboardError } from "./action"
import { DashboardDataRequest } from "./request"

export const setDashboardData = () => async (dispatch: any) => {
  try {
    const { data } = await DashboardDataRequest()
    dispatch(fetchDashboardData(data))
  } catch (e: any) {

    if (e.response.status === 404) {
      return dispatch(setDashboardError(e.response.statusText))
    }
    dispatch(setDashboardError(e.response.data))

  }

}