import { LOGOUT } from '../auth/actionTypes';
import {
	DASHBOARD_ERROR,
	INITIAL_DASHBOARD_LOADING,
	DASHBOARD_LOADING,
} from './actionTypes';

const initialState: any = {
	initialLoading: false,
	loading: false,
	error: '',
};
const dashboardReducer = (state = initialState, action: any) => {
	switch (action.type) {
		case INITIAL_DASHBOARD_LOADING:
			return {
				...state,
				initialLoading: action.payload,
			};
		case DASHBOARD_LOADING:
			return {
				...state,
				loading: action.payload,
			};
		case DASHBOARD_ERROR:
			return {
				...state,
				error: action.payload,
			};
		case LOGOUT:
			return {};

		default:
			return state;
	}
};

export default dashboardReducer;
