import axios from 'axios'
export const DashboardDataRequest = async () => {
  return await axios.get(`/api/dashboard/data`)

}