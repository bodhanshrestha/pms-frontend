import HttpRequest from '../../utils/HttpRequest';
const URL = '/api/notification';

export const getNotificationRequest = async (skip: number, limit: number) => {
	return await HttpRequest.GET(`${URL}/list?skip=${skip}&limit=${limit}`);
};

