import { FETCH_NOTIFICATION, FETCH_INIT_NOTIFICATION, LOADING_NOTIFICATION } from './actionTypes';

export const loadingNotification = (data: boolean) => {
	return {
		type: LOADING_NOTIFICATION,
		payload: data,
	};
};

export const fetchNotification = (data: any) => {
	return {
		type: FETCH_NOTIFICATION,
		payload: data,
	};
};

export const fetchInitNotification = (data: any) => {
	return {
		type: FETCH_INIT_NOTIFICATION,
		payload: data,
	};
};
