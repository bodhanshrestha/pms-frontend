import { ErrorHandler } from '../../utils/handleError';
import {
	getNotificationRequest,
} from './request';
import { setSnackbar } from '../snackbar/action';
import { fetchNotification, fetchInitNotification, loadingNotification } from './action';


export const fetchNotificationAsyncAction = (skipValue: number = 0, limitValue: number = 5, showMore: boolean = false) => async (dispatch: any) => {
	try {

		const { data } = await getNotificationRequest(skipValue, limitValue);
		if (showMore) dispatch(fetchNotification(data));
		else dispatch(fetchInitNotification(data));
	} catch (e) {
		const errorMessage = ErrorHandler(e);
		dispatch(setSnackbar({ open: true, type: 'error', message: errorMessage }));
	} finally {
		dispatch(loadingNotification(false))
	}
};
