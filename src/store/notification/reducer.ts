import { LOGOUT } from '../auth/actionTypes';
import { ActionProps } from '../types';
import { FETCH_NOTIFICATION, FETCH_INIT_NOTIFICATION, LOADING_NOTIFICATION } from './actionTypes';

export const notificationReducer = (state: any = [], action: ActionProps) => {
	const joinWithoutDupes = (A: any, B: any) => {
		const a = new Set(A.map((x: any) => x))
		const b = new Set(B.map((x: any) => x))
		return [...A.filter((x: any) => !b.has(x._id)), ...B.filter((x: any) => !a.has(x._id))]
	}


	switch (action.type) {
		case LOADING_NOTIFICATION:
			return {
				...state,
				loading: action.payload
			};
		case FETCH_INIT_NOTIFICATION:
			return {
				...state,
				...action.payload
			};
		case FETCH_NOTIFICATION:
			return {
				...state,
				total: action.payload.total,
				notification: joinWithoutDupes(state.notification, action.payload.notification)
			};

		case LOGOUT:
			return [];

		default:
			return state;
	}
};
