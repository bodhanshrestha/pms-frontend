import { FETCH_TASK_DATA, FETCH_REVIEW, CLEAR_TASK, FETCH_NEW_TASK_DATA, FETCH_SPRINT_TASK_DATA, LOADING_TASK, FETCH_TOTAL_TASK, MODIFY_TASK_DATA } from './actionTypes'



export const clearTasksAction = () => {
  return {
    type: CLEAR_TASK,
  }
}
export const modifyTasks = (data: any) => {
  return {
    type: MODIFY_TASK_DATA,
    payload: data
  }
}
export const fetchTasksAction = (data: any) => {
  return {
    type: FETCH_TASK_DATA,
    payload: data
  }
}
export const fetchTotalTaskCount = (data: number) => {
  return {
    type: FETCH_TOTAL_TASK,
    payload: data
  }
}

export const fetchNewTasksAction = (data: any) => {
  return {
    type: FETCH_NEW_TASK_DATA,
    payload: data
  }
}
export const fetchSprintTasksAction = (data: any) => {
  return {
    type: FETCH_SPRINT_TASK_DATA,
    payload: data
  }
}
export const fetchTaskLoading = (data: boolean) => {
  return {
    type: LOADING_TASK,
    payload: data
  }
}
export const fetchReviewTask = (data: any) => {
  return {
    type: FETCH_REVIEW,
    payload: data
  }
}
