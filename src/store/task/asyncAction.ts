import { fetchSprintTasks } from './../sprint/asyncAction';
import { ErrorHandler } from '../../utils/handleError';
import {
	taskTitleCreateRequest,
	getTasksRequest,
	getTasksByFilteringRequest,
	getTasksByIdRequest,
	UpdateMassRequest,
	DeleteRequest,
	getRestrictedTasksRequest,
	UpdateRequest,
	initialTaskRequest,
	createTaskRequest,
	DeleteGroupRequest,
} from './request';
import { setSnackbar } from '../snackbar/action';
import {
	fetchTasksAction,
	fetchNewTasksAction,
	fetchReviewTask,
	fetchTaskLoading,
	fetchTotalTaskCount
} from './action';

export const createTaskTitle = (task: any, socket: any) => async (dispatch: any) => {
	try {
		const { data } = await taskTitleCreateRequest(task);
		dispatch(fetchTaskLoading(false));
		const project = localStorage.getItem('project')
		const username = localStorage.getItem('username')
		if (socket) socket.emit("taskChange", { type: "create", project, task: data.task._id, sender: username })
		dispatch(
			setSnackbar({ open: true, type: 'success', message: data.message })
		);
		// dispatch(fetchNewTasks());
	} catch (e) {
		const errorMessage = ErrorHandler(e);
		dispatch(setSnackbar({ open: true, type: 'error', message: errorMessage }));
	}
};

export const fetchTasks = (pagination: any) => async (dispatch: any) => {
	try {
		const { data } = await getTasksRequest(pagination);
		dispatch(fetchTasksAction(data.data));
		dispatch(fetchTotalTaskCount(data.totalCount));
		dispatch(fetchTaskLoading(false));
	} catch (e) {
		const errorMessage = ErrorHandler(e);
		dispatch(setSnackbar({ open: true, type: 'error', message: errorMessage }));
	}
};

export const fetchFilteringTasks = (path: string, pagination: any) => async (dispatch: any) => {
	try {
		const { data } = await getTasksByFilteringRequest(path, pagination);
		dispatch(fetchTasksAction(data.data));
		dispatch(fetchTotalTaskCount(data.totalCount));
		dispatch(fetchTaskLoading(false));
	} catch (e) {
		const errorMessage = ErrorHandler(e);
		dispatch(setSnackbar({ open: true, type: 'error', message: errorMessage }));
	}
};
export const fetchNewTasks = () => async (dispatch: any) => {
	try {
		const { data } = await getRestrictedTasksRequest();
		dispatch(fetchTaskLoading(false));
		dispatch(fetchNewTasksAction(data));
	} catch (e) {
		const errorMessage = ErrorHandler(e);
		dispatch(setSnackbar({ open: true, type: 'error', message: errorMessage }));
	}
};

export const initialCreateTaskAsyncAction =
	(task: any, history: any) => async (dispatch: any) => {
		try {
			await initialTaskRequest(task);
			history.replace(`/default-task-stages`);
		} catch (e) {
			const errorMessage = ErrorHandler(e);
			dispatch(
				setSnackbar({ open: true, type: 'error', message: errorMessage })
			);
		}
	};

export const createTaskAsyncAction = (task: any) => async (dispatch: any) => {
	try {
		const { data } = await createTaskRequest(task);
		dispatch(
			setSnackbar({ open: true, type: 'success', message: data.message })
		);
	} catch (e) {
		const errorMessage = ErrorHandler(e);
		dispatch(setSnackbar({ open: true, type: 'error', message: errorMessage }));
	}
};

export const UpdateTaskAsyncAction =
	(id: string, task: any, message: boolean, pathname: string = 'all', socket: any = undefined) =>
		async (dispatch: any) => {
			try {
				const { data } = await UpdateRequest(id, task);
				const project = localStorage.getItem('project')
				const username = localStorage.getItem('username')
				if (socket) socket.emit("taskChange", { type: "update", project, task: id, sender: username })
				dispatch(fetchReviewTask(data.newData));
				message &&
					dispatch(setSnackbar({ open: true, type: 'success', message: data.message }));
				if (pathname === '/milestone') {
					dispatch(fetchSprintTasks());
				}
				dispatch(fetchNewTasks());
			} catch (e) {
				const errorMessage = ErrorHandler(e);
				dispatch(
					setSnackbar({ open: true, type: 'error', message: errorMessage })
				);
			}
		};

export const UpdateMassTaskAsyncAction =
	(taskIds: string[]) => async (dispatch: any) => {
		try {
			const { data } = await UpdateMassRequest(taskIds);
			dispatch(
				setSnackbar({ open: true, type: 'success', message: data.message })
			);

			dispatch(fetchNewTasks());
		} catch (e) {
			const errorMessage = ErrorHandler(e);
			dispatch(
				setSnackbar({ open: true, type: 'error', message: errorMessage })
			);
		}
	};

export const DeleteTaskAsyncAction =
	(id: string, message: boolean, socket: any) => async (dispatch: any) => {
		try {
			dispatch(fetchTaskLoading(true));
			const { data } = await DeleteRequest(id);
			message &&
				dispatch(
					setSnackbar({ open: true, type: 'success', message: data.message })
				);
			const project = localStorage.getItem('project')
			const username = localStorage.getItem('username')
			if (socket) socket.emit("taskChange", { type: "delete", project, task: id, sender: username })
			dispatch(fetchTaskLoading(false));
			dispatch(fetchNewTasks());
		} catch (e) {
			const errorMessage = ErrorHandler(e);
			dispatch(
				setSnackbar({ open: true, type: 'error', message: errorMessage })
			);
		}
	};

export const DeleteGroupTaskAsyncAction =
	(id: string[], message: boolean, socket: any) => async (dispatch: any) => {
		try {
			dispatch(fetchTaskLoading(true));
			const { data } = await DeleteGroupRequest(id);
			message &&
				dispatch(
					setSnackbar({ open: true, type: 'success', message: data.message })
				);
			const project = localStorage.getItem('project')
			const username = localStorage.getItem('username')
			if (socket) {
				id.forEach((taskId) => {
					socket.emit("taskChange", { type: "delete", project, task: taskId, sender: username })
				})
			}
			dispatch(fetchTaskLoading(false));
			dispatch(fetchNewTasks());
		} catch (e) {
			const errorMessage = ErrorHandler(e);
			dispatch(
				setSnackbar({ open: true, type: 'error', message: errorMessage })
			);
		}
	};

export const fetchTasksByID = (id: string) => async (dispatch: any) => {
	try {
		const { data } = await getTasksByIdRequest(id);
		dispatch(fetchReviewTask(data));
	} catch (e) {
		const errorMessage = ErrorHandler(e);
		dispatch(setSnackbar({ open: true, type: 'error', message: errorMessage }));
	}
};
