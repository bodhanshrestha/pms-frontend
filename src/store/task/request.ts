import HttpRequest from '../../utils/HttpRequest';
const URL = '/api/task';
export const taskTitleCreateRequest = async (task: any) => {
	return await HttpRequest.POST(`/api/task/create`, task);
};

export const getTasksRequest = async (pagination: any) => {
	return await HttpRequest.GET(`/api/task/list?page=${pagination.page}&size=${pagination.pageSize}`);
};
export const getTasksByIdRequest = async (id: string) => {
	return await HttpRequest.GET(`/api/task/${id}`);
};
export const getTasksByFilteringRequest = async (path: string, pagination: any) => {
	return await HttpRequest.GET(`/api/task/list?check=${path}&page=${pagination.page}&size=${pagination.pageSize}`);
};
export const getRestrictedTasksRequest = async () => {
	return await HttpRequest.GET(`/api/task/backlog`);
};

export const initialTaskRequest = async (task: any) => {
	const data: any[] = [];
	//It is because key variable is not used so we remove this and make it undefined
	for (const [, value] of Object.entries(task)) {
		if (value) data.push({ title: value });
	}
	if (data.length > 0)
		return await HttpRequest.POST(`/api/task/initial-create`, data);
};
export const createTaskRequest = async (task: any) => {
	return await HttpRequest.POST('/api/task/create', task);
};

export const UpdateRequest = async (id: string, task: any) => {
	return await HttpRequest.PUT(`${URL}/${id}/update`, task);
};
export const UpdateMassRequest = async (taskIds: string[]) => {
	return await HttpRequest.PUT(`${URL}/mass/update`, taskIds);
};
export const DeleteRequest = async (id: string) => {
	return await HttpRequest.DELETE(`${URL}/${id}/delete`);
};
export const DeleteGroupRequest = async (id: string[]) => {
	return await HttpRequest.POST(`${URL}/groupDelete`, id);
};
