import { LOGOUT } from '../auth/actionTypes';
import { ActionProps } from '../types';
import {
	FETCH_TASK_DATA,
	FETCH_NEW_TASK_DATA,
	FETCH_REVIEW,
	CLEAR_TASK,
	LOADING_TASK,
	FETCH_TOTAL_TASK,
	MODIFY_TASK_DATA
} from './actionTypes';
import { ITaskInitialProps } from './types';
import { ModifiedTasksData } from './../../pages/backlog/log/index';
const initialState: ITaskInitialProps = {
	loading: false,
	tasks: [],
	newTasks: [],
	current: [],
	review: {},
};

export const taskReducer = (state = initialState, action: ActionProps) => {
	switch (action.type) {
		case FETCH_TASK_DATA:
			return {
				...state,
				tasks: action.payload,
			};
		case FETCH_NEW_TASK_DATA:
			return {
				...state,
				newTasks: action.payload,
			};
		case FETCH_REVIEW:
			return {
				...state,
				review: action.payload,
			};
		case FETCH_TOTAL_TASK:
			return {
				...state,
				totalCount: action.payload,
			};

		case LOADING_TASK:
			return {
				...state,
				loading: action.payload,
			};
		case CLEAR_TASK:
			return {};
		case MODIFY_TASK_DATA:
			switch (action.payload.modifiedType) {
				case 'newTasks': {
					const prevTasks: any = state.newTasks
					const incommingData = action.payload.data.data
					const type = action.payload.data.type
					switch (type) {
						case "update":
							return {
								...state,
								newTasks: ModifiedTasksData(prevTasks, incommingData)
							};
						case 'create':
							if (!prevTasks.filter((el: any) => el._id === incommingData._id).length) {
								return {
									...state,
									newTasks: [...prevTasks, { ...action.payload.data.data }]
								};
							} else {
								return state
							}
						case 'delete':
							return {
								...state,
								newTasks: prevTasks.filter((el: any) => el._id !== incommingData._id)
							};

						default:
							return state;
					}
				}
				case 'tasks': {
					const prevTasks: any = state.tasks
					const incommingData = action.payload.data.data
					incommingData.assigne_info = incommingData.assigne
					const type = action.payload.data.type
					switch (type) {
						case "update":
							return {
								...state,
								tasks: ModifiedTasksData(prevTasks, incommingData)
							};
						case 'delete':
							return {
								...state,
								tasks: prevTasks.filter((el: any) => el._id !== incommingData._id)
							};

						default:
							return state;
					}
				}
				default:
					return state;
			}
		case LOGOUT:
			return {};

		default:
			return state;
	}
};
