import { LOGOUT } from '../auth/actionTypes';
import { ActionProps } from '../types';
import { FETCH_GANTT_TASK, FETCH_DEPENDENCY_GANTT_TASK } from './actionTypes';
import { ITaskInitialProps } from './types';
const initialState: ITaskInitialProps = {};

export const ganttTaskReducer = (state = initialState, action: ActionProps) => {
	switch (action.type) {
		case FETCH_GANTT_TASK:
			return {
				...state,
				tasks: action.payload,
			};
		case FETCH_DEPENDENCY_GANTT_TASK:
			return {
				...state,
				dependency: action.payload,
			};

		case LOGOUT:
			return {};

		default:
			return state;
	}
};
