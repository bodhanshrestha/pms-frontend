import { ErrorHandler } from '../../utils/handleError';
import {
	taskTitleCreateRequest,
	getTasksRequest,
	UpdateRequest,
	DeleteRequest,
	taskDependencyTitleCreateRequest,
	getDependencyTasksRequest,
	DependencyDeleteRequest
} from './request';
import { setSnackbar } from '../snackbar/action';
import { fetchGanttTasksAction, fetchDependencyGanttTasksAction } from './action';

export const createGranttTask = (task: any) => async (dispatch: any) => {
	try {
		const { data } = await taskTitleCreateRequest(task);
		fetchGanttTasks();
		dispatch(
			setSnackbar({ open: true, type: 'success', message: data.message })
		);
	} catch (e) {
		const errorMessage = ErrorHandler(e);
		dispatch(setSnackbar({ open: true, type: 'error', message: errorMessage }));
	}
};

export const createDependencyGranttTask = (task: any) => async (dispatch: any) => {
	try {
		const { data } = await taskDependencyTitleCreateRequest(task);
		fetchGanttTasks();
		dispatch(
			setSnackbar({ open: true, type: 'success', message: data.message })
		);
	} catch (e) {
		const errorMessage = ErrorHandler(e);
		dispatch(setSnackbar({ open: true, type: 'error', message: errorMessage }));
	}
};

export const fetchGanttTasks = () => async (dispatch: any) => {
	try {
		const { data } = await getTasksRequest();
		localStorage.setItem("epic", JSON.stringify(data))
		dispatch(fetchGanttTasksAction(data));
	} catch (e) {
		const errorMessage = ErrorHandler(e);
		dispatch(setSnackbar({ open: true, type: 'error', message: errorMessage }));
	}
};
export const fetchDependencyGanttTasks = () => async (dispatch: any) => {
	try {
		const { data } = await getDependencyTasksRequest();
		dispatch(fetchDependencyGanttTasksAction(data));
	} catch (e) {
		const errorMessage = ErrorHandler(e);
		dispatch(setSnackbar({ open: true, type: 'error', message: errorMessage }));
	}
};

export const updateGranttTask =
	(id: string, task: any) => async (dispatch: any) => {
		try {
			const { data } = await UpdateRequest(id, task);
			fetchGanttTasks()
			dispatch(
				setSnackbar({ open: true, type: 'success', message: data.message })
			);
		} catch (e) {
			const errorMessage = ErrorHandler(e);
			dispatch(
				setSnackbar({ open: true, type: 'error', message: errorMessage })
			);
		}
	};
export const deleteGranttTask = (id: string) => async (dispatch: any) => {
	try {
		const { data } = await DeleteRequest(id);

		dispatch(
			setSnackbar({ open: true, type: 'success', message: data.message })
		);
	} catch (e) {
		const errorMessage = ErrorHandler(e);
		dispatch(setSnackbar({ open: true, type: 'error', message: errorMessage }));
	}
};
export const deleteDependencyGranttTask = (id: string) => async (dispatch: any) => {
	try {
		const { data } = await DependencyDeleteRequest(id);

		dispatch(
			setSnackbar({ open: true, type: 'success', message: data.message })
		);
	} catch (e) {
		const errorMessage = ErrorHandler(e);
		dispatch(setSnackbar({ open: true, type: 'error', message: errorMessage }));
	}
};
