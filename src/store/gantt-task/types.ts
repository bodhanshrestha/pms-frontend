export type ITaskInitialProps = {
  loading?: boolean
  tasks?: any[]
  newTasks?: any[]
  sprintTasks?: any[]
  current?: any[]
  review?: {}
}
