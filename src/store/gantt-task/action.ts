import { FETCH_GANTT_TASK, FETCH_DEPENDENCY_GANTT_TASK } from './actionTypes';

export const fetchGanttTasksAction = (data: any) => {
	return {
		type: FETCH_GANTT_TASK,
		payload: data,
	};
};
export const fetchDependencyGanttTasksAction = (data: any) => {
	return {
		type: FETCH_DEPENDENCY_GANTT_TASK,
		payload: data,
	};
};
