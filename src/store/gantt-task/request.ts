import HttpRequest from '../../utils/HttpRequest';
const URL = '/api/gantt-task';
export const taskTitleCreateRequest = async (task: any) => {
	return await HttpRequest.POST(`/api/gantt-task/create`, task);
};

export const getTasksRequest = async () => {
	return await HttpRequest.GET(`/api/gantt-task/list`);
};

export const UpdateRequest = async (id: string, task: any) => {
	return await HttpRequest.PUT(`${URL}/${id}/update`, task);
};
export const DeleteRequest = async (id: string) => {
	return await HttpRequest.DELETE(`${URL}/${id}/delete`);
};
export const taskDependencyTitleCreateRequest = async (task: any) => {
	return await HttpRequest.POST(`/api/gantt-task/d/create`, task);
};

export const getDependencyTasksRequest = async () => {
	return await HttpRequest.GET(`/api/gantt-task/d/list`);
};


export const DependencyDeleteRequest = async (id: string) => {
	return await HttpRequest.DELETE(`${URL}/${id}/d/delete`);
};
