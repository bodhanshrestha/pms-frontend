
import { SET_SOCKET } from './actionTypes';

export const setSocket = (data: any) => {
  return {
    type: SET_SOCKET,
    payload: data,
  }
};
