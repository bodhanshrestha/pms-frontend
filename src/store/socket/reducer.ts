import { LOGOUT } from '../auth/actionTypes';
import { SET_SOCKET } from './actionTypes';

const initialState = {

};

const socketReducer = (state = initialState, action: any) => {
	switch (action.type) {
		case SET_SOCKET:
			return {
				...action.payload
			};
		case LOGOUT:
			return {};
		default:
			return state;
	}
};

export default socketReducer;
