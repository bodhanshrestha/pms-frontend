import { ErrorHandler } from '../../utils/handleError';

import {
	registerRequest,
	loginRequest,
	externalLoginRequest,
	inviteTeammate,
	externalInvitedLoginRequest,
	resendLinkRequest, fetchDataOfChoosenProject, forgetPasswordRequest, sendforgetPasswordEmailRequest,
	checkTokenAccessRequest
} from './request';
import { setSnackbar } from '../snackbar/action';
import { loginAction, resendEmailAction } from './action';
import { resetCurrentSprintTask } from './../sprint/action';
import { clearTasksAction } from './../task/action';

export const checkTokenAccess =
	(history: any) => async (dispatch: any) => {
		try {
			const token = localStorage.getItem('token')
			if (token) {
				await checkTokenAccessRequest(token)
			}
			else {
				localStorage.clear()
				dispatch(setSnackbar({ open: true, type: 'error', message: "Token Error" }));
				history.replace({ pathname: '/login' });
			}
		} catch (e: any) {
			const errorMessage = ErrorHandler(e);
			localStorage.clear()
			dispatch(
				setSnackbar({ open: true, type: 'error', message: errorMessage })
			);
			history.replace({ pathname: '/login' });
		}
	};
export const register =
	(userData: any, history: any) => async (dispatch: any) => {
		try {
			const { data } = await registerRequest(userData);
			dispatch(
				setSnackbar({ open: true, type: 'success', message: data.message })
			);
			history.push({
				pathname: '/email-verification',
				state: { username: data.username, email: data.email },
			});
		} catch (e) {
			const errorMessage = ErrorHandler(e);
			dispatch(
				setSnackbar({ open: true, type: 'error', message: errorMessage })
			);
		}
	};

export const login = (userData: any, history: any, setLoding: any) => async (dispatch: any) => {
	try {
		const { data } = await loginRequest(userData);
		dispatch(
			resendEmailAction({
				resendEmail: false,
				username: userData.username,
				email: '',
			})
		);
		if (!data?.user?.projects || data?.user?.projects?.length === 0) {
			dispatch(
				resendEmailAction({
					resendEmail: true,
					username: data.user.username,
					email: data.user.email,
				})
			);
		} else {
			dispatch(loginAction({ accessToken: data.token, user: data.user }));
			dispatch(
				setSnackbar({ open: true, type: 'success', message: data.message })
			);
			if (data.user?.projects.length > 1) {
				history.replace('/select-project');
			} else {

				history.replace('/dashboard');
			}
		}
	} catch (e: any) {
		if (e.response?.data?.message === 'User Not Registered') {
			dispatch(loginAction({}));
			dispatch(
				resendEmailAction({
					resendEmail: true,
					username: userData.username,
					email: '',
				})
			);
		}
		const errorMessage = ErrorHandler(e);
		dispatch(setSnackbar({ open: true, type: 'error', message: errorMessage }));
	} finally {
		setLoding(false)

	}
};

export const initialExternalLogin =
	(hash: string, history: any) => async (dispatch: any) => {
		try {
			const { data } = await externalLoginRequest({ hash });
			localStorage.setItem('hash', hash);
			dispatch(loginAction({ accessToken: data.token, user: data.user }));
			history.replace(`/initial-project`);
		} catch (e) {
			const errorMessage = ErrorHandler(e);
			dispatch(
				setSnackbar({ open: true, type: 'error', message: errorMessage })
			);
		}
	};
export const initialProjectExternalLogin =
	(hash: string | null, history: any) => async (dispatch: any) => {
		try {
			const { data } = await externalLoginRequest({ hash });
			dispatch(loginAction({ accessToken: data.token, user: data.user }));
			history.replace(`/initial-project-task`);
		} catch (e) {
			const errorMessage = ErrorHandler(e);
			dispatch(
				setSnackbar({ open: true, type: 'error', message: errorMessage })
			);
		}
	};

export const initialExternalInvitedLogin =
	(hash: string | null, newUser: Boolean | null, history: any) =>
		async (dispatch: any) => {
			try {
				const { data } = await externalInvitedLoginRequest({ hash });
				dispatch(loginAction({ accessToken: data.token, user: data.user }));
				if (newUser) {
					return history.replace(`/welcome-newUser`);
				} else {
					return history.replace(`/welcome-project`);
				}
			} catch (e) {
				const errorMessage = ErrorHandler(e);
				dispatch(
					setSnackbar({ open: true, type: 'error', message: errorMessage })
				);
			}
		};

export const inviteTeammateWithEmailAsyncAction =
	(email: string) => async (dispatch: any) => {
		try {
			const { data } = await inviteTeammate(email);
			dispatch(
				setSnackbar({ open: true, type: 'success', message: data.message })
			);
		} catch (e) {
			const errorMessage = ErrorHandler(e);
			dispatch(
				setSnackbar({ open: true, type: 'error', message: errorMessage })
			);
		}
	};
export const resendLink =
	(email: string, username: string) => async (dispatch: any) => {
		try {
			const { data } = await resendLinkRequest({ email, username });
			dispatch(
				setSnackbar({ open: true, type: 'success', message: data.message })
			);
		} catch (e) {
			const errorMessage = ErrorHandler(e);
			dispatch(
				setSnackbar({ open: true, type: 'error', message: errorMessage })
			);
		}
	};

export const clearAllState = (dispatch: any) => {
	dispatch(resetCurrentSprintTask())
	dispatch(clearTasksAction())
}

export const chooseProject =
	(id: string, history: any) => async (dispatch: any) => {
		try {
			const { data } = await fetchDataOfChoosenProject(id);
			dispatch(loginAction({ accessToken: data.token, user: data.user }));
			clearAllState(dispatch)
			history.replace('/dashboard')
			return window.location.reload();
		} catch (e) {
			const errorMessage = ErrorHandler(e);
			dispatch(
				setSnackbar({ open: true, type: 'error', message: errorMessage })
			);
		}
	};



export const forgetPassword =
	(email: string, password: string, history: any | undefined, setLoading: any | undefined) => async (dispatch: any) => {
		try {
			const { data } = await forgetPasswordRequest({ email, password });
			dispatch(
				setSnackbar({ open: true, type: 'success', message: data.message })
			);
			history?.replace('/login')
			if (setLoading) setLoading(false)
		} catch (e) {
			const errorMessage = ErrorHandler(e);
			dispatch(
				setSnackbar({ open: true, type: 'error', message: errorMessage })
			);
		}
	};

export const sendForgetPasswordEmail =
	(email: string) => async (dispatch: any) => {
		try {
			const { data } = await sendforgetPasswordEmailRequest({ email });
			dispatch(
				setSnackbar({ open: true, type: 'success', message: data.message })
			);
		} catch (e) {
			const errorMessage = ErrorHandler(e);
			dispatch(
				setSnackbar({ open: true, type: 'error', message: errorMessage })
			);
		}
	};

