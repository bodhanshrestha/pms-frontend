import HttpRequest from "../../utils/HttpRequest"
import { LoginHashProps } from "./types"

export const checkTokenAccessRequest = async (token: string) => {
  return await HttpRequest.GET(`/api/auth/checkToken?token=${token}`)
}
export const registerRequest = async (data: any) => {
  return await HttpRequest.POST(`/api/auth/register`, data)
}
export const loginRequest = async (data: any) => {
  return await HttpRequest.POST(`/api/auth/login`, data)
}
export const externalLoginRequest = async (data: LoginHashProps) => {
  return await HttpRequest.POST(`/api/auth/hashLogin`, data)
}
export const externalInvitedLoginRequest = async (data: LoginHashProps) => {
  return await HttpRequest.POST(`/api/auth/hashInvitedLogin`, data)
}


export const inviteTeammate = async (email: string) => {
  return await HttpRequest.POST(`/api/auth/invite`, { email })
}
export const resendLinkRequest = async (data: { username: string, email: string }) => {
  return await HttpRequest.POST(`/api/auth/resendLink`, data)
}
export const fetchDataOfChoosenProject = async (id: string) => {
  return await HttpRequest.GET(`/api/auth/${id}/choosen`)
}

export const forgetPasswordRequest = async (data: { email: string, password: string }) => {
  return await HttpRequest.POST(`/api/auth/forget/changePassword`, data)
}

export const sendforgetPasswordEmailRequest = async (data: { email: string }) => {
  return await HttpRequest.POST(`/api/auth/forgetPassword`, data)
}