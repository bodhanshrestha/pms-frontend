export type IDashboard = {
  id: number
  title: string
  body: string
}

export type LoginHashProps = {
  hash: string | null
}