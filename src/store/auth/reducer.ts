import { LOGIN, LOGOUT, SEND_EMAIL, CHANGE_ACCESSTOKEN } from './actionTypes';
import storage from 'redux-persist/lib/storage';

const initialState: any = {
	username: '',
	email: '',
	role: '',
	project: '',
	accessToken: '',
};
const authReducer = (state = initialState, action: any) => {
	switch (action.type) {
		case LOGIN:
			if (Object.keys(action.payload).length !== 0) {
				localStorage.setItem('username', action.payload.user.username);
				if (action.payload.user.project)
					localStorage.setItem('project', action.payload.user.project);
				if (action.payload.accessToken) {
					localStorage.setItem('token', action.payload.accessToken);
					return {
						...state,
						username: action.payload.user.username,
						email: action.payload.user.email,

						accessToken: action.payload.accessToken,
						role: action.payload.user.role,
						project: action.payload.user.project
							? action.payload.user.project
							: '',
					};
				}
				return {
					...state,
					username: action.payload.user.username,
					email: action.payload.user.email,

					role: action.payload.user.role,
					project: action.payload.user.project
						? action.payload.user.project
						: '',
				};
			} else {
				return {};
			}

		case SEND_EMAIL:
			return {
				...action.payload,
			};
		case CHANGE_ACCESSTOKEN:
			return {
				...state,
				accessToken: action.payload,
			};
		case LOGOUT:
			storage.removeItem('persist:root');
			localStorage.clear();
			action.history.push('/login');
			return initialState;

		default:
			return state;
	}
};

export default authReducer;
