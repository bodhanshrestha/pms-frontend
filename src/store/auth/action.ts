import { LOGIN, LOGOUT, SEND_EMAIL, CHANGE_ACCESSTOKEN } from './actionTypes'



export const loginAction = (data: any) => {
  return {
    type: LOGIN,
    payload: data
  }
}

export const changeAccessToken = (data: string) => {
  return {
    type: CHANGE_ACCESSTOKEN,
    payload: data
  }
}
export const logoutAction = (history: any) => {
  return {
    type: LOGOUT,
    history
  }
}
export const resendEmailAction = (data: { resendEmail: boolean, username: string, email: string }) => {
  return {
    type: SEND_EMAIL,
    payload: data
  }
}
