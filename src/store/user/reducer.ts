import { LOGOUT } from '../auth/actionTypes';
import { ActionProps } from '../types';
import { FETCH_USER, FETCH_QUERY_USER, USER_LOADING, CHANGE_MODE, LOADING_QUERY_USER } from './actionTypes';
const initialUserState = {
	user: {},
	loading: false
};

export const userReducer = (state = initialUserState, action: ActionProps) => {
	switch (action.type) {
		case FETCH_USER:
			localStorage.setItem('mode', action.payload.dark_mode);
			return {
				...state,
				user: action.payload,
			};
		case FETCH_QUERY_USER:
			return {
				...state,
				queryUser: action.payload,
			};
		case USER_LOADING:
			return {
				...state,
				loading: action.payload,
			};
		case LOADING_QUERY_USER:
			return {
				...state,
				loadQueryUser: action.payload,
			};
		case LOGOUT:
			return {};
		case CHANGE_MODE:
			return {
				...state,
				user: {
					...state.user,
					dark_mode: action.payload
				}
			};
		default:
			return state;
	}
};
