import HttpRequest from "../../utils/HttpRequest";

export const inviteTeamRequest = async (users: any) => {
    const data: any[] = []
    //It is because key variable is not used so we remove this and make it undefined
    for (const [, value] of Object.entries(users)) {
        if (value) data.push({ email: value })
    }
    if (data.length > 0) return await HttpRequest.POST(`/api/user/invite-team`, data)
}

export const FetchUserRequest = async (username: string) => {
    return await HttpRequest.GET(`/api/user/get/${username}`)
}
export const UpdateUserRequest = async (data: any, username: string) => {
    return await HttpRequest.PUT(`/api/user/update/${username}`, data)
}
