import { fetchQueryUser, fetchUser, userLoading, loadingQueryUser } from './action';
import { setSnackbar } from './../snackbar/action';
import { ErrorHandler } from './../../utils/handleError';
import {
	inviteTeamRequest,
	FetchUserRequest,
	UpdateUserRequest,
} from './request';
import { loginAction } from '../auth/action';

export const inviteTeamAsyncAction =
	(users: any, history: any) => async (dispatch: any) => {
		try {
			await inviteTeamRequest(users);
			history.replace('/dashboard');
		} catch (e) {
			const errorMessage = ErrorHandler(e);
			dispatch(
				setSnackbar({ open: true, type: 'error', message: errorMessage })
			);
		}
	};

export const fetchUserAsyncAction =
	(username: string, queryUser: boolean | undefined = false) =>
		async (dispatch: any, getState: any) => {
			try {
				const { data } = await FetchUserRequest(username);
				if (queryUser) {
					const team = getState().project.team;
					const filter = team.filter(
						(el: any) => el.member.username === username
					);
					if (filter.length === 0) {
						throw new Error('Not a team Member');
					}
					dispatch(fetchQueryUser(data));
				} else {
					dispatch(fetchUser(data));
				}
			} catch (e: any) {
				let errorMessage: string;
				if (e.message === 'Not a team Member') {
					errorMessage = e.message;
				} else {
					errorMessage = ErrorHandler(e);
				}
				dispatch(fetchQueryUser({}));
				dispatch(fetchUser({}));
				dispatch(
					setSnackbar({ open: true, type: 'error', message: errorMessage })
				);
			} finally {
				dispatch(loadingQueryUser(false))
			}
		}

export const updateUserAsyncAction =
	(
		userData: any,
		username: string,
		message: boolean,
		type: string = '',
		history: any = {}
	) =>
		async (dispatch: any) => {
			try {
				const { data } = await UpdateUserRequest(userData, username);
				if (type !== 'change_mode') dispatch(fetchUserAsyncAction(data.user.username));
				if (type === 'login') {
					dispatch(loginAction({ user: data.user }));
					history.replace('/dashboard');
				}
				message &&
					dispatch(
						setSnackbar({ open: true, type: 'success', message: data.message })
					);
			} catch (e) {
				const errorMessage = ErrorHandler(e);
				dispatch(
					setSnackbar({ open: true, type: 'error', message: errorMessage })
				);
			} finally {
				dispatch(userLoading(false))
			}
		};
