import { FETCH_USER, FETCH_QUERY_USER, USER_LOADING, CHANGE_MODE, LOADING_QUERY_USER } from './actionTypes';

export const fetchUser = (data: any) => {
	return {
		type: FETCH_USER,
		payload: data,
	};
};
export const fetchQueryUser = (data: any) => {
	return {
		type: FETCH_QUERY_USER,
		payload: data,
	};
};
export const loadingQueryUser = (data: boolean) => {
	return {
		type: LOADING_QUERY_USER,
		payload: data,
	};
};
export const userLoading = (data: boolean) => {
	return {
		type: USER_LOADING,
		payload: data,
	};
};
export const changeMode = (data: boolean) => {
	return {
		type: CHANGE_MODE,
		payload: data,
	};
};
