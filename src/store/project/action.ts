import { EMPTY_PROJECT_STATE, FETCH_PROJECT_DATA, PROJECT_LEADER_FETCH, PROJECT_TEAM_MEMBER, PROJECT_LOADING } from './actionTypes'



export const fetchProjectAction = (data: any) => {
  return {
    type: FETCH_PROJECT_DATA,
    payload: data
  }
}
export const fetchProjectTeamMemberAction = (data: any) => {
  return {
    type: PROJECT_TEAM_MEMBER,
    payload: data
  }
}
export const projectLeaderFetch = (data: any) => {
  return {
    type: PROJECT_LEADER_FETCH,
    payload: data
  }
}
export const projectLoading = (data: boolean) => {
  return {
    type: PROJECT_LOADING,
    payload: data
  }
}

export const emptyProjectState = () => {
  return {
    type: EMPTY_PROJECT_STATE
  }
}
