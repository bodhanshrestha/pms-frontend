import { LOGOUT } from '../auth/actionTypes';
import { ActionProps } from '../types';
import {
	FETCH_PROJECT_DATA,
	PROJECT_LEADER_FETCH,
	EMPTY_PROJECT_STATE,
	PROJECT_TEAM_MEMBER,
	PROJECT_LOADING
} from './actionTypes';

const projectReducer = (state = {}, action: ActionProps) => {
	switch (action.type) {
		case FETCH_PROJECT_DATA:
			return {
				...state,
				name: action.payload?.name,
				_id: action.payload._id,
				shortName: action.payload.shortName,
				image: action.payload.image,
				leader: action.payload.leader
					? action.payload.leader
					: action.payload.creator.username,
				team: action.payload.team ? action.payload.team : [],
			};
		case PROJECT_LEADER_FETCH:
			return {
				...state,
				leader: action.payload,
			};
		case PROJECT_TEAM_MEMBER:
			return {
				...state,
				team: action.payload,
			};

		case EMPTY_PROJECT_STATE:
			return {};
		case LOGOUT:
			return {};
		case PROJECT_LOADING:
			return {
				...state,
				loading: action.payload
			};
		default:
			return state;
	}
};

export default projectReducer;
