import HttpRequest from "../../utils/HttpRequest"

export const projectCreationRequest = async (data: any) => {
  return await HttpRequest.POST(`/api/project/create`, data)
}

export const getProjectRequest = async () => {
  return await HttpRequest.GET(`/api/project/get`)
}
export const getProjectByIDRequest = async (id: string) => {
  return await HttpRequest.GET(`/api/project/${id}`)
}

export const getProjectTeamMemberRequest = async () => {
  return await HttpRequest.GET(`/api/project/get/team`)
}
export const updateProject = async (id: string, data: any) => {
  return await HttpRequest.PUT(`/api/project/${id}/update`, data)
}
export const updateProjectRole = async (id: string, data: any) => {
  return await HttpRequest.PUT(`/api/project/${id}/update-role`, data)
}
