import { fetchProjectAction, fetchProjectTeamMemberAction, projectLoading } from './action';
import { ErrorHandler } from "../../utils/handleError";
import { projectCreationRequest, getProjectRequest, getProjectByIDRequest, updateProjectRole, getProjectTeamMemberRequest, updateProject } from "./request"
import { setSnackbar } from '../snackbar/action';
import { clearAllState, initialProjectExternalLogin } from '../auth/asyncAction';
import { changeAccessToken } from '../auth/action';


export const ProjectCreationAsyncAction = (userData: any, hash: string | null, history: any, type: string) => async (dispatch: any) => {
  try {
    const { data } = await projectCreationRequest({ ...userData, hash, type })
    if (data.path) {
      localStorage.setItem('project', data.project)
      return history.replace(data.path)
    }
    localStorage.setItem('project', data._id)
    if (data.token) {
      localStorage.setItem('token', data.token)
      dispatch(changeAccessToken(data.token))
      delete data.token
    }
    dispatch(fetchProjectAction(data))
    if (type === 'new') {
      clearAllState(dispatch)
      history.replace('/initial-project-task')
    } else {
      dispatch(initialProjectExternalLogin(hash, history))
    }
  } catch (e) {
    const errorMessage = ErrorHandler(e)
    dispatch(setSnackbar({ open: true, type: 'error', message: errorMessage }))
  } finally {
    dispatch(projectLoading(false))
  }
}

export const ProjectAsyncAction = () => async (dispatch: any) => {
  try {
    const { data } = await getProjectRequest()
    return dispatch(fetchProjectAction(data))
  } catch (e) {

    const errorMessage = ErrorHandler(e)
    dispatch(setSnackbar({ open: true, type: 'error', message: errorMessage }))
  }
}

export const ProjectByIdAsyncAction = (id: string) => async (dispatch: any) => {
  try {
    const { data } = await getProjectByIDRequest(id)
    return dispatch(fetchProjectAction(data))
  } catch (e) {

    const errorMessage = ErrorHandler(e)
    dispatch(setSnackbar({ open: true, type: 'error', message: errorMessage }))
  }
}
export const ProjectTeamMemberAsyncAction = () => async (dispatch: any) => {
  try {
    const { data } = await getProjectTeamMemberRequest()
    dispatch(fetchProjectTeamMemberAction(data))

  } catch (e) {

    const errorMessage = ErrorHandler(e)
    dispatch(setSnackbar({ open: true, type: 'error', message: errorMessage }))
  }
}
export const ProjectUpdateAsyncAction = (id: any, project: any) => async (dispatch: any) => {
  try {
    await updateProject(id, project)
    dispatch(ProjectAsyncAction())

  } catch (e) {

    const errorMessage = ErrorHandler(e)
    dispatch(setSnackbar({ open: true, type: 'error', message: errorMessage }))
  }
}
export const ProjectRoleUpdateAsyncAction = (id: any, project: any) => async (dispatch: any) => {
  try {
    await updateProjectRole(id, project)
    dispatch(ProjectAsyncAction())

  } catch (e) {

    const errorMessage = ErrorHandler(e)
    dispatch(setSnackbar({ open: true, type: 'error', message: errorMessage }))
  }
}
