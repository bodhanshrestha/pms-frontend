import {
	FETCH_SPRINT,
	FETCH_CURRENT_SPRINT_TASK,
	RESET_CURRENT_SPRINT_TASK,
	FETCH_SPRINT_LIST,
	FETCH_SPRINT_REPORT,
	FETCH_DETAILS,
	LOADING_SPRINT,
	MODIFY_SPRINT_TASK_DATA,
	MODIFY_CURRENT_SPRINT_TASK_DATA
} from './actionTypes';

export const modifyCurrentSprintTasks = (data: any) => {
	return {
		type: MODIFY_CURRENT_SPRINT_TASK_DATA,
		payload: data
	}
}
export const modifySprintTasks = (data: any) => {
	return {
		type: MODIFY_SPRINT_TASK_DATA,
		payload: data
	}
}
export const fetchSprintData = (data: any) => {
	return {
		type: FETCH_SPRINT,
		payload: data,
	};
};
export const fetchSprintList = (data: any) => {
	return {
		type: FETCH_SPRINT_LIST,
		payload: data,
	};
};
export const fetchSprintReport = (data: any) => {
	return {
		type: FETCH_SPRINT_REPORT,
		payload: data,
	};
};
export const fetchSprintCurrentTask = (data: any) => {
	return {
		type: FETCH_CURRENT_SPRINT_TASK,
		payload: data,
	};
};
export const resetCurrentSprintTask = () => {
	return {
		type: RESET_CURRENT_SPRINT_TASK,
		payload: [],
	};
};
export const fetchSprintDetailsData = (data: any) => {
	return {
		type: FETCH_DETAILS,
		payload: data
	};
};
export const loadingSprint = (data: boolean) => {
	return {
		type: LOADING_SPRINT,
		payload: data
	};
};
