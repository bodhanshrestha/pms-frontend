import { ModifiedTasksData } from '../../pages/backlog/log';
import { LOGOUT } from '../auth/actionTypes';
import {
	FETCH_SPRINT,
	FETCH_CURRENT_SPRINT_TASK,
	FETCH_SPRINT_LIST,
	FETCH_SPRINT_REPORT,
	RESET_CURRENT_SPRINT_TASK,
	FETCH_DETAILS,
	LOADING_SPRINT,
	MODIFY_SPRINT_TASK_DATA,
	MODIFY_CURRENT_SPRINT_TASK_DATA
} from './actionTypes';


const CheckAssigneTask = (original: any, incomming: any) => {
	let data: any = original
	original.forEach((el: any) => {
		if (el.assigne._id === incomming.assigne._id) {
			if (!original.filter((og: any) => og._id === incomming._id).length) {
				data = original.push(incomming)
			}
		} else {
			data = original.filter((li: any) => li._id !== el._id)
		}
	})
	return data
}

const initialState: any = {};
const sprintReducer = (state = initialState, action: any) => {
	switch (action.type) {
		case FETCH_SPRINT:
			return {
				...state,
				...action.payload,
			};
		case FETCH_CURRENT_SPRINT_TASK:
			localStorage.setItem('current', JSON.stringify(action.payload));
			if (action.payload) {
				return {
					...state,
					current: action.payload,
				};
			}
			return {};

		case FETCH_SPRINT_LIST:
			return { ...state, list: action.payload };
		case FETCH_DETAILS:
			return { ...state, details: action.payload };
		case FETCH_SPRINT_REPORT:
			return { ...state, report: action.payload };
		case LOADING_SPRINT:
			return { ...state, loading: action.payload };
		case LOGOUT:
			return {};
		case RESET_CURRENT_SPRINT_TASK:
			return {};
		case MODIFY_SPRINT_TASK_DATA:
			switch (action.payload.modifiedType) {
				case 'tasks':
					{
						const incommingData = action.payload.data.data
						const prevTasks: any = state.task
						const type = action.payload.data.type
						switch (type) {
							case "update":
								return {
									...state,
									task: ModifiedTasksData(prevTasks, incommingData)
								};
							case 'create':
								if (!prevTasks.filter((el: any) => el._id === incommingData._id).length) {
									return {
										...state,
										task: [...prevTasks, { ...action.payload.data.data }]
									};
								} else {
									return state
								}
							case 'delete':
								return {
									...state,
									task: prevTasks.filter((el: any) => el._id !== incommingData._id)
								};
							default:
								return state;
						}
					}
				default:
					return state;
			}
		case MODIFY_CURRENT_SPRINT_TASK_DATA:
			switch (action.payload.modifiedType) {
				case 'tasks':
					{
						const incommingData = action.payload.data.data
						const prevTasks: any = state.current
						const type = action.payload.data.type
						switch (type) {
							case "update":
								return {
									...state,
									task: CheckAssigneTask(prevTasks, incommingData)
								};
							default:
								return state;
						}
					}
				default:
					return state;
			}
		default:
			return state;

	};
}

export default sprintReducer
