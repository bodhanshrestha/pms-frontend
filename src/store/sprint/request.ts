import HttpRequest from '../../utils/HttpRequest';
const URL = '/api/sprint';
export const CurrentSprintRequest = async () => {
	return await HttpRequest.GET(`/api/sprint/current`);
};
export const DetailSprintRequest = async () => {
	return await HttpRequest.GET(`/api/sprint/details`);
};
export const SprintListRequest = async (active: boolean) => {
	return await HttpRequest.GET(`/api/sprint/list?active=${active}`);
};
export const createSprintRequest = async (data: any) => {
	delete data._id
	return await HttpRequest.POST(`/api/sprint/create`, data);
};

export const ReportRequest = async (id: string) => {
	return await HttpRequest.GET(`${URL}/${id}/report`);
};
export const UpdateRequest = async (id: string, task: any) => {
	return await HttpRequest.PUT(`${URL}/${id}/update`, task);
};

export const UpdateSprintTaskRequest = async (id: string, task: any) => {
	return await HttpRequest.PUT(`${URL}/${id}/updateTask`, task);
};

export const getSprintTasksRequest = async () => {
	return await HttpRequest.GET(`/api/task/sprintTask`);
};

export const closeSprintRequest = async (id: string) => {
	return await HttpRequest.DELETE(`${URL}/${id}/close`);
};