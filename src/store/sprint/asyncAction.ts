import { setSnackbar } from './../snackbar/action';
import { ErrorHandler } from './../../utils/handleError';

import {
	fetchSprintCurrentTask,
	fetchSprintData,
	fetchSprintList,
	fetchSprintReport,
	resetCurrentSprintTask,
	fetchSprintDetailsData,
	loadingSprint
} from './action';
import {
	CurrentSprintRequest,
	createSprintRequest,
	UpdateRequest,
	UpdateSprintTaskRequest,
	getSprintTasksRequest,
	SprintListRequest,
	ReportRequest,
	closeSprintRequest,
	DetailSprintRequest,
} from './request';

export const listSprint = (active: boolean) => async (dispatch: any) => {
	try {
		const { data } = await SprintListRequest(active);
		dispatch(fetchSprintList(data));
	} catch (e: any) {
		dispatch(fetchSprintData({}));
	}
};
export const sprintReport = (id: string) => async (dispatch: any) => {
	try {
		const { data } = await ReportRequest(id);
		dispatch(fetchSprintReport(data));
	} catch (e: any) {
		dispatch(fetchSprintData({}));
	}
};
export const setCurrentSprint = () => async (dispatch: any) => {
	try {
		const { data } = await CurrentSprintRequest();
		dispatch(fetchSprintData(data));
	} catch (e: any) {
		dispatch(fetchSprintData(undefined));
	}
};
export const setDetailSprint = () => async (dispatch: any) => {
	try {
		const { data } = await DetailSprintRequest();
		dispatch(fetchSprintDetailsData(data));
	} catch (e: any) {
		const errorMessage = ErrorHandler(e);
		dispatch(setSnackbar({ open: true, type: 'error', message: errorMessage }));
	}
}

export const createSprint = (sprintData: any, setvisibility: any | undefined, socket: any) => async (dispatch: any) => {
	try {
		const { data } = await createSprintRequest(sprintData);
		dispatch(
			setSnackbar({ open: true, type: 'success', message: data.message })
		);


		const project = localStorage.getItem('project')
		const username = localStorage.getItem('username')
		if (socket) {
			socket.emit("sprintChange", { type: "create", project, sender: username })
		}

		dispatch(fetchSprintData(data.sprint));
	} catch (e: any) {
		const errorMessage = ErrorHandler(e);
		dispatch(setSnackbar({ open: true, type: 'error', message: errorMessage }));
	} finally {
		dispatch(loadingSprint(false))
		setvisibility(false)
	}
};

export const UpdateSprint =
	(id: string, sprint: any, message: boolean, setvisibility: any | undefined, socket: any) => async (dispatch: any) => {
		try {
			const { data } = await UpdateRequest(id, sprint);
			const project = localStorage.getItem('project')
			const username = localStorage.getItem('username')
			if (socket) {
				socket.emit("sprintChange", { type: "update", project, sender: username })
			}
			message &&
				dispatch(
					setSnackbar({ open: true, type: 'success', message: data.message })
				);
			dispatch(setCurrentSprint());
		} catch (e) {
			const errorMessage = ErrorHandler(e);
			dispatch(
				setSnackbar({ open: true, type: 'error', message: errorMessage })
			);
		} finally {
			dispatch(loadingSprint(false))
			setvisibility(false)
		}
	};
export const UpdateSprintTask =
	(id: string, sprint: any, socket: any) => async (dispatch: any) => {
		try {
			await UpdateSprintTaskRequest(id, sprint);
			const project = localStorage.getItem('project')
			const username = localStorage.getItem('username')
			if (socket) {
				socket.emit("sprintTaskChange", { type: "create", project, task: sprint.task[0], sender: username })
			}
			dispatch(setCurrentSprint());
		} catch (e) {
			const errorMessage = ErrorHandler(e);
			dispatch(
				setSnackbar({ open: true, type: 'error', message: errorMessage })
			);
		}
	};

export const fetchSprintTasks = () => async (dispatch: any) => {
	try {
		const { data } = await getSprintTasksRequest();

		if (data.length > 0) {
			dispatch(fetchSprintCurrentTask(data));
		} else {
			dispatch(fetchSprintCurrentTask(null));
		}
	} catch (e) {
		const errorMessage = ErrorHandler(e);
		dispatch(setSnackbar({ open: true, type: 'error', message: errorMessage }));
	} finally {
		dispatch(loadingSprint(false))
	}
};


export const closeSprintAsyncAction = (id: string, socket: any) => async (dispatch: any) => {
	try {
		const { data } = await closeSprintRequest(id);
		const project = localStorage.getItem('project')
		const username = localStorage.getItem('username')
		if (socket) {
			socket.emit("sprintChange", { project, sender: username, type: 'close' })
		}
		dispatch(resetCurrentSprintTask())
		dispatch(fetchSprintCurrentTask(null));
		dispatch(
			setSnackbar({ open: true, type: 'success', message: data.message })
		);
	} catch (e) {
		const errorMessage = ErrorHandler(e);
		dispatch(setSnackbar({ open: true, type: 'error', message: errorMessage }));
	}
};
