export type IDashboard = {
  id: number
  title: string
  body: string
}

export type ArticleState = {
  articles: IDashboard[]
}

export type ArticleAction = {
  type: string
  article: IDashboard
}
