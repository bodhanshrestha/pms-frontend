import { LOGOUT } from '../auth/actionTypes';
import { SET_SNACKBAR } from './actionTypes';

const initialState = {
	open: false,
	type: 'success',
	message: '',
};

const snackReducer = (state = initialState, action: any) => {
	switch (action.type) {
		case SET_SNACKBAR:
			const { open, type, message } = action.payload;
			return {
				...state,
				open,
				type,
				message,
			};
		case LOGOUT:
			return {};
		default:
			return state;
	}
};

export default snackReducer;
