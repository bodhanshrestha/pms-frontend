import { SnackbarProps } from './../../components/snackbar/types';
import { SET_SNACKBAR } from './actionTypes';

export const setSnackbar = (data: SnackbarProps) => {
  return {
    type: SET_SNACKBAR,
    payload: data,
  }
};
