import React, { Fragment, useEffect } from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { MenuData } from './data';
import { Link, useLocation } from 'react-router-dom';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import Collapse from '@material-ui/core/Collapse';
import Divider from '@material-ui/core/Divider';
import './index.scss';
import { useSelector } from 'react-redux';

const Sidebar = () => {
	const [report, setReport] = React.useState(false);
	const [issue, setIssue] = React.useState(false);
	const [plan, setPlan] = React.useState(false);
	const handleReportClick = () => {
		setReport(!report);
	};
	const { pathname } = useLocation()
	const dark_mode = useSelector((state: any) => state.user.user.dark_mode);
	const handleIssueClick = () => {
		setIssue(!issue);
	};
	const handlePlanClick = () => {
		setPlan(!plan);
	};
	useEffect(() => {
		if (pathname.includes('/report')) {
			setReport(true)
		}
		if (pathname.includes('/issue')) {
			setIssue(true)
		}
		if (pathname.includes('/plan')) {
			setPlan(true)
		}
	}, [pathname])
	return (
		<List className={`main-sidebar-menu`}>
			{MenuData.map((menu: any, index: number) => (
				<Fragment key={index}>
					{menu.divider ? (
						<Divider />
					) : (
						<>
							{menu.children ? (
								<>
									<ListItem
										className='listing'
										button
										onClick={
											menu.callback === 'report'
												? handleReportClick
												: menu.callback === 'issue'
													? handleIssueClick
													: handlePlanClick
										}>
										<ListItemIcon
											style={{ color: !dark_mode ? '#3f51b5' : '#ffffff' }}>
											{menu.icon}
										</ListItemIcon>
										<ListItemText primary={menu.label} />
										{menu.callback === 'report' ? (
											report ? (
												<ExpandLess />
											) : (
												<ExpandMore />
											)
										) : menu.callback === 'issue' ? (
											issue ? (
												<ExpandLess />
											) : (
												<ExpandMore />
											)
										) : plan ? (
											<ExpandLess />
										) : (
											<ExpandMore />
										)}
									</ListItem>
									<Collapse
										in={
											menu.callback === 'report'
												? report
												: menu.callback === 'issue'
													? issue
													: plan
										}
										timeout='auto'
										className='menu-collapse-list'
										unmountOnExit>
										<List component='div' disablePadding>
											<ListItem>
												<List
													style={{ width: '100%', padding: '0', margin: '0' }}>
													{menu.children.map((child: any, i: number) => (
														<Link to={child.route} key={i}>
															<ListItem
																button
																key={index}
																className={`${pathname === child.route &&
																	`${!dark_mode
																		? 'menu-active-light'
																		: 'menu-active'
																	}`
																	} list-child`}>
																<ListItemIcon
																	style={{
																		color: !dark_mode ? '#3f51b5' : '#ffffff',
																	}}>
																	{child.icon}
																</ListItemIcon>
																<ListItemText primary={child.label} />
															</ListItem>
														</Link>
													))}
												</List>
											</ListItem>
										</List>
									</Collapse>
								</>
							) : (
								<Link to={menu.route}>
									<ListItem
										button
										key={index}
										className={`${pathname.includes(menu.route) &&
											`${!dark_mode ? 'menu-active-light' : 'menu-active'}`
											} `}>
										<ListItemIcon
											style={{ color: !dark_mode ? '#3f51b5' : '#ffffff' }}>
											{menu.icon}
										</ListItemIcon>
										<ListItemText primary={menu.label} />
									</ListItem>
								</Link>
							)}
						</>
					)}
				</Fragment>
			))}
		</List>
	);
};

export default Sidebar;
