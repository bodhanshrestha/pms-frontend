import DashboardIcon from '@material-ui/icons/Dashboard';
import ListAltIcon from '@material-ui/icons/ListAlt';
import PeopleAltIcon from '@material-ui/icons/PeopleAlt';
import DescriptionIcon from '@material-ui/icons/Description';
import SettingsIcon from '@material-ui/icons/Settings';
import ClearAllIcon from '@material-ui/icons/ClearAll';
import BorderAllIcon from '@material-ui/icons/BorderAll';
import BorderInnerIcon from '@material-ui/icons/BorderInner';
import BorderClearIcon from '@material-ui/icons/BorderClear';
import BorderOuterIcon from '@material-ui/icons/BorderOuter';
import BorderStyleIcon from '@material-ui/icons/BorderStyle';
import VerticalSplitIcon from '@material-ui/icons/VerticalSplit';
import PictureInPictureIcon from '@material-ui/icons/PictureInPicture';
import PieChartIcon from '@material-ui/icons/PieChart';
import RotateLeftIcon from '@material-ui/icons/RotateLeft';
import LowPriorityIcon from '@material-ui/icons/LowPriority';
import PlaylistAddIcon from '@material-ui/icons/PlaylistAdd';
import LocalLibraryIcon from '@material-ui/icons/LocalLibrary';
import TimelineIcon from '@material-ui/icons/Timeline';
export const planMenu = [
	{
		label: 'Gantt Chart',
		route: '/plan/gantt-chart',
		icon: <VerticalSplitIcon style={{ transform: 'rotate(180deg)' }} />,
	},
	{
		label: 'Analyze',
		route: '/plan/analyze',
		icon: <TimelineIcon />,
	},
	{
		label: 'Diagram',
		route: '/plan/diagram',
		icon: <PictureInPictureIcon />,
	},
];

export const reportMenu = [
	{
		label: 'Chart Report',
		route: '/report/chart',
		icon: <PieChartIcon />,
	},

	{
		label: 'Sprint Report',
		route: '/report/sprint',
		icon: <RotateLeftIcon />,
	},
	// {
	// 	label: 'User WorkLoad Report',
	// 	route: '/reports',
	// },
];

export const issueMenu = [
	{
		label: 'All Issues',
		route: '/issues',
		icon: <BorderAllIcon />,
	},
	{
		label: 'My Open Issue',
		route: '/issues?check=my',
		icon: <BorderInnerIcon />,
	},
	{
		label: 'Reported By Me',
		route: '/issues?check=reported',
		icon: <BorderStyleIcon />,
	},
	{
		label: 'Open Issue',
		route: '/issues?check=open',
		icon: <BorderClearIcon />,
	},
	{
		label: 'Done Issue',
		route: '/issues?check=done',
		icon: <BorderOuterIcon />,
	},
];

export const MenuData = [
	{
		label: 'Dashboard',
		route: '/dashboard',
		icon: <DashboardIcon />,
	},
	{
		label: 'Plan',
		route: '/plan',
		icon: <LocalLibraryIcon />,
		children: planMenu,
		callback: 'plan',
	},
	{
		label: 'Backlog',
		route: '/backlog',
		icon: <ListAltIcon />,
	},
	{
		label: 'Active Sprint',
		route: '/sprint',
		icon: <PlaylistAddIcon />,
	},
	{
		label: 'Milestone',
		route: '/milestone',
		icon: <LowPriorityIcon />,
	},
	{
		label: 'Team',
		route: '/people',
		icon: <PeopleAltIcon />,
	},

	{
		label: 'Report',
		route: '/report',
		icon: <DescriptionIcon />,
		children: reportMenu,
		callback: 'report',

	},
	{
		divider: true,
	},
	{
		label: 'Issues',
		route: '/issues',
		icon: <ClearAllIcon />,

	},

	{
		label: 'Project Setting',
		route: '/setting',
		icon: <SettingsIcon />,
	},
];
