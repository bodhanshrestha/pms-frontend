import React from 'react';
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/Inbox';

import './style.scss';
import ButtonComponent from '../../../components/button';
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import RecentActorsIcon from '@material-ui/icons/RecentActors';
import { Link, useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { logoutAction } from '../../../store/auth/action';
import { fetchUser } from '../../../store/user/action';
import { emptyProjectState } from '../../../store/project/action';
import AvatarComponent from '../../../components/avatar';

const RightSideMenu = ({ setOpen }: { setOpen: any }) => {
	let history = useHistory();
	const { user } = useSelector((state: any) => state.user);
	const team = useSelector((state: any) => state.project.team);
	const dispatch = useDispatch();
	const handleOnClick = () => {
		dispatch(emptyProjectState());
		dispatch(logoutAction(history));
		dispatch(fetchUser({}));
	};
	const handleClose = () => {
		setOpen(false);
	};
	const userRole = (username: string, email: string) => {
		const filter = team.filter(
			(el: any) => el.member.username === username && el.member.email === email
		);
		return filter.length > 0 && filter[0].role;
	};
	return (
		<div className='login'>
			<div className='right-menu-logo'>
				<AvatarComponent
					src={`${user.avatar ? user.avatar : ''}`}
					className='avatar-menu'
					variant='square'
				/>
			</div>

			<div className='user-info'>
				<p>{user && user.username} </p>
				<p>{user && user.email}</p>
				<ButtonComponent
					label={`${userRole(user.username, user.email)}`}
					color='primary'
					variant='contained'
				/>
			</div>
			<Divider />
			<List>
				<Link to='/profile' onClick={handleClose}>
					<ListItem button>
						<ListItemIcon>
							<RecentActorsIcon />
						</ListItemIcon>
						<ListItemText primary='Profile' />
					</ListItem>
				</Link>
				<Link to='/projects' onClick={handleClose}>
					<ListItem button>
						<ListItemIcon>
							<InboxIcon />
						</ListItemIcon>
						<ListItemText primary='Projects' />
					</ListItem>
				</Link>

				<Link to='/help' onClick={handleClose}>
					<ListItem button>
						<ListItemIcon>
							<HelpOutlineIcon />
						</ListItemIcon>
						<ListItemText primary='Help' />
					</ListItem>
				</Link>
				<Divider />

				<ListItem button onClick={handleOnClick}>
					<ListItemIcon>
						<ExitToAppIcon />
					</ListItemIcon>
					<ListItemText primary='Logout' />
				</ListItem>
				<Divider />
			</List>
		</div>
	);
};

export default RightSideMenu;
