import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { updateUserAsyncAction } from '../../../store/user/asyncAction';
import { Tooltip } from '@material-ui/core';
import './index.css'
import { changeMode, userLoading } from './../../../store/user/action';
import Brightness7Icon from '@material-ui/icons/Brightness7';
import Brightness4Icon from '@material-ui/icons/Brightness4';
const ColorMode = () => {
	const dispatch = useDispatch();
	const { dark_mode, username } = useSelector((state: any) => state.user.user);
	const handleMode = () => {
		const mode = !dark_mode
		localStorage.setItem("mode", mode.toString())
		dispatch(userLoading(true))
		dispatch(changeMode(mode))
		dispatch(updateUserAsyncAction({ dark_mode: mode }, username, false, 'change_mode'));
	};
	return (

		<Tooltip title={`${dark_mode ? 'Dark Mode' : 'Light Mode'}`}>
			{/* <div className='main-toogle'>
				<label>
					<input className='toggle-checkbox' type='checkbox' disabled={loading} checked={user.dark_mode ? true : false} onChange={handleMode}></input>
					<div className='toggle-slot'>
						<div className='sun-icon-wrapper'>
							<div className="iconify sun-icon" data-icon="feather-sun" data-inline="false"></div>
						</div>
						<div className='toggle-button'></div>
						<div className='moon-icon-wrapper'>
							<div className="iconify moon-icon" data-icon="feather-moon" data-inline="false"></div>
						</div>
					</div>
				</label>
			</div> */}
			<div className='mode_change pointer'>
				{
					dark_mode ? (
						<Brightness4Icon onClick={handleMode} style={{ color: 'white' }} />
					) : (
						<Brightness7Icon onClick={handleMode} />
					)
				}
			</div>
		</Tooltip>
	);
};

export default ColorMode;
