import React from 'react';
import Skeleton from '@material-ui/lab/Skeleton';
import { Divider } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
const Template = () => (
  <div style={{ display: 'flex', justifyContent: 'flex-start', gap: '10px' }}>
    <div>
      <Skeleton variant="rect" width={50} height={50} style={{ marginBottom: '5px' }} />
    </div>
    <div>
      <Skeleton variant="rect" width={150} height={15} style={{ marginBottom: '5px' }} />
      <Skeleton variant="rect" width={120} height={12} style={{ marginBottom: '5px' }} />
      <div style={{ display: 'flex', justifyContent: 'flex-start', gap: '20px' }}>

        <Skeleton variant="rect" width={50} height={10} />
        <Skeleton variant="rect" width={50} height={10} />
      </div>
    </div>
  </div>
)
const NotificationSkeleton = () => {
  const history = useHistory()
  return <div style={{ padding: '10px' }}>
    {
      history.location.pathname === "/notification" ? (
        <>
          {Array(5).fill(1).map(() => (
            <>
              <Template />
              <Divider style={{ marginBottom: '5px' }} />
            </>
          ))}
        </>
      ) : (
        <>
          {Array(3).fill(1).map(() => (
            <>
              <Template />
              <Divider style={{ marginBottom: '5px' }} />
            </>
          ))}
        </>
      )
    }

  </div>
};

export default NotificationSkeleton;
