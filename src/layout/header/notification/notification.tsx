import React, { Fragment, useEffect, useState } from 'react';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import MenuList from '@material-ui/core/MenuList';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import { Socket } from "socket.io-client";
import NotificationsNoneIcon from '@material-ui/icons/NotificationsNone';
import Badge from '@material-ui/core/Badge';
import { useDispatch, useSelector } from 'react-redux';
import { fetchNotificationAsyncAction } from './../../../store/notification/asyncAction';
import NotificationCard from './card';
import TypographyComponent from './../../../components/typography/index';
import Divider from '@material-ui/core/Divider';
import NotificationsIcon from '@material-ui/icons/Notifications';

import { Link } from 'react-router-dom';
import { loadingNotification } from './../../../store/notification/action';
import NotificationSkeleton from './skeleton';
const useStyles = makeStyles((theme: Theme) =>
	createStyles({
		root: {
			display: 'flex',
		},
		paper: {
			marginRight: theme.spacing(2),
		},
		showmore: {
			textAlign: 'center',
			padding: '10px',
			"&:hover": {
				background: '#eeeeee'
			}
		},
		card: {
			minHeight: '90px'
		},
		center: {
			textAlign: 'center'
		}
	})
);

export default function NotificationMenu({ socket }: { socket: Socket }) {
	const classes = useStyles();
	const [open, setOpen] = React.useState(false);
	const anchorRef = React.useRef<HTMLButtonElement>(null);
	const dark_mode = useSelector((state: any) => state.user.user.dark_mode);
	const handleToggle = () => {

		setOpen((prevOpen) => {

			return !prevOpen
		});
	};

	const handleClose = (event: React.MouseEvent<EventTarget>) => {
		if (
			anchorRef.current &&
			anchorRef.current.contains(event.target as HTMLElement)
		) {
			return;
		}

		setOpen(false);
	};

	function handleListKeyDown(event: React.KeyboardEvent) {
		if (event.key === 'Tab') {
			event.preventDefault();
			setOpen(false);
		}
	}

	// return focus to the button when we transitioned from !open -> open
	const prevOpen = React.useRef(open);
	React.useEffect(() => {
		if (prevOpen.current === true && open === false) {
			anchorRef.current!.focus();
		}

		prevOpen.current = open;
	}, [open]);
	const dispatch = useDispatch()
	const [notification, setNotification] = useState<any>([])
	const { loading, notification: notificationData } = useSelector((state: any) => state.notification)
	useEffect(() => {
		socket?.on("getNotification", data => {
			setNotification((prev: any) => [...prev, data])
		})
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [socket])
	const onOpenNotification = () => {
		if (open) {
			setNotification([])
			dispatch(loadingNotification(true))
			dispatch(fetchNotificationAsyncAction())
		}
	}
	useEffect(() => {
		if (open) {
			onOpenNotification()
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [open])
	return (
		<div className={classes.root}>
			<div className='notification-root'>
				<button
					ref={anchorRef}
					className='notification-toggle-btn'
					aria-controls={open ? 'menu-list-grow' : undefined}
					aria-haspopup='true'>
					<Badge
						onClick={handleToggle}
						badgeContent={notification.length}
						color='error'
						className='header-right-badge'>
						{
							open ? (
								<NotificationsNoneIcon style={{ fontSize: '28px', color: `${dark_mode ? 'white' : 'black'}` }} />
							) : (

								<NotificationsIcon style={{ fontSize: '28px', color: `${dark_mode ? 'white' : 'black'}` }} />
							)
						}
					</Badge>
				</button>
				<Popper
					open={open}
					anchorEl={anchorRef.current}
					role={undefined}
					transition
					disablePortal>
					{({ TransitionProps, placement }) => (
						<Grow
							{...TransitionProps}
							style={{
								transformOrigin:
									placement === 'bottom' ? 'center top' : 'center bottom',
							}}>
							<Paper>
								<ClickAwayListener onClickAway={handleClose}>
									<MenuList
										autoFocusItem={open}
										style={{ width: '400px', marginTop: '15px', paddingBottom: '0' }}
										id='menu-list-grow'
										onKeyDown={handleListKeyDown}>

										<div className={classes.card}>
											<TypographyComponent label='Notification' variant='body1' className={classes.center} />
											<Divider />
											{
												loading ? (<NotificationSkeleton />) : (
													<>
														{
															(notificationData && notificationData.length > 0) ? (
																<>
																	{
																		notificationData.map((el: any, i: number) => {
																			if (i < 5) {
																				return (
																					<Fragment key={i}>
																						<NotificationCard data={el} handleClose={handleClose} />

																					</Fragment>
																				)
																			}
																			return false
																		})
																	}
																	{
																		notificationData.length >= 5 && (
																			<Link to='/notification' onClick={handleClose}>
																				<TypographyComponent
																					label="show More"
																					className={classes.showmore}
																				/>
																			</Link>
																		)
																	}
																</>
															) : (
																<TypographyComponent label='Empty Notification' variant='h5' style={{ marginTop: '40px', padding: '20px' }} color='textSecondary' />
															)
														}

													</>
												)
											}
										</div>
									</MenuList>
								</ClickAwayListener>
							</Paper>
						</Grow>
					)}
				</Popper>
			</div>
		</div>
	);
}
