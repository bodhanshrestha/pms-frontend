import React from 'react';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import { makeStyles } from '@material-ui/core/styles';
import TypographyComponent from '../../../components/typography';
import { Link, useLocation } from 'react-router-dom';
import AvatarComponent from '../../../components/avatar';
import { dateFormat } from './../../../utils/momet';
const useStyles = makeStyles((theme: any) => ({
  root: {

    textAlign: 'left',
    padding: '4px 5px',
    "&:hover": {
      background: '#eeeeee'
    }
  },
  width: {
    maxWidth: '400px',
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  header: {
    padding: '0px 8px',
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: 'red',
  },
}));
const NotificationCard = ({ data, handleClose }: any) => {
  const classes = useStyles();
  const location = useLocation()
  const CardSection = () => (
    <CardHeader
      className={classes.header}
      avatar={
        <AvatarComponent
          alt={`avatar-${data?.assignedBy?.username}`}
          src={data?.assignedBy?.avatar}
        />
      }
      title={
        <>
          <TypographyComponent
            label={`${data?.assignedBy?.name} has ${data?.type} you a task`}
            style={{
              fontSize: '0.7rem',
              fontWeight: 600,
              textTransform: 'uppercase',
            }}
          />
          {
            location.pathname === '/notification' ? (
              <TypographyComponent label={data?.task?.title} />
            ) : (
              <TypographyComponent
                label={`${data?.task?.title.substring(0, 70)} ${data?.task?.title?.length > 70 ? '...' : ''}`}
              />
            )
          }
        </>
      }
      subheader={
        <div className='flex-start gap-20'>

          <TypographyComponent
            label={data?.task?.key}
            variant='caption'
            color='textSecondary'
          />
          <TypographyComponent
            label={dateFormat(data?.createdAt)}
            variant='caption'
            color='textSecondary'
          />


        </div>
      }
    />
  )

  return (
    <Card className={`${classes.root} ${location.pathname !== '/notification' && classes.width}`}>


      <Link to={`/task/${data?.task?._id}`} onClick={handleClose}>
        <CardSection />
      </Link>


    </Card>
  );
};

export default NotificationCard;
