import React from 'react';
import './index.scss';
import ProjectMenu from './projectMenu';
import RightMenu from './userMenu';
import TaskFormIndex from './task';
import NotificationMenu from './notification/notification';
import { Link } from 'react-router-dom';
import PaperComponent from '../../components/paper';
import ColorMode from './togglemode/colorMode';
import { useSelector } from 'react-redux';
import AvatarComponent from './../../components/avatar/index';
import { Socket } from 'socket.io-client';
import logo from '../../images/logo.png';
import TypographyComponent from '../../components/typography';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
const useStyles = makeStyles((theme: Theme) =>
	createStyles({
		root: {
			[theme.breakpoints.down('sm')]: {
				display: 'none'
			},
		},
		headerNarrow: {
			[theme.breakpoints.down('xs')]: {
				maxWidth: "70px",
				minWidth: "56px"
			},
		},
		label: {
			[theme.breakpoints.down('xs')]: {
				display: "none"
			},
		}
	})
);

const Header = ({ open, socket }: { open: boolean; socket: Socket }) => {
	const dark_mode = useSelector((state: any) => state.user.user.dark_mode);
	const classes = useStyles();
	return (
		<div className='main-top-header'>
			<PaperComponent
				className={`header-paper h-100 ${!dark_mode && 'header-light-mode'}`}>
				<div
					className={`flexed ${!dark_mode ? 'header-light-mode' : ''}`}>
					<div className={`${classes.headerNarrow} logo ${open ? 'logo-wider' : 'logo-narrow'}`}>
						<Link to='/dashboard'>
							<div className='flex-center'>
								<AvatarComponent
									src={logo}
									variant='square'
									className='logo-pic'
								/>
								{open && (
									<div className={classes.label}>
										<TypographyComponent label='TeamZone' />
									</div>
								)}
							</div>
						</Link>
					</div>
					<div className='flexed'>
						<div className='left-header'>
							<ProjectMenu />
							<div className={classes.root}>
								<TaskFormIndex />
							</div>
						</div>
						<div className='flexed'>
							<div className='right-header'>
								<ColorMode />
								<NotificationMenu socket={socket} />
								<RightMenu />
							</div>
						</div>
					</div>
				</div>
			</PaperComponent>
		</div>
	);
};

export default Header;
