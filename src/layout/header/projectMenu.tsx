import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Menu, { MenuProps } from '@material-ui/core/Menu';

import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListSubheader from '@material-ui/core/ListSubheader';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import WorkIcon from '@material-ui/icons/Work';

import TypographyComponent from '../../components/typography';
import Divider from '@material-ui/core/Divider';
import './index.scss';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

const StyledMenu = withStyles({
	paper: {
		border: '1px solid #d3d4d5',
	},
})((props: MenuProps) => (
	<Menu
		elevation={0}
		getContentAnchorEl={null}
		anchorOrigin={{
			vertical: 'bottom',
			horizontal: 'center',
		}}
		transformOrigin={{
			vertical: 'top',
			horizontal: 'center',
		}}
		{...props}
	/>
));
const useStyles = makeStyles((theme: Theme) =>
	createStyles({
		root: {
			width: '100%',
			maxWidth: 360,
			minWidth: 150,
		},
		nested: {
			paddingLeft: theme.spacing(4),
		},
	})
);

export default function ProjectTopMenu() {
	const classes = useStyles();
	const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
	const { name: projectName } = useSelector((state: any) => state.project);
	const handleClick = (event: React.MouseEvent<HTMLElement>) => {
		setAnchorEl(event.currentTarget);
	};
	const handleClose = () => {
		setAnchorEl(null);
	};
	const { dark_mode } = useSelector((state: any) => state.user.user)

	return (
		<div className='top-header-project-menu'>
			<Button
				aria-controls='customized-menu'
				aria-haspopup='true'
				endIcon={<ExpandMoreIcon />}
				onClick={handleClick}
				style={{ textTransform: 'none' }}>
				<TypographyComponent label='Projects' variant='body1' />
			</Button>
			<StyledMenu
				id='customized-project-menu'
				anchorEl={anchorEl}
				keepMounted
				open={Boolean(anchorEl)}
				onClose={handleClose}>
				<List
					component='nav'
					aria-labelledby='nested-list-subheader'
					className={classes.root}
					subheader={
						<ListSubheader component='div' id='nested-list-subheader'>
							<TypographyComponent label='RECENT' variant='caption' />
						</ListSubheader>
					}>
					<Divider />
					<Link to='/project' onClick={handleClose}>
						<ListItem button>
							<ListItemIcon>
								<WorkIcon />
							</ListItemIcon>
							<h3 style={{ color: dark_mode ? 'white' : 'black' }} className='ellipsis'>{projectName}</h3>
						</ListItem>
					</Link>
					<Link to='/projects' onClick={handleClose}>
						<ListItem button style={{ textAlign: 'center' }}>
							<ListItemText primary='Show All Projects' />
						</ListItem>
					</Link>
				</List>
			</StyledMenu>
		</div>
	);
}
