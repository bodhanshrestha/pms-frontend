import React from 'react';

import { useSelector } from 'react-redux';

import TaskForm from './main';
import CircularProgress from '@material-ui/core/CircularProgress';

const TaskFormIndex = ({ handleClose }: any) => {
	const project = useSelector((state: any) => state.project);
	const { user } = useSelector((state: any) => state.user);
	const ganttTasks = useSelector((state: any) => state.ganttTasks);
	return (
		<>
			{Object.keys(project).length > 0 && Object.keys(ganttTasks).length > 0 && Object.keys(user).length > 0 ? (
				<TaskForm
					project={project ? [project] : []}
					userId={user._id}
					epic={ganttTasks.tasks}
					handleClose={handleClose}
				/>
			) : (
				<div className='flex-center loader-main'>
					<CircularProgress style={{ width: '150px', height: '150px' }} />
				</div>
			)}
		</>
	);
};

export default TaskFormIndex;
