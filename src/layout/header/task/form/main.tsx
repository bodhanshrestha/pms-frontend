import React, { useState, useEffect } from 'react';
import './index.scss';
// import InputLabel from '@material-ui/core/InputLabel';
import InputField from '../../../../components/form/text';
import SelectInputField from '../../../../components/form/select';
// import DropzoneAreaExample from '../../../../components/dropZone';
import * as yup from 'yup';
import HtmlEditor, {
	Toolbar,
	MediaResizing,
	Item,
} from 'devextreme-react/html-editor';
import { useDispatch } from 'react-redux';
import { useFormik } from 'formik';
import {
	createTaskAsyncAction,
	fetchNewTasks,
} from '../../../../store/task/asyncAction';
import ButtonComponent from '../../../../components/button';
import Divider from '@material-ui/core/Divider';
import { MappedEpicData } from './../../../../pages/common/taskList/rightSection/epic';
const validationSchema = yup.object({
	title: yup.string().min(3, 'Too Short!').required('Title is required'),
});

const TaskForm = ({ project, userId, handleClose, epic }: any) => {
	const dispatch = useDispatch();
	const [summary, setSummary] = useState('');

	const formik = useFormik({
		initialValues: {
			title: '',
			type: 'Task',
			priority: 'Medium',
			assigne: '',
			reporter: userId,
			epic: '',
			project: project[0]._id,
			sprint: '',
			summary: '',
		},
		validationSchema: validationSchema,
		onSubmit: (values) => {
			const data: any = { ...values };
			// data.attachments = files;
			if (!data.assigne) delete data.assigne;
			if (!data.reporter) delete data.reporter;
			if (!data.sprint) delete data.sprint;
			if (!data.type) delete data.type;
			if (!data.epic) delete data.epic;
			dispatch(createTaskAsyncAction(data));
			dispatch(fetchNewTasks());
			handleClose();
		},
	});

	// const [files, setFiles] = useState([]);
	// const handleFileChange = (files: any) => {
	// 	setFiles(files);
	// };

	const handleReset = () => {
		formik.resetForm();
	};

	const mapTeammate = (team: any) => {
		return team.map((el: any) => {
			return {
				id: el.member._id,
				name: el.member.username,
			};
		});
	};

	const [user, setUser] = useState<any>();
	useEffect(() => {
		const data = mapTeammate(project[0].team).filter((el: any) => {
			return el.id === userId;
		});
		setUser(data);
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);
	return (
		<form
			noValidate
			autoComplete='off'
			onSubmit={(e: any) => e.preventDefault()}
		>
			<InputField
				label='Title'
				name='title'
				type='text'
				small={true}
				placeholder='Enter Task Title'
				value={formik.values.title}
				callback={formik.handleChange}
				error={formik.touched.title && Boolean(formik.errors.title)}
				helperText={formik.touched.title && formik.errors.title}
			/>
			<div className='flex-center gap-10'>
				<SelectInputField
					name='project'
					label='Project'
					value={formik.values.project}
					callback={formik.handleChange}
					width='50%'
					placeholder='Select Project'
					data={project}
					small={true}
				/>
				<SelectInputField
					name='type'
					label='Issue Type'
					width='50%'
					value={formik.values.type}
					callback={formik.handleChange}
					placeholder='Task'
					small={true}
					data={[
						{ value: 'Task', label: 'Task' },
						{ value: 'Bug', label: 'Bug' },
					]}
				/>
			</div>
			<div className='widget-container mb-20 mt-20'>
				<HtmlEditor
					height='175px'
					defaultValue=''
					valueType='html'
					value={summary}
					onValueChanged={(e: any) => setSummary(e.value)}
				>
					<MediaResizing enabled={true} />
					<Toolbar multiline={true}>
						<Item name='bold' />
						<Item name='italic' />
						<Item name='strike' />
						<Item name='underline' />
						<Item name='separator' />
						<Item name='alignLeft' />
						<Item name='alignCenter' />
						<Item name='alignRight' />
						<Item name='alignJustify' />
						<Item name='separator' />
						<Item name='orderedList' />
						<Item name='bulletList' />
					</Toolbar>
				</HtmlEditor>
			</div>
			{/* <InputLabel htmlFor='attachment'>Attachments</InputLabel> */}
			{/* <div id='attachment'>
				<DropzoneAreaExample
					label='Drag and drop an attachments or click'
					callback={handleFileChange}
				/>
			</div> */}
			{/* <br />
			<br /> */}
			<div className='flex-center gap-10'>
				<SelectInputField
					label='Reporter'
					name='reporter'
					width='50%'
					small={true}
					className='mb-20'
					placeholder='Select Reporter'
					value={formik.values.reporter}
					callback={formik.handleChange}
					data={user}
				/>
				<SelectInputField
					label='Assignee'
					name='assigne'
					width='50%'
					className='mb-20'
					small={true}
					placeholder='Select Assignee'
					value={formik.values.assigne}
					callback={formik.handleChange}
					data={mapTeammate(project[0].team)}
				/>
			</div>
			<div className='flex-center gap-10'>
				<SelectInputField
					name='priority'
					label='Priority'
					width='75%'
					small={true}
					className='mb-20'
					placeholder='Select Priority'
					value={formik.values.priority}
					callback={formik.handleChange}
					data={[
						{ value: 'Highest', label: 'Highest' },
						{ value: 'High', label: 'High' },
						{ value: 'Medium', label: 'Medium' },
						{ value: 'Low', label: 'Low' },
						{ value: 'Lowest', label: 'Lowest' },
					]}
				/>
				<SelectInputField
					name='epic'
					label='Epic'
					width='75%'
					className='mb-20'
					value={formik.values.epic}
					callback={formik.handleChange}
					placeholder='Select Epic'
					small={true}
					data={MappedEpicData(epic)}
				/>
			</div>
			<Divider />
			<div className='flex-center gap-20 pt-20'>
				<ButtonComponent
					label='Cancel'
					type='button'
					variant='contained'
					className='w-100'
					color='primary'
					callback={handleClose}
				/>
				<ButtonComponent
					label='Reset'
					type='button'
					variant='contained'
					className='w-100'
					color='primary'
					callback={handleReset}
				/>

				<ButtonComponent
					label='Create'
					type='button'
					variant='contained'
					className='w-100'
					color='primary'
					callback={formik.handleSubmit}
				/>
			</div>
		</form>
	);
};
export default TaskForm;
