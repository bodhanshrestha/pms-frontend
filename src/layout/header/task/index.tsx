import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import DialogComponent from '../../../components/dialog';
import { fetchGanttTasks } from '../../../store/gantt-task/asyncAction';
import TaskFormIndex from './form';

const TaskForm = () => {
	const [open, setOpen] = useState(false)
	const dispatch = useDispatch()
	useEffect(() => {
		dispatch(fetchGanttTasks())
	}, [dispatch])
	return (
		<div>
			<DialogComponent
				title='Create Task'
				action={{
					cancelBtn: '',
					submitBtn: '',
				}}
				btn={{
					label: 'Create Task',
					variant: 'contained',
				}}
				open={open}
				setOpen={setOpen}
			>
				<TaskFormIndex />
			</DialogComponent>
		</div >
	);
};

export default TaskForm;
