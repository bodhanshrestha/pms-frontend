import React, { useContext, useEffect } from 'react';
import clsx from 'clsx';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Fab from '@material-ui/core/Fab';
import Divider from '@material-ui/core/Divider';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import Header from './header/header';
import Content from './content';
import Sidebar from './sidebar';
import './index.scss';
import PaperComponent from '../components/paper';
import { useDispatch } from 'react-redux';

import { useHistory } from 'react-router-dom';
import { checkTokenAccess } from './../store/auth/asyncAction';
import { SocketContext } from '../context/socket';

const drawerWidth = 240;


export default function MiniDrawer(props: any) {
	const dispatch = useDispatch();
	const [open, setOpen] = React.useState(true);
	const handleDrawerOpen = () => {
		setOpen(true);
	};
	const socket: any = useContext(SocketContext);


	const useStyles = makeStyles((theme: Theme) =>
		createStyles({
			root: {
				display: 'flex',
				alignItems: 'center',
			},
			appBar: {
				zIndex: theme.zIndex.drawer + 1,
				background: '#e0e0e0',
				height: '56px',

				transition: theme.transitions.create(['width', 'margin'], {
					easing: theme.transitions.easing.sharp,
					duration: theme.transitions.duration.leavingScreen,
				}),
			},
			appBarShift: {
				marginLeft: drawerWidth,
				width: `calc(100% - ${drawerWidth}px)`,
				transition: theme.transitions.create(['width', 'margin'], {
					easing: theme.transitions.easing.sharp,
					duration: theme.transitions.duration.enteringScreen,
				}),
			},
			menuButton: {
				marginRight: 36,
			},
			hide: {
				display: 'none',
			},
			drawer: {
				width: drawerWidth,
				flexShrink: 0,
				whiteSpace: 'nowrap',
				position: 'relative',
			},
			drawerOpen: {
				width: drawerWidth,
				transition: theme.transitions.create('width', {
					easing: theme.transitions.easing.sharp,
					duration: theme.transitions.duration.enteringScreen,
				}),
				overflow: 'hidden auto',
				[theme.breakpoints.down('xs')]: {
					width: '56px',
				},
			},
			drawerClose: {
				transition: theme.transitions.create('width', {
					easing: theme.transitions.easing.sharp,
					duration: theme.transitions.duration.leavingScreen,
				}),

				width: '56px',
				overflow: 'hidden auto',

			},

			toolbar: {
				display: 'flex',
				alignItems: 'center',
				justifyContent: 'flex-end',
				padding: theme.spacing(0, 1),
				// necessary for content to be below app bar
				...theme.mixins.toolbar,
			},
			burgerMenu: {
				display: 'flex',
				alignItems: 'center',
				justifyContent: 'flex-end',
				padding: theme.spacing(0, 1),
				...theme.mixins.toolbar,
				marginTop: '50px',

			},
			burger: {
				position: 'absolute',
				right: '-14px',
			},
			content: {
				flexGrow: 1,
			},
		})
	);
	const classes = useStyles();
	const handleDrawerClose = () => {
		setOpen(false);
	};

	useEffect(() => {
		const username = localStorage.getItem('username');
		const project = localStorage.getItem('project');
		if (username) {
			socket?.emit('newUser', { username, project });
		}
	}, [socket]);

	const history = useHistory()
	useEffect(() => {
		dispatch(checkTokenAccess(history))
	}, [dispatch, history, history.location.pathname])

	return (
		<div className='menu'>
			<div className={classes.root}>
				<AppBar position='fixed' className={clsx(classes.appBar)}>
					<Header open={open} socket={socket} />
				</AppBar>
				<div className={`side-menu`}>
					<Drawer
						variant='permanent'
						className={clsx(classes.drawer, {
							[classes.drawerOpen]: open,
							[classes.drawerClose]: !open,
						})}
						classes={{
							paper: clsx({
								[classes.drawerOpen]: open,
								[classes.drawerClose]: !open,
							}),
						}}>
						<div>
							<div className={classes.burgerMenu}>
								{open ? (
									<div className={classes.burger}>
										<Fab
											aria-label='like'
											onClick={handleDrawerClose}
											style={{
												width: '35px',
												height: '12px',
												marginTop: '3px',
											}}>
											<ArrowBackIosIcon
												style={{ fontSize: '15px', marginLeft: '0px' }}
											/>
										</Fab>
									</div>
								) : (
									<Fab
										aria-label='like'
										onClick={handleDrawerOpen}
										style={{ width: '35px', height: '12px', marginTop: '3px' }}>
										<ArrowForwardIosIcon
											style={{ fontSize: '15px', marginLeft: '3px' }}
										/>
									</Fab>
								)}
							</div>

							<Divider />
							<Sidebar />
							<Divider />
						</div>
					</Drawer>
				</div>

				<PaperComponent className='main-content-style'>
					<Content>{props.children}</Content>
				</PaperComponent>
			</div>
		</div>
	);
}
