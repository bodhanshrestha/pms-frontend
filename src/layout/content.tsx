import React from 'react';
import PaperComponent from '../components/paper';
const Content = ({ children }: { children: React.ReactNode }) => {
	return (
		<PaperComponent className='main-paper-component'>
			<div className='p-20 pt-80'>{children}</div>
		</PaperComponent>
	);
};

export default Content;
