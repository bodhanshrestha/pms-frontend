import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import './style.css';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter as Router } from 'react-router-dom';

import { Provider } from 'react-redux';
import store from './store';
import App from './App';
import { Theme } from './theme';
import SocketProvider from './context/socketProvider';

ReactDOM.render(
	<Provider store={store}>
		<SocketProvider>
			<Theme>
				<Router>
					<App />
				</Router>
			</Theme>
		</SocketProvider>
	</Provider>,
	document.getElementById('root')
);

reportWebVitals();
