import React from 'react';
import './App.scss';
import SnackbarComponent from './components/snackbar';
import Routes from './routes';

const App = () => {
	return (
		<>
			<SnackbarComponent />
			<Routes />
		</>
	);
};

export default App;
