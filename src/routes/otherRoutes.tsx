import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import storage from 'redux-persist/lib/storage';

const OtherRoute = ({ component: Component, ...rest }: any) => {
	const username = localStorage.getItem('username');
	const project = localStorage.getItem('project');
	return (
		<Route
			{...rest}
			render={(props: any) => {
				if (username && project) return <Redirect to='/dashboard' />;
				else if (rest.exact) return <Component {...rest} {...props} />;
				else {
					storage.removeItem('persist:root');
					localStorage.clear();
					return <Redirect to='/login' />;
				}
			}}
		/>
	);
};

export default OtherRoute;
