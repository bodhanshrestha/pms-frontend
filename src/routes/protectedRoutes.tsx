import React from 'react';

import { Route, Redirect } from 'react-router-dom';
import Layout from '../layout';
import storage from 'redux-persist/lib/storage';
const ProtectedRoute = ({
	component: Component,
	withoutLayout,
	...rest
}: any) => {
	const username = localStorage.getItem('username');
	const token = localStorage.getItem('token');
	const project = localStorage.getItem('project');
	let params = new URLSearchParams(rest.location.search);

	const hash = params.get('hash');
	const parsedUsername = params.get('username');

	const Routed = () => {
		return (
			<Route
				{...rest}
				render={(props: any) => {
					if (rest.exact && username && token && withoutLayout)
						return <Component {...rest} {...props} />;
					if (rest.exact && hash && parsedUsername && withoutLayout)
						return <Component {...rest} {...props} />;
					else if (rest.exact && username && token && project) {
						return (
							<Layout>
								<Component {...rest} {...props} />
							</Layout>
						);
					} else {
						storage.removeItem('persist:root');
						localStorage.clear();
						return <Redirect to='/login' />;
					}
				}}
			/>
		);
	};
	return Routed();
};

export default ProtectedRoute;
