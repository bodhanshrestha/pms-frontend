import React from 'react';
import { Switch } from 'react-router-dom';
import BacklogPage from '../pages/backlog';
import Dashboard from '../pages/dashboard';
import DashboardProject from '../pages/dashboard/project';
import DocumentPage from '../pages/documents';
import IssuesPage from '../pages/issues';
import PeoplePage from '../pages/people';
import ProjectPage from '../pages/project';
import ReportPage from '../pages/reports/chart';
import SprintPage from '../pages/sprint';
import ProtectedRoute from './protectedRoutes';
import SignUp from '../pages/login-process/sign-process';
import EmailVerification from '../pages/login-process/email-verification';
import InitialPage from '../pages/login-process/initial';
import SetupProject from '../pages/login-process/initial-project';
import ProjectTask from '../pages/login-process/initial-project-task';
import TaskStages from '../pages/login-process/default-task-stages';
import InviteTeam from '../pages/login-process/invite-team-mates';
import Help from '../pages/help';
import OtherRoutes from './otherRoutes';
import ProfilePage from '../pages/profile';
import WelcomePage from '../pages/welcome/main';
import StartUp from '../pages/welcome/startup';
import WelcomeNewUser from '../pages/welcome/newUser';
import ErrorPageOutSide from '../pages/error-page/error-outside';
import { useSelector } from 'react-redux';
import LinkErrorPage from '../pages/error-page/link-error';
import PeopleRole from '../pages/people/role';
import MilestonePage from '../pages/milestone';
import MainTaskView from '../pages/common/taskList/main';
import GanttChart from '../pages/plan/gantt/gantt';
import DiagramPage from './../pages/plan/diagram/diagram';
import SelectProject from './../pages/projectSelect/index';
import NotificationPage from '../pages/notification';
import SprintReport from './../pages/reports/sprint/index';
import ProjectsPage from './../pages/projects';
import ChangePasswordPage from './../pages/login-process/changePassword/index';
import CriticalPathCalculation from '../pages/plan/critical method';


const Routes = () => {
	const auth = useSelector((state: any) => state.auth);

	return (
		<Switch>
			<ProtectedRoute exact path='/project' component={ProjectPage} />
			<ProtectedRoute exact path='/projects' component={ProjectsPage} />
			<ProtectedRoute
				exact
				path='/dashboard/project'
				component={DashboardProject}
			/>

			<ProtectedRoute
				exact
				path='/dashboard'
				component={Dashboard}
				authData={auth}
			/>
			<ProtectedRoute exact path='/backlog' component={BacklogPage} />
			<ProtectedRoute exact path='/task/:id' component={MainTaskView} />
			<ProtectedRoute exact path='/milestone' component={MilestonePage} />
			<ProtectedRoute exact path='/sprint' component={SprintPage} />
			<ProtectedRoute exact path='/people' component={PeoplePage} />
			<ProtectedRoute exact path='/people/edit-role' component={PeopleRole} />
			<ProtectedRoute exact path='/issues' component={IssuesPage} />
			<ProtectedRoute exact path='/documents' component={DocumentPage} />
			<ProtectedRoute exact path='/setting' component={ProjectPage} />
			<ProtectedRoute exact path='/profile' component={ProfilePage} />
			<ProtectedRoute exact path='/plan/gantt-chart' component={GanttChart} />
			<ProtectedRoute exact path='/plan/analyze' component={CriticalPathCalculation} />
			<ProtectedRoute exact path='/plan/diagram' component={DiagramPage} />
			<ProtectedRoute exact path='/notification' component={NotificationPage} />
			<ProtectedRoute exact path='/report/chart' component={ReportPage} />
			<ProtectedRoute exact path='/report/sprint' component={SprintReport} />

			<ProtectedRoute exact path='/help' component={Help} />
			<ProtectedRoute
				exact
				withoutLayout
				path='/initial'
				component={InitialPage}
			/>
			<OtherRoutes
				exact
				withoutLayout
				path='/welcome'
				component={WelcomePage}
			/>
			<ProtectedRoute
				exact
				withoutLayout
				path='/select-project'
				component={SelectProject}
			/>
			<ProtectedRoute
				exact
				path='/welcome-project'
				component={StartUp}
				withoutLayout
			/>
			<ProtectedRoute
				exact
				path='/welcome-newUser'
				component={WelcomeNewUser}
				withoutLayout
			/>

			<ProtectedRoute
				exact
				withoutLayout
				path='/initial-project'
				component={SetupProject}
			/>
			<ProtectedRoute
				exact
				withoutLayout
				path='/initial-project-task'
				component={ProjectTask}
			/>

			<ProtectedRoute
				exact
				withoutLayout
				path='/default-task-stages'
				component={TaskStages}
			/>
			<ProtectedRoute
				exact
				withoutLayout
				path='/initial-invite-team'
				component={InviteTeam}
			/>
			<OtherRoutes
				exact
				path='/email-verification'
				component={EmailVerification}
			/>
			<OtherRoutes exact path='/changePassword' component={ChangePasswordPage} />
			<OtherRoutes exact path='/login' component={SignUp} />
			<OtherRoutes exact path='/' component={SignUp} />
			<OtherRoutes exact path='/err' component={ErrorPageOutSide} />
			<OtherRoutes exact path='/link-error' component={LinkErrorPage} />
			<OtherRoutes />
		</Switch>
	);
};

export default Routes;
